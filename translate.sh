#!/bin/bash

# Runs sed against all files in the "psx" folder.
# This should be enough to "correctly" translate the jp.dic and *.xml files
# This is needed because XOSD appears to use the name of some menu items to 
#   match up the top level items to the lower level entries. This means 
#   that two instances of the same JP string ends up being the same EN string.

# Use this to find Japanese characters:
# grep -rh "["$'\xe4\xb8\x80'"-"$'\xe9\xbe\xa5'"]" psx/ | sed -e 's/.* = "/"/g' | sed -e 's/.*str="/"/' | sed -e 's/";/"/'

# Find all the files in the psx/ folder
FILES=$(find psx/ -type f \( -name "*.xml" -o -name "jp.dic" \) )

# Make staging folder
mkdir -p staging/psx lint/psx

# Mirror folder structure
rsync -a -f"+ */" -f"- *" psx/ staging/psx/
rsync -a -f"+ */" -f"- *" psx/ lint/psx/

# Loop for each file
for file in $FILES; do
  echo "Operating on $file"

  # Lint the XML files
  if [[ "$file" == *.xml ]]; then

    # Format as UTF-8 (usually is, but doesn't declare it properly

    # Remove comments, not tested yet
    cat "$file" | sed -e 's^<?xml version="1.0"?>^<?xml version="1.0" encoding="UTF-8"?>^' | xmlstarlet ed -d '//comment()' > "lint/$file"

    # Run substitution
    sed -f substitutions.sed "lint/$file" > staging/$file

    # Ensure XML files use CRLF (\r\n, Windows) formatting
    unix2dos -q -r "staging/$file"
  else

    # Run substitution
    sed -f substitutions.sed "$file" > staging/"$file"

  fi



done

# Make output folder
mkdir -p final

# Sync only changed files - this means FTP clients can look at mtime
rsync -ac staging/psx/ final/

./lint.sh
