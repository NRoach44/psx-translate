# Script to translate PSX UI

* Ensure you have `bash`, `rsync`, `xmlstarlet`, `sed`, `xmllint`, and optionally `lftp`.

* Place your PSX HDD dump (must contain the `__system` partition, which has the dictionary (`.dic`) and XMB (`.xml`) files) in a folder called `psx`.

  * You can transfer them with `lftp -e "mirror -vR psx/ /hdd/0/__system" << PSX IP >>`

* Run the translation script: `./translate.sh`

  * It automatically runs `./lint.sh` so the last line of output before your shell should be "Operating on...". If not, there's probably an issue

* Copy the translated files back to the PSX: `lftp -e "mirror -vR final/ /hdd/0/__system" << PSX IP >>`

## How it works

* Mirrors the folder structure into `staging/` and `final/` using `rsync`.
* Scans for `*.dic` and `*.xml` files.

  * If it's an XML file, it adjusts the file to report as UTF-8.
  * Then lints the file with `xmlstarlet` (PSX1 v 1.10 contains XML files that delimit strings with `'`, which breaks things) into `lint/`.

* Use `sed` to apply substitutions from `substitutions.sed` and output the new file into `staging/`.
* Uses `rsync` to copy any file, **only when it is different**, to `final/`.

  * This means FTP clients will be able to use the last modified date on the files to transfer only those that have changed.

* Runs a different linter (`xmllint`) just to check.


## Helpful commands

### Find files with non-ASCII characters:
`grep --color='auto' -P "[^\x00-\x7F]" psx/`
