s^"～"^"~"^
s^"%$3:ファイナライズするセッション.%3$s.が不正です。"^"%$3: The session '%3$s' to be finalized is invalid."^
s^"100 笠岡"^"100 Kasaoka"^
s^"101 広島"^"101 Hiroshima"^
s^"102 福山"^"102 Fukuyama"^
s^"103 尾道"^"103 Onomichi"^
s^"104 呉"^"104 Kure"^
s^"105 山口"^"105 Yamaguchi"^
s^"106 下関"^"106 Shimonoseki"^
s^"107 宇部"^"107 Ube"^
s^"108 岩国"^"108 Iwakuni"^
s^"109 徳島"^"109 Tokushima"^
s^"10 釧路"^"10 Kushiro"^
s^"110 高松"^"110 Takamatsu"^
s^"111 丸亀"^"111 Marugame"^
s^"112 松山"^"112 Matsuyama"^
s^"113 新居浜"^"113 Niihama"^
s^"114 今治"^"114 Imabari"^
s^"115 宇和島"^"115 Uwajima"^
s^"116 高知"^"116 Kochi"^
s^"117 福岡"^"117 Fukuoka"^
s^"118 久留米"^"118 Kurume"^
s^"119 大牟田"^"119 Omuta"^
s^"11 網走"^"11 Abashiri"^
s^"120 北九州"^"120 Kitakyushu"^
s^"121 行橋"^"121 Yukuhashi"^
s^"122 佐賀"^"122 Saga"^
s^"123 長崎"^"123 Nagasaki"^
s^"124 佐世保"^"124 Sasebo"^
s^"125 諫早"^"125 Isahaya"^
s^"126 熊本"^"126 Kumamoto"^
s^"127 大分"^"127 Oita"^
s^"128 中津"^"128 Nakatsu"^
s^"129 宮崎"^"129 Miyazaki"^
s^"12 北見"^"12 Kitami"^
s^"12時間"^"12 hours"^
s^"130 延岡"^"130 Nobeoka"^
s^"131 鹿児島"^"131 Kagoshima"^
s^"132 kbps ( 高音質 )"^"132 kbps (high quality)"^
s^"132 kbps (高音質)"^"132 kbps (high quality)"^
s^"132 阿久根"^"132 Akune"^
s^"133 鹿屋"^"133 Kanoya"^
s^"134 沖縄"^"134 Okinawa"^
s^"13 青森"^"13 Aomori"^
s^"14 八戸"^"14 Hachinohe"^
s^"15 むつ"^"15 Mutsu"^
s^"16 : 9 　　　　　　"^"16:9"^
s^"16 : 9"^"16:9"^
s^"16：9"^"16:9"^
s^"16 盛岡"^"16 Morioka"^
s^"17 釜石"^"17 Kamaishi"^
s^"18 二戸"^"18 Ninohe"^
s^"19 仙台"^"19 Sendai"^
s^"1サムネイル"^"1 Thumbnail"^
s^"1回のみ録画可能なタイトルは、..青い画面で録画されます。"^"Titles that can only be recorded once will be recorded with a blue screen."^
s^"1回のみ録画可能なタイトルをダビングします。..ダビング後、プロテクトされていても..ハードディスクから削除されます。..よろしいですか？"^"Dub a title that can only be recorded once.\\nAfter dubbing, it will be deleted from the hard disk even if it is protected.\\nAre you sure?"^
s^"1時間以内(%s)に録画予約があります。..ダビング中は予約録画は開始されませんが..ダビングを開始しますか?"^"There is a scheduled recording within 1 hour (%s).\\nThe scheduled recording will not start while dubbing.\\nWould you like to start dubbing?"^
s^"1時間以内(%s)に録画予約があります。..初期化中は予約録画は開始されませんが..初期化を開始しますか?"^"There is a scheduled recording within 1 hour (%s).\\nThe scheduled recording will not start during initialization.\\nDo you want to start initialization?"^
s^"1 札幌"^"1 Sapporo"^
s^"20 石巻"^"20 Ishinomaki"^
s^"21 気仙沼"^"21 Kesennuma"^
s^"22 秋田"^"22 Akita"^
s^"23 大館"^"23 Odate"^
s^"24 大曲"^"24 Omagari"^
s^"24時間"^"24hours"^
s^"25 山形"^"25 Yamagata"^
s^"26 鶴岡"^"26 Tsuruoka"^
s^"27 米沢"^"27 Yonezawa"^
s^"28 福島"^"28 Fukushima"^
s^"29 いわき"^"29 Iwaki"^
s^"2 小樽"^"2 Otaru"^
s^"30 会津若松"^"30 Aizuwakamatsu"^
s^"31 水戸"^"31 Mito"^
s^"32 日立"^"32 Hitachi"^
s^"33 宇都宮"^"33 Utsunomiya"^
s^"&#34;DNAS&#34;エラーがおこりました。"^"DNAS Error has occured"^
s^"&#34;DNAS&#34;サーバーエラーです。"^"DNAS Server error"^
s^"&#34;DNAS&#34;サーバーが正常に動作しておりません。"^"DNAS server is not working properly."^
s^"&#34;DNAS&#34;サーバーとの接続がタイムアウトしました。"^"Connection to DNAS server timed out."^
s^"&#34;DNAS&#34;サーバーへの接続に失敗しました。"^"Failed to connect to DNAS server."^
s^"&#34;DNAS&#34;認証エラーです。"^"DNAS Authentication error."^
s^"&#34;DNAS&#34;認証中です。"^"DNAS Authenticating."^
s^"34 矢板"^"34 sheet piles"^
s^"35 前橋"^"35 Maebashi"^
s^"36 桐生"^"36 Kiryu"^
s^"37 さいたま"^"37 Saitama"^
s^"38 熊谷"^"38 Kumagaya"^
s^"39 秩父"^"39 Chichibu"^
s^"3サムネイル(特殊レイアウト)"^"3 thumbnails (special layout)"^
s^"3 旭川"^"3 Asahikawa"^
s^"40 千葉"^"40 Chiba"^
s^"41 銚子"^"41 Choshi"^
s^"42 東京２３区"^"42 Tokyo 23 Wards"^
s^"4：3 パンスキャン"^"4:3 Letterbox"^
s^"4 : 3パンスキャン"^"4:3 Pan and Scan"^
s^"4 : 3パンスキャン　"^"4:3 Pan and Scan"^
s^"4：3 パンスキャン"^"4:3 Pan and Scan"^
s^"4 : 3レターボックス"^"4:3 Letterbox"^
s^"4：3 レターボックス"^"4:3 Letterbox"^
s^"43 八王子"^"43 Hachioji"^
s^"44 多摩"^"44 Tama"^
s^"45 横浜１"^"45 Yokohama 1"^
s^"46 横浜２"^"46 Yokohama 2"^
s^"47 平塚"^"47 Hiratsuka"^
s^"48 秦野"^"48 Hadano"^
s^"49 小田原"^"49 Odawara"^
s^"4サムネイル"^"4 Thumbnails"^
s^"4 名寄"^"4 Nayoro"^
s^"4桁の暗証番号を入力してください。"^"Please enter your 4-digit PIN."^
s^"50 甲府"^"50 Kofu"^
s^"51 長野１"^"51 Nagano 1"^
s^"52 長野２"^"52 Nagano 2"^
s^"53 松本"^"53 Matsumoto"^
s^"54 飯田"^"54 Iida"^
s^"55 岡谷・諏訪"^"55 Okaya/Suwa"^
s^"56 新潟"^"56 Niigata"^
s^"57 上越"^"57 Joetsu"^
s^"58 富山"^"58 Toyama"^
s^"59 高岡"^"59 Takaoka"^
s^"5 稚内"^"5 Wakkanai"^
s^"60 金沢"^"60 Kanazawa"^
s^"61 七尾"^"61 Nanao"^
s^"62 福井"^"62 Fukui"^
s^"63 敦賀"^"63 Tsuruga"^
s^"64 岐阜"^"64 Gifu"^
s^"65 高山"^"65 Takayama"^
s^"66 中津川"^"66 Nakatsugawa"^
s^"67 静岡"^"67 Shizuoka"^
s^"68 浜松"^"68 Hamamatsu"^
s^"69 富士"^"69 Fuji"^
s^"6 室蘭"^"6 Muroran"^
s^"6時間以上の録画はできません。"^"You cannot record more than 6 hours."^
s^"6時間以上の録画予約はできません。..開始または終了時刻を正しく設定してください。"^"You cannot schedule recording for more than 6 hours.\\nPlease set the start or end time correctly."^
s^"70 三島・沼津"^"70 Mishima/Numazu"^
s^"71 島田"^"71 Shimada"^
s^"72 藤枝"^"72 Fujieda"^
s^"73 名古屋"^"73 Nagoya"^
s^"74 豊橋"^"74 Toyohashi"^
s^"75 豊田"^"75 Toyota"^
s^"76 津"^"76 Tsu"^
s^"77 伊勢"^"77 Ise"^
s^"78 名張"^"78 Nabari"^
s^"79 大津"^"79 Otsu"^
s^"7 苫小牧"^"7 Tomakomai"^
s^"80 彦根"^"80 Hikone"^
s^"81 京都"^"81 Kyoto"^
s^"82 舞鶴"^"82 Maizuru"^
s^"83 福知山"^"83 Fukuchiyama"^
s^"84 大阪"^"84 Osaka"^
s^"85 神戸"^"85 Kobe"^
s^"86 神戸灘"^"86 Kobe Sea"^
s^"87 川西"^"87 Kawanishi"^
s^"88 三木"^"88 Miki"^
s^"89 姫路"^"89 Himeji"^
s^"8cmメディアはサポートされません。"^"8cm media is not supported."^
s^"8サムネイル"^"8 Thumbnails"^
s^"8 函館"^"8 Hakodate"^
s^"90 明石"^"90 Akashi"^
s^"91 奈良"^"91 Nara"^
s^"92 五條"^"92 Gojo"^
s^"93 和歌山"^"93 Wakayama"^
s^"94 海南・田辺"^"94 Kainan/Tanabe"^
s^"95 鳥取"^"95 Tottori"^
s^"96 松江"^"96 Matsue"^
s^"97 浜田"^"97 Hamada"^
s^"98 岡山"^"98 Okayama"^
s^"99 津山"^"99 Tsuyama"^
s^"9 帯広"^"9 Obihiro"^
s^"Ａ-Ｂリピート"^"A-B repeat"^
s^"ABリピートポイントをクリア"^"Clear AB repeat point"^
s^"“AIDJ”アルゴリズムによって実現されています。"^"Achieved by the AIDJ algorithm."^
s^"ATAPIエラー"^"ATAPI error"^
s^"ATAPIドライバ"^"ATAPI driver"^
s^"ATAPIプロトコル"^"ATAPI protocol"^
s^"「ATOK」は株式会社ジャストシステムの登録商標です。"^"「ATOK」 is a registered trademark of Just System Co., Ltd. "^
s^"Aポイントをクリア"^"Clear A point"^
s^"Aポイントをメモリ"^"Memorize point A"^
s^"Aポイントを検索"^"Search for A point"^
s^"A点と同じ位置にはB点を設定できません。"^"Point B cannot be set at the same position as point A."^
s^"A点を設定しました。"^"Point A has been set."^
s^"BGMを選択してください。"^"Please select background music."^
s^"BGM選択"^"BGM selection"^
s^"BSDライセンスについて"^"About the BSD License"^
s^"BSアンテナレベル表示"^"BS Antenna Signal\\nStrength Display"^
s^"BSアンテナ電源"^"BS Antenna\\nPower Supply"^
s^"BSアンテナ電源 :"^"BS Antenna Power:"^
s^"BSアンテナ電源の設定を選択してください。"^"Please select the BS Antenna Power Setting."^
s^"BSアンテナ電源を入にできませんでした。"^"Failed to turn on the BS Antenna Power."^
s^"BSアンテナ電源設定"^"BS Antenna Power Setting"^
s^"BSチャンネル"^"BS Channel"^
s^"BSチャンネル飛ばし"^"Skip BS channel"^
s^"BUSYタイムアウト。"^"BUSY Timeout"^
s^"Bポイントをメモリ"^"Memory B point"^
s^"CD取込み設定"^"CD import settings"^
s^"CHAPTERリピート"^"CHAPTER repeat"^
s^"Core初期化エラー。"^"Core initialization error."^
s^"DISCリピート"^"DISC repeat"^
s^"DJ Box SEの起動準備をしています"^"Preparing to start DJ Box SE"^
s^"“DJbox”および”AIDJ”は、"^"“DJbox” and “AIDJ” are"^
s^"“DJbox”のDJシミュレータエンジンと"^"“DJbox” DJ simulator engine and"^
s^"DJ登録 :"^"DJ registration :"^
s^"DJ登録"^"Register DJ"^
s^"DJ登録が終了しました。"^"DJ registration has ended."^
s^"DJ登録したトラックがありません。..取り込んだトラックをDJ登録してください。..x-DJを終了します。"^"There are no tracks registered as DJ.\\nPlease register the imported tracks as DJ.\\nx-DJ will be terminated."^
s^"DJ登録しています。"^"Registering songs with x-DJ"^
s^"DJ登録しているトラックは"^"What tracks are registered as DJs?"^
s^"DJ登録しました。"^"I have registered as a DJ."^
s^"DJ登録します。"^"Register as a DJ."^
s^"DJ登録します。..よろしいですか？"^"I'm registering as a DJ.\\nAre you sure?"^
s^"DJ登録できなかったトラックがあります。..アルバムを確認してください。"^"Some tracks could not be registered as DJs.\\nPlease check the album."^
s^"DJ登録を中止します。"^"DJ registration will be cancelled."^
s^"DJ登録を中止します。..よろしいですか？"^"DJ registration will be cancelled.\\nAre you sure?"^
s^"DJ登録を解除しています。"^"DJ registration is being cancelled."^
s^"DJ登録を解除しました。"^"DJ registration has been cancelled."^
s^"DJ登録を解除します。..よろしいですか？"^"I will cancel your DJ registration.\\nAre you sure?"^
s^"DJ登録を解除後、削除します。"^"It will be deleted after canceling DJ registration."^
s^"DJ登録中画面"^"DJ registration screen"^
s^"DJ登録曲がいっぱいです。..不要なトラックのＤＪ登録を解除してください。"^"There are too many songs registered as a DJ.\\nPlease cancel the DJ registration of unnecessary tracks."^
s^"DJ登録曲が無いため起動できません..ミュージックトラックのDJ登録を行ってください"^"Unable to start because there are no DJ registered songs.\\nPlease register the music track as a DJ."^
s^"DJ登録解除"^"Deregister DJ"^
s^"DJ登録解除が終了しました。"^"DJ registration cancellation has been completed."^
s^"DJ登録解除しています。"^"DJ registration is being cancelled."^
s^"DJ登録解除します。"^"DJ registration will be cancelled."^
s^"DJ登録解除を中止します。..よろしいですか？"^"DJ registration cancellation will be cancelled.\\nAre you sure?"^
s^"DNSエラーです。"^"DNS error"^
s^"DNSサーバーがみつかりません。"^"DNS server not found"^
s^"DNSサーバーが応答しません。"^"DNS server is not responding."^
s^"DNSサーバーの応答にエラーがあります。"^"There is an error in the DNS server response."^
s^"DNS設定方法を選択してください。"^"Please select a DNS configuration method."^
s^"Dolby、ドルビー及びダブルD記号はドルビーラボラトリーズの商標です。"^"Dolby, Dolby and the double D symbol are trademarks of Dolby Laboratories."^
s^"DTSの設定を選択してください。"^"Please select your DTS settings."^
s^"DTSを設定し、→ボタンで次の画面に進んでください。"^"Please configure DTS and proceed to the next screen with the → button."^
s^"DTS設定"^"DTS settings"^
s^"（DVD+RW用）"^"(for DVD+RW)"^
s^"DVD-RW記録モード"^"DVD-RW recording\\nmode"^
s^"DVD-RW記録モード変更"^"DVD-RW recording mode change"^
s^"DVDアクセス"^"DVD access"^
s^"DVDが見つかりませんでした。..DVDを挿入してください。"^"No DVD was found.\\nPlease insert the DVD."^
s^"DVDダビング時のディスク使用量："^"Disk usage when dubbing DVD:"^
s^"DVDディスクのフォーマットを行います。"^"Format the DVD disc."^
s^"DVDデバイスにエラーが発生しました。..ダビングを中止します。"^"An error occurred with the DVD device.\\nDubbing will be cancelled."^
s^"DVDドライバエラー"^"DVD drive error"^
s^"DVDドライブが使用中です。"^"DVD drive is in use."^
s^"DVDドライブの書込み機能が使えません。"^"The writing function of the DVD drive cannot be used."^
s^"DVDにタイトルを書き込めません。"^"Unable to write title to DVD."^
s^"DVDに書込む順番を確認してください。"^"Please check the order in which you want to write the DVD."^
s^"DVDのステータスが読めません。"^"Unable to read DVD status."^
s^"DVDのステータス取得に失敗しました。"^"Failed to obtain DVD status."^
s^"DVDのダビングに失敗しました。..ダビングを中止します。"^"DVD dubbing failed.\\nDubbing will be cancelled."^
s^"DVDのファイナライズに失敗しました。"^"Failed to finalize the DVD."^
s^"DVDのファイナライズに失敗しました。..ダビングを中止します。"^"Failed to finalize the DVD.\\nDubbing will be cancelled."^
s^"DVDのファイナライズ解除に失敗しました。"^"Failed to unfinalize the DVD."^
s^"DVDの作成ができませんでした。"^"The DVD could not be created."^
s^"DVDの初期化が正常に終了できませんでした。..ダビングを中止します。"^"DVD initialization could not be completed successfully.\\nDubbing will be cancelled."^
s^"DVDの初期化に失敗しました。"^"Failed to initialize DVD."^
s^"DVDの初期化を行います。"^"The DVD will be initialized."^
s^"DVDの容量が不足しています。..このタイトルはダビングできません。"^"There is not enough space on the DVD.\\nThis title cannot be dubbed."^
s^"DVDの容量が不足するため、..録画モードを自動調整します。..録画モードを固定したいタイトルは、..モード固定を選択してください。..よろしいですか？"^"Due to insufficient DVD space,\\nThe recording mode will be adjusted automatically.\\nIf you want to fix the recording mode for a title,\\nplease select Fixed mode.\\nAre you sure?"^
s^"DVDの状況がわかりません。"^"I don't understand the status of the DVD."^
s^"DVDの設定"^"DVD Settings"^
s^"DVDバーナー"^"DVD burner"^
s^"DVDへの%sを中止します。..よろしいですか?"^"Aborting %s to DVD.\\nAre you sure?"^
s^"DVDへのダビングが完了しました。..ダビングを終了します。"^"Dubbing to DVD is complete.\\nDubbing will end."^
s^"DVDへのファイナライズが完了しました。..ファイナライズを終了します。"^"Finalization to DVD is complete.\\nFinalization will now end."^
s^"DVDへの初期化が完了しました。..初期化を終了します。"^"Initialization to DVD is complete.\\nInitialization will end."^
s^"DVDメニューの作成に失敗しました。..ダビングを中止します。"^"Failed to create DVD menu.\\nDubbing will be cancelled."^
s^"DVDメニューの作成に失敗しました。..ダビングを終了します。"^"Failed to create DVD menu.\\nDubbing will end."^
s^"DVDメニューは20ページまで作成できます。..選択したメニューでは20ページを超えてしまいます。..ほかのメニューを選択してください。"^"You can create a DVD menu with up to 20 pages.\\nThe selected menu will exceed 20 pages.\\nPlease select another menu."^
s^"DVDメニューは20ページ迄です。..このタイプは選択できません。"^"The DVD menu can be up to 20 pages.\\nThis type cannot be selected."^
s^"DVDメニューを作成した後に..自動的にダビングを開始します。"^"After creating the DVD menu\\nit will automatically start dubbing."^
s^"DVDメニューを作成した後、..続いてダビングを開始します。"^"After creating the DVD menu\\nthen start dubbing."^
s^"DVDメニュー作成中…"^"Creating DVD menu..."^
s^"DVDメニュー作成開始に失敗しました。"^"Failed to start DVD menu creation."^
s^"DVDメニュー言語"^"DVD Menu Language"^
s^"DVD二カ国語記録音声"^"DVD Bilingual\\nRecording Audio"^
s^"DVD二カ国語記録音声の音声設定で行われます。"^"This is done in the audio settings of the DVD bilingual recording audio."^
s^"DV→HDDダビング"^"DV → HDD dubbing"^
s^"DVダビング"^"DV Dubbing"^
s^"DV機器からのダビングを開始します。..音声と録画モードを設定し、ダビングを開始してください。"^"Start dubbing from the DV device.\\nPlease set the audio and recording mode and start dubbing."^
s^"DV機器が制御できません。..ダビングを中止しました。"^"The DV device cannot be controlled.\\nDubbing has been cancelled."^
s^"DV機器が認識できません。..ダビングを中止しました。"^"The DV device cannot be recognized.\\nDubbing has been cancelled."^
s^"DV機器にカセットが入っていません。..ダビングを中止しました。"^"There is no cassette in the DV device.\\nDubbing has been cancelled."^
s^"DV音声入力設定"^"DV Audio\\nInput Settings"^
s^"%d時間%d分以内(%s)に録画予約があります。..ダビング中は予約録画は開始されませんが..ダビングを開始しますか?"^"There is a scheduled recording within %d hours and %d minutes (%s).\\nWhile dubbing, scheduled recording will not start.\\nWould you like to start dubbing?"^
s^"%d時間%d分以内(%s)に録画予約があります。..初期化中は予約録画は開始されませんが..初期化を開始しますか?"^"There is a scheduled recording within %d hours and %d minutes (%s).\\nScheduled recording will not start during initialization.\\nDo you want to start initialization?"^
s^"%d時間以内(%s)に録画予約があります。..ダビング中は予約録画は開始されませんが..ダビングを開始しますか?"^"There is a scheduled recording within %d hours (%s).\\nThe scheduled recording will not start while dubbing.\\nWould you like to start dubbing?"^
s^"%d時間以内(%s)に録画予約があります。..初期化中は予約録画は開始されませんが..初期化を開始しますか?"^"There is a scheduled recording within %d hours (%s).\\nScheduled recording will not start during initialization.\\nDo you want to start initialization?"^
s^"%d月%d日でタイトルがいっぱいになります。..不要なタイトルを削除してください。"^"Titles will be full in %d days of %d months.\\nPlease delete unnecessary titles."^
s^"%d月%d日でハードディスクの容量が不足します。..不要なタイトルやトラック、フォトを削除してください。"^"Hard disk space will run out in %d days of %d months.\\nPlease delete unnecessary titles, tracks, and photos."^
s^"D端子からプログレッシブ映像を出力します。..ほかの映像出力端子からは出力しません。..よろしいですか？"^"Progressive video will be output from the D terminal.\\nIt will not be output from other video output terminals.\\nAre you sure?"^
s^"HDD→DVDダビング / DVDメニュー作成"^"HDD → DVD dubbing/DVD menu creation"^
s^"HDD→DVDダビング"^"HDD → DVD dubbing"^
s^"HDD→DVDダビング / タイトル選択"^"HDD→DVD dubbing/title selection"^
s^"HDD→DVDダビング / ディスクチェック"^"HDD→DVD dubbing / disk check"^
s^"HDD→DVDダビング / ファイナライズ"^"HDD→DVD dubbing/finalization"^
s^"HDD→DVDダビング / メニュー選択"^"HDD→DVD dubbing/menu selection"^
s^"HDD→DVDダビング / 初期化"^"HDD→DVD dubbing/initialization"^
s^"HDD→DVDダビング / 拡大表示"^"HDD→DVD dubbing/enlarged display"^
s^"HDD→DVDダビング / 書込み"^"HDD→DVD dubbing/writing"^
s^"HDD→DVDダビング / 書込み順確認"^"HDD→DVD dubbing / Check writing order"^
s^"HDD→DVDダビング / 開始確認"^"HDD→DVD dubbing/start confirmation"^
s^"HDDの領域設定を実行すると、ハードディスクに..保存している以下の内容を消去します。..よろしいですか？"^"When you configure the HDD space,\\nthe following contents saved on the hard disk will be erased.\\nAre you sure?"^
s^"HDD二カ国語記録音声"^"HDD Recording\\nAudio Channel"^
s^"HDD二カ国語記録音声の変更をします。"^"Change the HDD bilingual recording audio."^
s^"HDD読込エラー。"^"HDD reading error."^
s^"HDD領域設定"^"HDD area settings"^
s^"HDD領域設定を中止します。..よろしいですか？"^"Cancel HDD area setting.\\nAre you sure?"^
s^"HDD領域設定を実行し、ゲーム領域を作成します。"^"Execute HDD area settings and create game area."^
s^"HDD領域設定を実行し、ゲーム領域を削除します。"^"Perform HDD area settings and delete game area."^
s^"HDD領域設定を実行し、ゲーム領域を変更します。"^"Perform HDD area settings and change game area."^
s^"I/Oエラーが発生しました。"^"An I/O error has occurred."^
s^"IPアドレス :"^"IP Address:"^
s^"ＩＰアドレス"^"IP Address"^
s^"IPアドレスの入力値を確認してください。"^"Please check the IP address input."^
s^"IPアドレスの自動設定ができませんでした。"^"The IP address could not be automatically configured."^
s^"IPアドレス、またはデフォルトルータの"^"IP Address or Default Router"^
s^"IPアドレス、またはネットマスクの"^"IP Address or Subnet Mask"^
s^"IPアドレス設定方法を選択してください。"^"Please select an IP address setting method."^
s^"JP"^"EN"^
s^"J-ポップ"^"J-Pop"^
s^"Ｊ－ポップ"^"J-pop"^
s^"L2・R2ボタンにサーチの機能を設定します。"^"Set the search function to the L2/R2 button."^
s^"L2・R2ボタンにサーチの機能を設定します。..フラッシュは操作パネルから行ってください。..よろしいですか？"^"Set the search function to the L2 and R2 buttons.\\nPlease flash from the control panel.\\nAre you sure?"^
s^"L2・R2ボタンにフラッシュの機能を設定します。"^"Set the flash function to the L2 and R2 buttons."^
s^"L2・R2ボタンにフラッシュの機能を設定します。..サーチはジョイスティックの左右で行ってください。..よろしいですか？"^"Set the flash function for the L2 and R2 buttons.\\nPlease use the left and right joysticks to search.\\nAre you sure?"^
s^"L2・R2ボタン設定"^"L2/R2 Button Settings"^
s^"MACアドレス :"^"MAC Address:"^
s^"Pict Storyに利用できるフォトはありません。"^"There are no photos available for Pict Story."^
s^"Pict Storyを作成するためのフォトはありません。"^"There are no photos to create a Pict Story."^
s^"Pict Storyを保存しました。　　"^"Pict Story has been saved."^
s^"Pict Storyを保存しました。　　　　　"^"Pict Story has been saved."^
s^"Pict Storyを保存しました。　　　　　　　"^"Pict Story has been saved."^
s^"Pict Storyを保存しました。　　　　　　　　"^"Pict Story has been saved."^
s^"Pict Storyを保存しました。　　　　　　　　　　　　　"^"Pict Story has been saved."^
s^"Pict Storyを保存します。　　　　　　　"^"Save Pict Story."^
s^"Pict Storyを選択してください。"^"Please select a Pict Story."^
s^"Pict Story作成"^"Create Pict Story"^
s^"Pict Story修正"^"Pict Story Modification"^
s^"Pict Story編集を中止します。"^"Cancel Pict Story editing."^
s^"Pict Story編集を終了します。　"^"Ending Pict Story editing."^
s^"Pict Story編集を終了します。　　　　　"^"Ending Pict Story editing."^
s^"Pict Story編集を終了します。　　　　　　"^"Ending Pict Story editing."^
s^"Pict Story編集を終了します。"^"Quit editing Pict Story."^
s^"Pict Story編集を終了します。　　　　"^"Quit editing Pict Story."^
s^"Pict Story編集を終了します。　　　　　　　　　"^"Quit editing Pict Story."^
s^"Pict Story編集を終了します。　　　　　　　　　　　　"^"Quit Editing Pict Story."^
s^"Pict Story編集終了"^"Finish Pict Story editing"^
s^"Pict Story選択"^"Pict Story selection"^
s^"PlayStation®2ディスク"^"PlayStation®2 Disc"^
s^"PlayStation®2格式的软件"^"PlayStation®2 official version"^
s^"PlayStation®2規格ソフトウェア"^"PlayStation®2 standard software"^
s^"PlayStation®2規格之軟件"^"PlayStation®2 standard software"^
s^"PlayStation®ゲーム ..テクスチャマッピング"^"PlayStation® Game\\nTexture Mapping"^
s^"PlayStation®ゲーム..テクスチャマッピング"^"PlayStation® Game\\nTexture Mapping"^
s^"PlayStation®ディスク"^"PlayStation® Disc"^
s^"PlayStation®ディスク ..読込み速度"^"PlayStation® Disc \\nReading Speed"^
s^"PlayStation®ディスク..読込み速度"^"PlayStation® Disc\\nReading Speed"^
s^"PlayStation®またはPlaystation®2規格の..ディスクではありません。"^"This disc is not PlayStation® or Playstation®2 standard."^
s^"PlayStation®規格ソフトウェア"^"PlayStation® standard software"^
s^"PPPoEのユーザーIDとパスワードを確認してください。"^"Please check your PPPoE user ID and password."^
s^"PPPoEの一般的なエラーです。"^"This is a common PPPoE error."^
s^"PSCエラー。"^"PSC error."^
s^"PSCのエラーはありません"^"No PSC errors"^
s^"PSXオリジナルサウンド"^"PSX Original Sound"^
s^"PSXについて"^"About PSX"^
s^"PSXの&#34;DNAS&#34;認証サービス期間が終了しました。"^"PSX's DNAS authentication service period has ended."^
s^"PSXの&#34;DNAS&#34;認証サービス期間以前です。"^"This is before the PSX DNAS authentication service period."^
s^"&quot;、&quot;PSX&quot;および&quot;PlayStation&quot;は"^"PSX and Playstation are"^
s^"R3ボタンを押してください。"^"Press the R3 button."^
s^"RSAおよびBSAFEは、RSA Security Inc.の日本、米国および"^"RSA and BSAFE are registered trademarks of RSA Security Inc. in Japan, the United States and "^
s^"%sを中止します。..よろしいですか?"^"%s will be cancelled.\\nAre you sure?"^
s^"S映像"^"S-Video"^
s^"%s終了までおよそ  %d分%d秒"^"Approximately %d minutes %d seconds until %s ends"^
s^"TITLEリピート"^"TITLE repeat"^
s^"TVシステムが違います。"^"The TV system is different."^
s^"TVタイプ :"^"TV Type:"^
s^"TVタイプ"^"TV Type"^
s^"TVタイプ設定"^"TV type setting"^
s^"USB機器"^"USB device"^
s^"Videoモード"^"Video mode"^
s^"Videoモードで%sを行います。..ディスクに何か書き込まれている場合は、..その内容は削除されます。..よろしいですか?"^"%s in Video mode.\\nIf anything has been written to the disc,\\nit will be deleted.\\nAre you sure?"^
s^"VIEWER禁止。"^"VIEWER prohibited."^
s^"VPS/PDC Scan 設定 を変更してください。"^"Please change the VPS/PDC Scan settings."^
s^"VPS/PDCをScanしています。"^"Scanning VPS/PDC."^
s^"VRモード"^"VR mode"^
s^"VRモードで%sを行います。..ディスクに何か書き込まれている場合は、..その内容は削除されます。..よろしいですか?"^"%s in VR mode.\\nIf there is anything written to the disc, it will be deleted.\\nAre you sure?"^
s^"+VRモードのディスクです。..追記しますか?"^"This is a +VR mode disc.\\nWould you like to add additional information?"^
s^"VRモードのディスクです。..追記しますか?"^"This is a VR mode disc.\\nWould you like to add additional information?"^
s^"VRモードプレイリスト"^"VR Mode Playlist"^
s^"x-DJの起動準備をしています。"^"Preparing to start x-DJ."^
s^"x-DJ登録にエラーが発生しました。..データが破損しています。..DJboxサウンドデータと、x-DJユーザーデータを..削除してください。"^"An error occurred with x-DJ registration.\\nThe data is corrupted.\\nPlease delete the DJbox sound data and x-DJ user data."^
s^"--:--xM - --:--xM 放送局名"^"--:--xM - --:--xM Broadcasting station name"^
s^"..:..xM - ..:..xM 放送局名"^"..:..xM - ..:..xM station name"^
s^"x-Pict Story 編集"^"x-Pict Story Edit"^
s^"x-おまかせ・まる録"^"x-Omakase/Maruroku"^
s^"アーティスト"^"Artist"^
s^"アーティスト名 :"^"Artist name:"^
s^"アイテム名"^"Item name"^
s^"アイドル"^"Idol"^
s^"アゾレス諸島"^"Azores"^
s^"アップデート"^"Update"^
s^"アップデート / アップデート実行確認"^"Update/Confirm update execution"^
s^"アップデート / アップデート開始確認"^"Update/Confirm update start"^
s^"アップデート / インストール"^"Update/Install"^
s^"アップデートが終了しました。"^"Update completed."^
s^"アップデート / ソフトウェア確認"^"Update/Software Check"^
s^"アップデート / ダウンロード"^"Update/Download"^
s^"アップデート / バックアップ"^"Update/Backup"^
s^"アップデートを中止しています。"^"Update is being cancelled."^
s^"アップデートを中止します。"^"Update will be cancelled."^
s^"アップデートを中止し、本機を再起動します。"^"Cancel the update and restart the device."^
s^"アップデートを実行します。"^"Perform the update."^
s^"アップデートを終了します。"^"Update will end."^
s^"アップデートを開始します。"^"Starting update."^
s^"アップデート中、サーバーとの通信が中断されました。"^"Communication with the server was interrupted during the update."^
s^"アップデート中、サーバーとの通信に失敗しました。"^"Communication with the server failed during update."^
s^"アップデート中、サーバーへの接続に失敗しました。"^"Failed to connect to server during update."^
s^"アップデート中にエラーが発生しました。"^"An error occurred while updating."^
s^"アップデート中、本機を再起動しますので"^"During the update, the device will be restarted."^
s^"アップデート / 使用許諾"^"Update/License"^
s^"アップデート / 復旧処理"^"Update/Recovery Process"^
s^"アップデート / 終了処理"^"Update/Termination"^
s^"アテネ"^"Athens"^
s^"アデレード"^"Adelaine"^
s^"アニメ"^"Anime"^
s^"アブダビ"^"Abu Dhabi"^
s^"アプリケーション (PocketStation®)"^"Application (PocketStation®)"^
s^"アムステルダム"^"Amsterdam"^
s^"アメリカ"^"America"^
s^"アラスカ"^"Alaska"^
s^"アルジェ"^"Alger"^
s^"アルバム "^"Album"^
s^"アルバム :"^"Album:"^
s^"アルバム"^"Album"^
s^"アルバム："^"Album:"^
s^"アルバム %d"^"Album:"^
s^"アルバムからフォトを確認してください。"^"Please check the photo from the album."^
s^"アルバムジャケットにする"^"Set Cover"^
s^"アルバムジャケットに登録しました。"^"Registered for album jacket."^
s^"アルバムジャケット登録"^"Register album jacket"^
s^"アルバムにあるすべてのトラックについて..DJ登録を解除します。..よろしいですか？"^"I will cancel the DJ registration for all tracks on the album.\\nAre you sure?"^
s^"アルバムにあるすべてのトラックは削除されます。"^"All tracks on the album will be deleted."^
s^"アルバムにあるすべてのトラックをDJ登録します。..よろしいですか？"^"All tracks on the album will be registered as DJs.\\nAre you sure?"^
s^"アルバムにあるすべてのトラックを削除します。"^"Delete all tracks in the album."^
s^"アルバムにあるすべてのフォトは削除されます。"^"All photos in the album will be deleted."^
s^"アルバムにあるトラックがいっぱいです。"^"There are a lot of tracks on the album"^
s^"アルバムにあるフォトがいっぱいです。"^"The album is full of Photos"^
s^"アルバムに保存しているフォトからPict Story"^"Pict Story from photos saved in the album"^
s^"アルバムを選択してください。"^"Please select an album."^
s^"アルバム内にあるすべてのフォトを削除します。"^"Delete all photos in the album."^
s^"アルバム内の全トラックをDJ登録します。"^"All tracks in the album will be registered as DJs."^
s^"アルバム内の全トラックをDJ登録解除します。"^"All tracks in the album will be unregistered as DJs."^
s^"アルバム名 :"^"Album Name:"^
s^"アルバム名を変更してください。"^"Please change the album name."^
s^"アルバム選択"^"Album selection"^
s^"アングル"^"Angle"^
s^"アングルはありません。"^"There are no angles."^
s^"アングル・ブロック直前のためアングル変更不可"^"Angle cannot be changed as it is just before the angle block"^
s^"アングル切り換え"^"Angle switching"^
s^"アンドララベラ"^"Andorra La Bella"^
s^"アンマン"^"Amman"^
s^"イースター島"^"Easter island"^
s^"いいえ"^"No"^
s^"いくつかのコンテンツは取込めませんでした。"^"Some content could not be imported."^
s^"いくつかのトラックが取り込めませんが"^"Some tracks cannot be imported"^
s^"いくつかのフォトが取り込めませんが"^"Some photos cannot be imported"^
s^"イジェフスク"^"Izhevsk"^
s^"イスタンブール"^"Istanbul"^
s^"イタリア語"^"Italian"^
s^"インストール中"^"Installing"^
s^"インターネットラジオ"^"Internet Radio"^
s^"ウィーン"^"Vienna"^
s^"ウィザードを中止しますか？"^"Do you want to cancel the wizard?"^
s^"ウィザードを終了し、再起動を行います。"^"Exit the wizard and restart."^
s^"ウィントフック"^"Windhoek"^
s^"ウェリントン"^"Wellington"^
s^"ウラジオストク"^"Vladivostok"^
s^"エラー"^"Error"^
s^"エラーが発生しました。"^"An error has occurred."^
s^"エラーが発生しました。　　　"^"An error has occurred.   "^
s^"エラーが発生しました。..ダビングを中止します。.%d."^"An error has occurred. \\nDubbing will be cancelled. [%d]"^
s^"エラーが発生しました。..ダビングを中止します。[%d]"^"An error has occurred.\\nDubbing will be cancelled. [%d]"^
s^"エラーが発生しました。..一度電源を切った後、再度電源を入れ直してください。"^"An error has occurred.\\nPlease turn off the power and then turn it on again."^
s^"エラーコードTERMINATEコード"^"Error code TERMINATE code"^
s^"エラーはありません"^"No errors"^
s^"エラー番号：   "^"Error number: "^
s^"エラー番号："^"Error number:"^
s^"エルサレム"^"Jerusalem"^
s^"エレバン"^"Yerevan"^
s^"エンコーダ使用中です。..ダビングを中止します。。"^"Encoder in use.\\nDubbing will be cancelled."^
s^"エントリーポイントを追加しますか？"^"Add entry point?"^
s^"オーサリングエラー"^"authoring error"^
s^"オーサリングコンポーネント"^"authoring component"^
s^"オーディオ機器"^"Audio Device"^
s^"オスロ"^"Oslo"^
s^"オプション"^"Options"^
s^"おまかせ"^"Omakase"^
s^"おまかせキーワード :"^"Omakase Keyword"^
s^"おまかせ・まる録 :"^"Omakase/Maruroku:"^
s^"おまかせ・まる録"^"Omakase/Maruroku"^
s^"おまかせまる録"^"Omakase/Maruroku"^
s^"おまかせ・まる録 / おまかせキーワードユーザー作成"^"Omakase/Maruroku/Omakase Keyword User Creation"^
s^"おまかせ・まる録 チャンネル設定"^"Omakase/Maruroku Channel Settings"^
s^"おまかせ・まる録 ..チャンネル設定"^"Omakase/Maruroku \\nChannel settings"^
s^"おまかせ・まる録はすでに設定されています。"^"Omakase/Maruroku has already been set."^
s^おまかせ・まる録はすでに設定されています。^Omakase/Maruroku has already been set.^
s^"おまかせ・まる録はすでに設定されています。..新しく設定すると、現在の設定を消去して、..おまかせ・まる録 最大録画時間を初期の値に戻します。..よろしいですか？"^"Omakase/Maruroku has already been set.\\nSetting a new one will erase the current settings and\\nOmakase/Maruroku Returns the maximum recording time to the initial value.\\nIs it OK? "^
s^"おまかせ・まる録を利用しますか？"^"Would you like to use Omakase/Maruroku?"^
s^"おまかせ・まる録 ..最大録画時間"^"Omakase/Maruroku \\nMax Recording Time"^
s^"おまかせ・まる録 最大録画時間を初期の値に戻します。"^"Omakase/Maruroku Returns the Max recording time to the initial value."^
s^\^おまかせ・まる録 最大録画時間を初期の値に戻します。^Omakase/Maruroku Returns the Max recording time to the initial value.^
s^"おまかせ・まる録設定"^"Omakase/Maru record setting"^
s^"おまかせ・まる録設定"^"Omakase/Maru Record Settings"^
s^"おまかせ予約"^"Automatic reservation"^
s^"おまかせ予約"^"Omakase reservation"^
s^"おまかせ録画"^"Automatic recording"^
s^"オムスク"^"Omsk"^
s^"およそ"^"about"^
s^"およびそれぞれのロゴ、またメモリースティックは、"^"and their respective logos and memory sticks."^
s^"オランダ語"^"Dutch"^
s^"お住まいの地域を選択してください。"^"Please select your region."^
s^"お好みのキーワードを作成することができます。"^"You can create your own keywords."^
s^"お客様ご相談センターにお問い合わせください。"^"Please contact our customer service center."^
s^"お気に入り"^"Now Showing!"^
s^"お気に入りに追加"^"Add To Show"^
s^"お気に入りに追加できるアイテムがありません。"^"There are no items to add to favorites."^
s^"お気に入りを選択してください。"^"Choose your favorite."^
s^"ガイド"^"Guide"^
s^"ガイドチャンネル"^"Guide Channel"^
s^"ガイドチャンネルが重なっています。..設定値を確認してください。"^"Guide channels are overlapping.\\nPlease check the settings."^
s^"ガイドチャンネルが重なっています。..設定値を確認してください。"^"Guide channels overlap.\\nPlease check the settings."^
s^"カイロ"^"Cairo"^
s^"かかります。"^"takes."^
s^"カザフスタン中部"^"Central Kazakhstan"^
s^"カザフスタン東部"^"Eastern Kazakhstan"^
s^"カザフスタン西部"^"Western Kazakhstan"^
s^"カサブランカ"^"Wasablanca"^
s^"カテゴリー"^"Category"^
s^"カトマンズ"^"Kathmandu"^
s^"カナリア諸島"^"Canary Islands"^
s^"かな入力"^"Kana input"^
s^"カブール"^"Kabul"^
s^"カメラ :"^"Camera:"^
s^"カラカス"^"Caracas"^
s^"カラチ"^"Karachi"^
s^"カリーニングラード"^"Kaliningrad"^
s^"カルカッタ"^"Calcutta"^
s^"かんたん設定"^"Easy setup"^
s^"かんたん設定はシステムを現在の"^"Easy configuration to bring your system up to date"^
s^"かんたん設定を中止します。"^"Quick setup will be cancelled."^
s^"かんたん設定を中止します。..よろしいですか？"^"Quick setup will be cancelled.\\nAre you sure?"^
s^"かんたん設定を終了してください。"^"Please exit Easy Setup."^
s^"かんたん設定を終了します。"^"Easy Setup completed."^
s^"かんたん設定を開始します。"^"Start easy setup."^
s^"カントリー"^"Country"^
s^"キーバインド"^"Key Binding"^
s^"キーボードの設定"^"Keyboard settings"^
s^"キーリピートの速さ"^"Key Repeat Speed"^
s^"キーワード"^"Keyword"^
s^"キーワードを削除します。..よろしいですか？"^"Delete the keyword.\\nAre you sure?"^
s^"キーワード名"^"Keyword name"^
s^"キエフ"^"Kyiv"^
s^"キャンセル"^"Cancel"^
s^"キャンセルに失敗しました。"^"Cancellation failed."^
s^"キャンセル中…"^"Cancelling…"^
s^"クアラルンプール"^"kuala lumpur"^
s^"クイックタイマー設定"^"Quick timer settings"^
s^"クイックタイマー設定はできませんでした。"^"Quick timer settings could not be made."^
s^"クウェート"^"Kuwait"^
s^"クラシック"^"Classic"^
s^"グリーンランド北西部"^"Northwest Greenland"^
s^"グリーンランド南西部"^"Southwestern Greenland"^
s^"グリーンランド東部"^"Eastern Greenland"^
s^"クリア"^"Clear"^
s^"クリップ名"^"Clip Name"^
s^"グループサウンズ"^"Group Sounds"^
s^"ゲーム"^"Game"^
s^"ゲームの設定"^"Game settings"^
s^"ゲームプレイ機能"^"Gameplay features"^
s^"ゲームを起動しています。しばらくお待ちください。"^"Starting the game. Please wait."^
s^"ゲーム領域がありません。..HDD領域設定でゲーム領域を作成してください。..x-DJを終了します。"^"There is no game area.\\nPlease create a game area in the HDD area settings.\\nx-DJ will exit."^
s^"ゲーム領域が無いためその操作はできません。..HDD領域設定にてゲーム領域を作成してください。"^"This operation cannot be performed because there is no game area.\\nPlease create a game area in the HDD area settings."^
s^"ゲーム領域が無いため起動できません..HDD領域設定にてゲーム領域を作成してください。"^"Cannot start because there is no game area\\nPlease create a game area in the HDD area settings."^
s^"ゲーム領域が無いため起動できません..HDD領域設定にてゲーム領域を作成してください。"^"Unable to start because there is no game area\\nPlease create a game area in the HDD area settings."^
s^"ゲーム領域の容量が不足しています。..不要なトラックのＤＪ登録を解除するか、..ゲームを削除してください。"^"There is not enough space in the game area.\\nPlease cancel the DJ registration of unnecessary tracks or delete the game."^
s^"ゲーム領域を作成すると、番組の録画時間が"^"If you create a game area, the program recording time"^
s^"ゲーム領域を削除すると、ハードディスク内のゲームや、"^"Deleting the game area will delete the games on your hard disk,"^
s^"コア"^"Core"^
s^"ここで手動チャンネル設定を中止しますか？"^"Are you sure you want to cancel manual channel configuration now?"^
s^"このPict Storyで使用されているアルバムやフォトを"^"Albums and photos used in this Pict Story"^
s^"このPict Storyで使用しているアルバムや"^"Albums used in this Pict Story"^
s^"このPict Storyを修正できません。　　"^"This Pict Story cannot be modified."^
s^"このアルバムから作成したPict Storyがあります。"^"I have a Pict Story created from this album."^
s^"このアルバムにはこれ以上取り込めません。"^"I can't fit any more into this album."^
s^"このアルバムはスライドショーができません。"^"This album cannot be used as a slide show."^
s^"このアルバム名はすでに使用されています。取込みを続けると上書きされます。"^"This album name is already in use. It will be overwritten if you continue importing."^
s^"このゲームは起動できません。"^"This game cannot start."^
s^"このゲームは起動時にディスクが必要です。"^"This game requires a disc to start."^
s^"このゲームをハードディスクから削除します。"^"Delete this game from your hard disk."^
s^"このタイトルではVIEWERはできません。"^"VIEWER cannot be used with this title."^
s^"このタイトルでは操作できません。"^"This title cannot be operated."^
s^"このタイトルはダビングできません。"^"This title cannot be dubbed."^
s^"このタイトルは二カ国語音声を..主音声としてダビングします。..ダビングには、タイトルの再生時間と..同じくらいの時間がかかります。"^"This title will be dubbed with bilingual audio\\nas the main audio.\\nDubbing will take approximately the same amount of time as the title."^
s^"このタイトルは二カ国語音声を..副音声としてダビングします。..ダビングには、タイトルの再生時間と..同じくらいの時間がかります。"^"This title will be dubbed with bilingual audio\\nas secondary audio.\\nDubbing will take approximately the same amount of time as the title."^
s^"このタイトルは再生できません"^"This title cannot be played"^
s^"このタイトルをあとでDVDにダビングするときの目安です。"^"This is a guideline when dubbing this title to a DVD later."^
s^"このチャンネルを変更すると、番組表や..おまかせ・まる録が利用できなくなる場合があります。..よろしいですか？"^"If you change this channel, you may not be able to use the program guide or Omakase/MaruRoku.\\nAre you sure?"^
s^"このデータはコピーできません。"^"This data cannot be copied."^
s^"このデータは破損しています。"^"This data is corrupt."^
s^"このデータは移動できません。"^"This data cannot be moved."^
s^"このデータを移動しますか？"^"Do you want to move this data?"^
s^"このディスクではVIEWERはできません。"^"VIEWER cannot be used on this disc."^
s^"このディスクでは操作できません。"^"The operation cannot be performed on this disk."^
s^"このディスクには字幕はありません。"^"This disc has no subtitles."^
s^"このディスクはDVD+RWのため、..+VRモードで%sを行います。..ディスクに何か書き込まれている場合は、..その内容は削除されます。..よろしいですか?"^"Since this disc is a DVD+RW,\\nyou will perform %s in +VR mode.\\nIf there is anything written on the disc,\\nthat content will be deleted.\\nAre you sure? "^
s^"このディスクはDVD-Rのため、..Videoモードでダビングを行います。..よろしいですか?"^"Since this disc is a DVD-R,\\ndubbing will be done in Video mode.\\nAre you sure?"^
s^"このディスクはDVD+Rのため、..+VRフォーマットでダビングを行います。..よろしいですか?"^"Since this disc is DVD+R,\\nwe will dub in +VR format.\\nAre you sure?"^
s^"このディスクはDVD+Rのため、..+VRモードでダビングを行います。..よろしいですか?"^"Since this disc is DVD+R,\\ndubbing will be done in +VR mode.\\nAre you sure?"^
s^"このディスクは再生できません。"^"This disc cannot be played."^
s^"このディスクは初期化できません。..ダビング可能なディスクを挿入して..ください。"^"This disc cannot be initialized.\\nPlease insert a disc that can be dubbed."^
s^"このディスクは初期化できません。..初期化を終了します。"^"This disk cannot be initialized.\\nInitialization will end."^
s^"このディスクは取り込めません。"^"This disc cannot be imported."^
s^"このディスクは取込めません。"^"This disc cannot be imported."^
s^"このディスクは地域制限により再生を禁止されています。"^"Playback of this disc is prohibited due to regional restrictions."^
s^"このディスクは起動できません。"^"This disk cannot boot."^
s^"このトラックはDJ登録できません。"^"This track cannot be registered as a DJ."^
s^"このトラックを削除します。"^"Delete this track."^
s^"このトラックを削除します。よろしいですか？"^"Delete this track. Are you sure?"^
s^"このフォトは再生できません。"^"This photo cannot be played."^
s^"このフォトは操作できません。"^"This photo cannot be manipulated."^
s^"このフォトを削除します。"^"Delete this photo."^
s^"このフォトを取り込み"^"Import this photo"^
s^"このフォルダにあるすべてのデータを..削除します。..よろしいですか？"^"\nDelete all data in this folder.\\nAre you sure?"^
s^"このムービーを削除します。"^"Delete this movie."^
s^"このメモリーカードには..コピーできません。"^"You cannot copy\\nto this Memory Card."^
s^"このメモリーカードには..移動できません。"^"Cannot be moved to this memory card."^
s^"この操作は実行できません。"^"This operation cannot be performed."^
s^"この時間に別の予約が既に入っています。"^"I already have another reservation for this time."^
s^"この画像をサムネイルに"^"Make this image a thumbnail"^
s^"この語句は使用できません。"^"This phrase is not allowed."^
s^"コピー"^"Copy"^
s^"コピーしています。"^"Copying."^
s^"コピーしました。"^"Copy complete."^
s^"コピーに失敗しました。"^"Copy failed."^
s^"コピーに失敗しました。メモリーカードがありません。"^"Copy failed. No memory card."^
s^"コピー不可"^"Cannot be copied"^
s^"コピー不可 移動不可"^"Cannot copy, cannot move"^
s^"コピー先がありません。"^"There is no copy destination."^
s^"コピー先のデータを上書きしますか？"^"Do you want to overwrite the data at the copy destination?"^
s^"コピー先のフォルダ名が重複しています。"^"The copy destination folder name is duplicated."^
s^"コピー先の空き容量が不足しています。"^"There is not enough free space on the copy destination."^
s^"コピー先を選択してください。"^"Please select a destination."^
s^"コピー可能なデータがありません。"^"No data available to copy."^
s^"コピを中止しました。"^"Copying has been cancelled."^
s^"コペンハーゲン"^"Copenhagen"^
s^"コミュニケーション"^"Friends"^
s^"これまでの設定を保存しますか？"^"Do you want to save your current settings?"^
s^"これよりメニューを作成します。..画面が自動で切り替わりますが異常じゃないのです。"^"We will now create a menu.\\nThe screen will change automatically, but this is normal."^
s^"これ以上、Pict Storyを作成できません。"^"No more Pict Stories can be created."^
s^"これ以上タイトルを選択できません。..このタイトルはダビングできません。"^"No more titles can be selected.\\nThis title cannot be dubbed."^
s^"これ以上ビデオタイトルを作成できません。"^"Cannot create any more video titles."^
s^"これ以上新しいアイテムを追加できません。追加を中止します。"^"Cannot add any more new items. Additions will be stopped."^
s^"これ以上新しいアルバムを作成できません。ほかのアルバムを選択してください。"^"You cannot create any more new albums. Please choose another album."^
s^"これ以上新しいお気に入りを作成できません。ほかのお気に入りを選択してください。"^"You cannot create any more new favorites. Please select another favorite."^
s^"コンテンツ数 :"^"Album Size::"^
s^"ご注意"^"please note"^
s^"サーチ ＋"^"Search +"^
s^"サーチ －"^"Search -"^
s^"サーチ"^"Search"^
s^"サイズ :"^"Size:"^
s^"サウンドトラック"^"Soundtrack"^
s^"ザグレブ"^"Zagreb"^
s^"サッカー"^"Soccer"^
s^"サマータイム："^"Summer Time:"^
s^"サマータイム設定"^"Summer time setting"^
s^"サムネイルを登録しました。"^"Thumbnail has been registered."^
s^"サムネイル情報が取得できませんでした。"^"Failed to obtain thumbnail information."^
s^"サムネイル登録 "^"Thumbnail registration"^
s^"サムネイル登録できません。"^"Unable to register thumbnail."^
s^"サモア諸島"^"Samoan Islands"^
s^"サラエボ"^"Sarajevo"^
s^"サンティアゴ"^"Santiago"^
s^"サンプルPict Story"^"Sample Pict Story"^
s^"サンプル"^"Sample"^
s^"サンプル x-Pict Story"^"Sample x-Pict Story"^
s^"サンプルアルバム"^"Sample Album"^
s^"サンマリノ"^"San Marino"^
s^"システムドライバー(PlayStation®2)"^"System driver (PlayStation®2)"^
s^"してください。"^"Please"^
s^"シドニー"^"Sydney"^
s^"しない"^"No"^
s^"しばらくお待ちください。"^"Please wait."^
s^"しばらくお待ち下さい"^"Please wait for a little while"^
s^"しばらく時間をおいてから再接続してください。"^"Please wait a while and reconnect."^
s^"ジブラルタル"^"Gibraltar"^
s^"ジャズ"^"Jazz"^
s^"ジャストクロック"^"Automatically set"^
s^"シャッフル"^"shuffle"^
s^"シャッフル再生"^"Shuffle Playback"^
s^"ジャンル :"^"Genre:"^
s^"ジャンル"^"Genre"^
s^"ジャンル / サブジャンル"^"Genre/Subgenre"^
s^"ジャンルなし"^"No genre"^
s^"ジャンルを正しく設定してください。"^"Please set the genre correctly."^
s^"ジャンル別"^"By genre"^
s^"ジョイスティックの左右で行います。"^"Do it with the left and right sides of the joystick."^
s^"シンガポール"^"Singapore"^
s^"スウェーデン語"^"Swedish"^
s^"スコピエ"^"Skopje"^
s^"ステータス"^"Status"^
s^"ステレオ1"^"Stereo 1"^
s^"ステレオ2"^"Stereo 2"^
s^"ステレオ"^"Stereo"^
s^"ストックホルム"^"Stockholm"^
s^"スバ"^"Suva"^
s^"スペイン語"^"Spanish"^
s^"すべて"^"All"^
s^"すべてのトラック"^"All Track"^
s^"すべてのフォト"^"All Photo"^
s^"すべての設定を出荷時の状態に戻します。..よろしいですか？"^"Restore all settings to factory state.\\nAre you sure?"^
s^"すべて削除"^"Delete all"^
s^"スポーツ延長 :"^"Sports extension:"^
s^"スポーツ延長"^"Sports extension"^
s^"スライドショーBGM"^"Slideshow BGM"^
s^"スライドショー :"^"Slideshow:"^
s^"スライドショー"^"Slide Show"^
s^"スライドショーできるフォトがありません。"^"There are no Photos available for slideshow"^
s^"スライドショーの速さ"^"Slideshow speed"^
s^"スライドショー効果"^"Slideshow effect"^
s^"する"^"Yes"^
s^"スロー"^"Slow"^
s^"セーブデータ(PlayStation®2)"^"Save data (PlayStation®2)"^
s^"セーブデータ(PlayStation®)"^"Save data (PlayStation®)"^
s^"セーブデータ管理"^"Saved Data Utility"^
s^"セカンダリDNS :"^"Secondary DNS:"^
s^"セカンダリＤＮＳ"^"Secondary DNS"^
s^"セカンダリDNSの入力値を確認してください。"^"Please check the secondary DNS input value."^
s^"セキュリティの保護を可能にする株式会社ソニー・コンピュータエンタテインメント独自の"^"Sony Computer Entertainment Inc.'s unique technology that enables security protection"^
s^"セッションがありません。..ダビングを中止します。"^"There is no session.\\nDubbing will be cancelled."^
s^"ソート"^"Sort"^
s^"ソウル"^"Soul"^
s^"ソニー株式会社の商標または登録商標です。"^"It is a trademark or registered trademark of Sony Corporation."^
s^"その他"^"others"^
s^"その他の一部の鑑賞用の使用に制限されています。分解や、改造することも禁じられています。"^"Other limited use is limited to some viewing purposes. Disassembly or modification is also prohibited."^
s^"その他の国における商標または登録商標です。"^"It is a trademark or registered trademark in other countries."^
s^"その他記載されている会社名、製品名は各社の商標または登録商標です。"^"Other company and product names mentioned are trademarks or registered trademarks of each company."^
s^"ソフィア"^"Sofia"^
s^"ソフトウェアをインストールしています。"^"Installing software."^
s^"ソフトウェアをダウンロードしています。"^"Downloading software."^
s^"ソフトウェアをダウンロードしました。"^"Software downloaded."^
s^"タイトル.%d.においてデバッグエラーが出ました"^"Debug error occurred in title [%d]"^
s^"タイトル :"^"Title:"^
s^"タイトル"^"Title"^
s^"タイトルがいっぱいです。..不要なタイトルを削除してください。"^"It's full of titles. \\nPlease delete unnecessary titles."^
s^"タイトルがいっぱいです。..不要なタイトルを削除してください。"^"There are too many titles.\\nPlease delete unnecessary titles."^
s^"タイトルがいっぱいです。不要なタイトルを削除してください。"^"There are too many titles. Please delete unnecessary titles."^
s^"タイトルリスト"^"title list"^
s^"タイトルをDVDにダビングしています。"^"The title is being dubbed onto the DVD."^
s^"タイトルを削除しました。"^"Title removed."^
s^"タイトルを削除します。..よろしいですか？"^"Delete the title.\\nAre you sure?"^
s^"タイトル一覧"^"Title list"^
s^"タイトル名 :"^"Title Name:"^
s^"タイプ"^"Type"^
s^"タイムゾーン："^"Time Zone:"^
s^"タイムゾーンを選択し、→ボタンで次の画面に進んでください。"^"Select your time zone and press the → button to proceed to the next screen."^
s^"ダウンロードエラーです。"^"Download Error"^
s^"ダウンロード中"^"Downloading"^
s^"タシケント"^"Tashkent"^
s^"ただいまアップデートサーバーは大変混みあっているか、"^"The update server is very busy right now."^
s^"ダッカ"^"Dhaka"^
s^"ダビング"^"HDD → DVD dubbing"^
s^"ダビング :"^"Dubbing:"^
s^"ダビングが終了しました。"^"Dubbing finished."^
s^"ダビングするタイトルがありません。..ダビングを終了します。"^"There are no titles to dub.\\nDubbing will end."^
s^"ダビングできないディスクです。..ダビング可能なディスクを..挿入してください。"^"This disc cannot be dubbed.\\nPlease insert a disc that can be dubbed."^
s^"ダビングに失敗しました。"^"Dubbing failed."^
s^"ダビングを中止しました。"^"Dubbing has been cancelled."^
s^"ダビングを中止しました。..ダビングを終了します。"^"Dubbing has been cancelled.\\nDubbing will end."^
s^"ダビングを中止します。..よろしいですか？"^"Cancel dubbing.\\nAre you sure?"^
s^"ダビングを中止します。..中止すると、DVD-RおよびDVD+Rの場合は、..そのディスクを使用できなくなります。..よろしいですか?"^"Cancel dubbing.\\nIf you cancel,\\nfor DVD-R and DVD+R, you will no longer be able to use the disc.\\nAre you sure?"^
s^"ダビングを終了します。"^"End dubbing."^
s^"ダビングを開始します。..HH:MM AMに録画予約があります。..ダビング中、予約録画は実行できません。..よろしいですか？"^"Start dubbing.\\nThere is a recording reservation for HH:MM AM.\\nYou cannot perform scheduled recording while dubbing.\\nAre you sure?"^
s^"ダビングを開始します。..%sに録画予約があります。..ダビング中、録画予約は実行できません。..よろしいですか?"^"Start dubbing.\\nThere is a recording reservation for %s.\\nRecording reservation cannot be executed while dubbing.\\nAre you sure?"^
s^"ダビング中は本機の電源を切らないでください。"^"Do not turn off the power of this unit while dubbing."^
s^"ダビング可能なディスクを挿入し"^"Insert a dubbing disc"^
s^"ダビング開始"^"Start dubbing"^
s^"ダビング開始に失敗しました。"^"Failed to start dubbing."^
s^"ダブリン"^"Dublin"^
s^"ダマスカス"^"Damascus"^
s^"タリン"^"Tallinn"^
s^"ちがうタイプの奴は駄目です。"^"A different type of person is no good."^
s^"チャプター     （    ）"^"Channel  (  )"^
s^"チャプター"^"chapter"^
s^"チャプター"^"Chapter"^
s^"チャプターサーチエラーです。"^"Chapter search error."^
s^"チャプタースキャン表示"^"Chapter scan display"^
s^"チャプタースキャン表示失敗"^"Chapter scan display failure"^
s^"チャプタースキャン表示失敗終了"^"Chapter scan display failed"^
s^"チャプタースキャン表示終了"^"End of chapter scan display"^
s^"チャプター一覧"^"Chapter list"^
s^"チャプター数が多すぎるため、..このタイトルはダビングできません。"^"This title cannot be dubbed because there are too many chapters."^
s^"チャプター表示"^"Chapters"^
s^"チャンネル"^"Channel"^
s^"チャンネルの微調整"^"Fine tuning of channels"^
s^"チャンネルを切換えますが、よろしいですか？"^"Are you sure you want to change the channel?"^
s^"チャンネルを正しく設定してください。"^"Please set the channel correctly."^
s^"チャンネル設定を保存中です。"^"Saving channel settings."^
s^"チャンネル設定を保存中です。..しばらくお待ちください。"^"Saving channel settings.\\nPlease wait for a moment."^
s^"チャンネル飛ばし"^"Channel Skip"^
s^"チューナーを見るには操作パネルから"^"To view the tuner, use the control panel"^
s^"チューニング"^"Tuning"^
s^"チュニス"^"Tunisa"^
s^"チルドレン"^"Children"^
s^"チワワ"^"Chihuahua"^
s^"データCD"^"Data CD"^
s^"データがありません"^"No Items"^
s^"データをコピーしています。"^"Copying data."^
s^"データを削除しています。"^"Deleting data."^
s^"データを移動しています。"^"Moving data."^
s^"データ数 :"^"The number of data :"^
s^"でDVDメニューの種類を選択し"^"Select the DVD menu type with"^
s^"ディスク"^"Disc"^
s^"ディスク :"^"Disk:"^
s^"ディスクが入っていません。"^"There is no disc in it."^
s^"ディスクが入っていません。..ダビング可能なディスクを..挿入してください。"^"No disc is inserted.\\nPlease insert a disc that can be dubbed."^
s^"ディスクが読みとれません。"^"Unable to read disk."^
s^"ディスクが違います。"^"Wrong disc."^
s^"ディスクタイトル名"^"Disc title name"^
s^"ディスクチェックが終了しました。"^"Disk check completed."^
s^"ディスクチェックの開始に失敗しました。"^"Failed to start disk check."^
s^"ディスクチェック中"^"Checking disk"^
s^"ディスクに書き込み中は予約録画は動きません。"^"Scheduled recording will not work while writing to disc."^
s^"ディスクの読み込みに失敗しました。"^"Failed to read disk."^
s^"ディスクフォーマットが不正です。"^"The disk format is invalid."^
s^"ディスクを挿入してください。"^"Please insert the disc."^
s^"ディスク残量 100%"^"Disk remaining 100%"^
s^"ディスク種類 :"^"Disc type:"^
s^"ディスコ"^"Disco"^
s^"ティファナ"^"Tijuana"^
s^"ティラナ"^"Tirana"^
s^"でお住まいの地域を選んで"^"Select your region"^
s^"できません。"^"Not allowed"^
s^"テクノ"^"Techno"^
s^"デジタルカメラ"^"Digital Camera"^
##s^"デジタルカメラ"^"USB Storage"^
s^"でダビングするタイトルを選択し"^"Select the title to dub with"^
s^"デフォルトルータ :"^"Default Router:"^
s^"デフォルトルータ"^"Default Router"^
s^"デフォルトルータの入力値を確認してください。"^"Please check the default router input."^
s^"デフォルトルータ、またはネットマスクの"^"Default Router or Subnet Mask"^
s^"テヘラン"^"Teheran"^
s^"テレビ"^"TV"^
s^"テレビタイプ："^"TV Type:"^
s^"テレビタイプを設定し、→ボタンで次の画面に進んでください。"^"Please set the TV type and proceed to the next screen with the → button."^
s^"テレビの設定"^"TV settings"^
s^"テレビの設定 / 手動チャンネル設定 / チャンネルの微調整"^"TV settings / manual channel settings / fine-tuning channels"^
s^"テレビ画面の縦横比を選択してください。"^"Please select the aspect ratio of your TV screen."^
s^"デンマーク語"^"Danish"^
s^"で作成するDVDメニューを選択し"^"Select the DVD menu to create"^
s^"で取消す編集範囲を選び、"^" to select the edit range to cancel,"^
s^"↑↓で変更する項目を選択してください。"^"Please select the item to change using ↑↓."^
s^"で時刻の設定方法を選び"^"Select the time setting method with"^
s^"で項目を選択し、値を変更してください。"^"Please select the item and change the value."^
s^"ドイツ語"^"German"^
s^"トップメニュー"^"Top Menu"^
s^"トビリシ"^"Tbilisi"^
s^"ドライブはロックしています。..ダビングを中止します。"^"The drive is locked.\\nDubbing will be cancelled."^
s^"トラック %d"^"Track:"^
s^"トラック %i を取込んでいます。"^"Importing track %i."^
s^"トラック :"^"Track:"^
s^"トラック"^"Track"^
s^"トラック："^"Track:"^
s^"トラック名 :"^"Track name:"^
s^"トラック名を変更してください。"^"Please change the track name."^
s^"ドラム&ベース"^"Drum \& Bass"^
s^"ドラム＆ベース"^"Drum \& Bass"^
s^"トランス"^"Trance"^
s^"ドルビーデジタル :"^"Dolby Digital"^
s^"ドルビーデジタル"^"Dolby Digital"^
s^"ドルビーデジタル："^"Dolby Digital:"^
s^"ドルビーデジタルの設定を選択してください。"^"Please select your Dolby Digital settings."^
s^"ドルビーデジタルを設定し、→ボタンで次の画面に進んでください。"^"Please set Dolby Digital and proceed to the next screen with the → button."^
s^"ドルビーデジタル設定"^"Dolby Digital Settings"^
s^"トレーが閉じられません。"^"Tray cannot be closed."^
s^"なし"^"None"^
s^"なにやらエラーが発生した模様です。"^"It seems that an error has occurred."^
s^"ニコシア"^"Nicosia"^
s^"ニューウェーブ"^"New Wave"^
s^"ニューエイジ"^"New Age"^
s^"ニューカレドニア"^"New Caledonia"^
s^"ニューファンドランド標準時 (カナダ)"^"Newfoundland Standard Time (Canada)"^
s^"ニューミュージック"^"New Music"^
s^"に保存しました。"^"Saved to."^
s^"に変えますか？"^"Do you want to change it to?"^
s^"に録画予約があります。"^"There is a recording reservation for."^
s^"に録画予約が存在します。..最適化中、録画予約は実行できません。"^"Recording reservations exist.\\nRecording reservations cannot be executed while optimization is in progress."^
s^"ネットマスク :"^"Subnet Mask:"^
s^"ネットマスク"^"Subnet Mask"^
s^"ネットマスクの入力値を確認してください。"^"Please check the netmask input value."^
s^"ネットワークアップデート"^"Network Update"^
s^"ネットワークケーブルを接続してください。"^"Please connect the network cable."^
s^"ネットワークに接続できません。"^"Unable to connect to network."^
s^"ネットワークに接続できませんでした。"^"Unable to connect to network."^
s^"ネットワークの接続テストを行います。"^"Test network connection."^
s^"ネットワークの設定 / DNSの設定"^"Network settings/DNS settings"^
s^"ネットワークの設定 / IPアドレスの設定"^"Network settings/IP address settings"^
s^"ネットワークの設定"^"Network settings"^
s^"ネットワークの設定 / PPPoEの設定"^"Network settings / PPPoE settings"^
s^"ネットワークの設定を確認してください。"^"Please check your network settings."^
s^"ネットワークの設定 / 接続テスト"^"Network settings/Connection test"^
s^"ネットワークの設定 / 設定内容一覧"^"Network settings/List of settings"^
s^"ネットワークの設定 / 設定完了"^"Network settings/Setup complete"^
s^"ネットワークの設定 / 設定開始"^"Network settings/Start settings"^
s^"ネットワークへの接続に成功しました。"^"Successfully connected to the network."^
s^"ネットワーク接続を中断しました。"^"Network connection interrupted."^
s^"ネットワーク設定を中止します。"^"Network settings will be cancelled."^
s^"ネットワーク設定を確認してください。"^"Please check your network settings."^
s^"ネットワーク設定を開始します。"^"Start network configuration."^
s^"ノリリスク"^"Norilsk"^
s^"ノルウェー語"^"Norwegian"^
s^"バージョン :"^"Version:"^
s^"パース"^"Perth"^
s^"ハードディスクがいっぱいです。..不要なタイトルを削除してください。"^"Hard disk is full.\\nPlease delete unnecessary titles."^
s^"ハードディスクに十分な空き容量がありません。"^"There is not enough free space on your hard disk."^
s^"ハードディスクに問題がある可能性があります。"^"There may be a problem with your hard disk."^
s^"ハードディスクに空き容量がありません。不要なタイトルやトラック、フォトを削除してください。"^"There is no free space on your hard disk. Please delete unnecessary titles, tracks, and photos."^
s^"ハードディスクの容量が不足しています。"^"There is not enough hard disk space."^
s^"ハードディスクの容量が不足しています。..不要なタイトルやトラック、フォトを削除してください。"^"Hard disk space is insufficient. \\nPlease delete unnecessary titles, tracks, and photos."^
s^"ハードディスクの容量が不足しています。..不要なタイトルやトラック、フォトを削除してください。"^"There is not enough hard disk space.\\nPlease delete unnecessary titles, tracks, and photos."^
s^"ハードディスクの容量が不足しているか、"^"Is there insufficient hard disk space?"^
s^"ハードディスクの容量が不足しているため、..DVDメニューを作成することができません。..DVDメニューを作成する場合はダビングを中止して、..不要なタイトルをハードディスクから削除してください。"^"Due to insufficient hard disk space,\\nit is not possible to create a DVD menu.\\nIf you wish to create a DVD menu, please stop dubbing and\\ndelete unnecessary titles from your hard disk."^
s^"ハードディスクの容量が不足しているため、..ダビングを開始することができません。..ダビングを中止して、不要なタイトルを..ハードディスクから削除してください。"^"Unable to start dubbing because there is not enough hard disk space.\\nPlease stop dubbing and delete unnecessary titles from the hard disk."^
s^"ハードディスクの容量に空きが無いため、..DVDメニューを作成することができません。..DVDメニューを作る場合はダビングを中止して、..不要なタイトルをハードディスクから..削除して下さい。"^"Unable to create a DVD menu because there is no free space on the hard disk.\\nIf you want to create a DVD menu, please stop dubbing and delete unnecessary titles from the hard disk."^
s^"ハードディスクの容量に空きが無いため、..ダビングを開始することができません。..ダビングを中止して、..不要なタイトルをハードディスクから..削除して下さい。"^"Unable to start dubbing because there is no free space on the hard disk.\\nPlease stop dubbing and delete unnecessary titles from the hard disk."^
s^"ハードディスクの映像をDVDにダビングします。"^"Dubbing footage from your hard disk onto a DVD."^
s^"ハードディスクの空き容量が不足しているため　　　　　"^"Due to insufficient free space on the hard disk."^
s^"ハードディスクの空き容量を確認しています。"^"Checking free space on your hard disk."^
s^"ハードディスクの空き容量を確認しています。.*しばらくお待ちください。.*本体の電源を切らないでください。"^"Checking the free space on your hard disk.\\nPlease wait for a moment.\\nPlease do not turn off the device."^
s^"ハードディスクの空き容量を確認しています。..しばらくお待ちください。..本体の電源を切らないでください。"^"Checking the free space on your hard disk.\\nPlease wait for a moment.\\nPlease do not turn off the device."^
s^\^・ハードディスクを使用するゲーム^・Game data stored on the Hard Disk^
s^"・ハードディスクを使用するゲーム"^"・Games that use hard disks"^
s^"ハードロック"^"Hard Rock"^
s^"はい"^"Yes"^
s^"ハウス"^"House"^
s^"バクー"^"Baku"^
s^"バグダッド"^"Baghdad"^
s^"パスワード :"^"Password:"^
s^"パスワード"^"Password"^
s^"パナマシティ"^"Panama City"^
s^"・ハブ/ルータとモデム/ONU間を"^"・Between hub/router and modem/ONU"^
s^"バラエティ"^"Variety"^
s^"パリ"^"Paris"^
s^"バレッタ"^"Valletta"^
s^"パレンタルレベルを変更します"^"Change parental level"^
s^"パレンタルレベル設定"^"Parental level settings"^
s^"ハワイ"^"Hawaii"^
s^"パンク"^"Punk"^
s^"バンコク"^"Bangkok"^
s^"ぴーでぃ"^"PDC"^
s^"ピクチャーメモリ保存"^"Save picture memory"^
s^"ピクチャーメモリ終了"^"End picture memory"^
s^"ビジュアライザー"^"visualizer"^
s^"ビシュケク"^"Bishkek"^
s^"ビットレート :"^"Bitrate:"^
s^"ヒップホップ"^"Hip-hop"^
s^"ビデオ"^"Video"^
s^・ビデオ^・Video^
s^"ビデオタイトルの最適化中のため"^"Video title is being optimized"^
s^"ビデオの設定"^"Video settings"^
s^"ビデオの設定"^"Video Settings"^
s^"ビデオの設定 / おまかせ・まる録 チャンネル設定"^"Video settings / Omakase/Maruroku channel settings"^
s^"ビデオ作成"^"Video creation"^
s^"ビデオ作成　　　　"^"Video creation"^
s^"ビデオ作成してください。　　　"^"Please create a video."^
s^"ビデオ作成してください。　　　　　　"^"Please create a video."^
s^"ビデオ作成してください。　　　　　　　　"^"Please create a video."^
s^"ビデオ作成できません。　　　"^"Unable to create video."^
s^"ビデオ作成できません。　　　　"^"Unable to create video."^
s^"ビデオ作成できません。　　　　　　　　　　　　　　　"^"Unable to create video."^
s^"ビデオ作成中は.停止.以外は操作できません。"^"While creating a video, you cannot perform any \\noperations other than [Stop]."^
s^"ビデオ作成中は録画予約を実行できません。　"^"You cannot schedule a recording while creating a video."^
s^"ビデオ作成開始"^"Start creating video"^
s^"ビリニュス"^"Vilnius"^
s^"ファーストプレイを実行"^"Run first play"^
s^"ファイナライズ"^"finalize"^
s^"ファイナライズするセッションが指定されていません。"^"No session specified to finalize."^
s^"ファイナライズするディスクを挿入を確認して下さい。"^"Please confirm that you have inserted the disc you want to finalize."^
s^"ファイナライズの解除に失敗しました。..ダビングを中止します。"^"Failed to release finalization.\\nDubbing will be cancelled."^
s^"ファイナライズを中止します。..中止すると、DVD-RおよびDVD+Rの場合は、..そのディスクを使用できなくなります。..よろしいですか?"^"Cancel finalization.\\nIf you cancel, for DVD-R and DVD+R,\\nthe disc will become unusable.\\nAre you sure?"^
s^"ファイナライズ中…"^"Finalizing..."^
s^"ファイナライズ開始に失敗しました。"^"Failed to start finalization."^
s^"ファイル %d"^"File:"^
s^"ファイル :"^"File:"^
s^"ファイル"^"Files"^
s^"ファイルが壊れています。..編集できません。"^"The file is corrupt.\\nCannot be edited."^
s^"ファイル名 :"^"File Name:"^
s^"ファイル形式 :"^"File Format:"^
s^"ファイル形式"^"Image Type"^
s^"ファイル数 :"^"number of files :"^
s^"ファイル番号がおかしいです。.-9."^"The file number is incorrect. [-9]"^
s^"ファドゥーツ"^"Vaduz"^
s^"ぶいぴー"^"VPS"^
s^"フィンランド語"^"Finnish"^
s^"プエルトリコ"^"Puerto Rico"^
s^"フォーク"^"Folk"^
s^"フォーマット"^"format"^
s^"フォト %d"^"Photo:"^
s^"フォト %i を取込んでいます。"^"Importing photo %i."^
s^"フォト :"^"Photo:"^
s^"フォト"^"Photo"^
s^\^・フォト^・Photos^
s^"フォトがありません。　　　　　　　　　"^"There are no Photos."^
s^"フォトの設定"^"Photo settings"^
s^"フォトの読み込みに失敗しました。　　　"^"Failed to load photo."^
s^"フォルダ %d"^"Folder:"^
s^"フォルダ :"^"Folder:"^
s^"フォルダ"^"Folder"^
s^"フォルダを作成するための空き容量が不足しています。"^"There is not enough free space to create a folder."^
s^"フォルダ作成"^"Create Folder"^
s^"フォルダ名 :"^"Folder name :"^
s^"フォルダ名を入力してください。"^"Please enter the folder name."^
s^"フォルダ数 :"^"Number of folders:"^
s^"ブカレスト"^"Bucharest"^
s^"ブダペスト"^"Budapest"^
s^"ブックマークできません"^"Cannot bookmark"^
s^"ブックマーク表示"^"Bookmark display"^
s^"ブックマーク設定"^"Bookmark settings"^
s^"フュージョン"^"Fusion"^
s^"プライア"^"Praia"^
s^"プライマリDNS :"^"Primary DNS:"^
s^"プライマリＤＮＳ"^"Primary DNS"^
s^"プライマリDNSの入力値を確認してください。"^"Please check the primary DNS input value."^
s^"ブラチスラバ"^"Bratislava"^
s^"ブラツーク"^"Bratsk"^
s^"フラッシュ ＋"^"Flash +"^
s^"フラッシュ ＋"^"Flash+"^
s^"フラッシュ －"^"Flash -"^
s^"フラッシュ"^"Flash"^
s^"プラハ"^"Prague"^
s^"フランス語"^"French"^
s^"ブリュッセル"^"Brussels"^
s^"ブレークビーツ"^"Break Beats"^
s^"フレーム"^"Flame"^
s^"プレイしてからじゃないと無理。"^"You can't do it until you play."^
s^"プレイリスト一覧"^"Playlist list"^
s^"プロキシの設定にエラーがあります。"^"There is an error in your proxy settings."^
s^"プログラムから削除"^"Remove from program"^
s^"プログラムから削除しました。"^"Removed from program."^
s^"プログラムに追加"^"Add to program"^
s^"プログラムに追加しました。"^"Added to program."^
s^"プログラムの一覧"^"List of programs"^
s^"プログラム削除"^"Program deletion"^
s^"プログラム検索は禁止されています。"^"Program search is prohibited."^
s^"プログレッシブ"^"Progressive"^
s^"プログレッシブ出力"^"Progressive output"^
s^"プロテクト"^"Protect"^
s^"プロテクト :"^"Protected:"^
s^"プロテクトされています。"^"Protected."^
s^"プロテクトされています。..削除できません。"^"Protected.\\nCannot be deleted."^
s^"プロテクトされています。..名前変更できません。"^"Protected.\\nCannot be renamed."^
s^"プロテクトされています。..編集できません。"^"Protected.\\nCannot be edited."^
s^"プロテクトされています。..編集取消しできません。"^"Protected.\\nEditing cannot be undone."^
s^"プロテクト解除"^"Unprotect"^
s^"プロトコルエラーです。"^"Protocol error"^
s^"ベイルート"^"Beirut"^
s^"ベオグラード"^"Belgrade"^
s^"ペトロパブロフスク・カムチャツキー"^"Petropavlovsk-Kamchatsky"^
s^"ヘルシンキ"^"Helsinki"^
s^"ヘルプ"^"Help"^
s^"ペルミ"^"Perm"^
s^"ベルリン"^"Berlin"^
s^"ベルン"^"Bern"^
s^"ほかのPict Storyを選択してください。"^"Please select another Pict Story."^
s^"ほかのアルバムを選択してください。"^"Please select another album."^
s^"ほかのアルバムを選択してください。　　　　"^"Please select another album."^
s^"ポップス"^"Pops"^
s^"ポルトガル語"^"Portuguese"^
s^"マーク設定しました。"^"Mark set."^
s^"マガダン"^"Magadan"^
s^"マクロビジョン社の許可が必要で、また、マクロビジョン社の特別な許可がない限り家庭用および"^"Requires permission from Macrovision and for home use only unless specifically approved by Macrovision."^
s^"マスカット"^"Muscat"^
s^"また、&quot;DNAS&quot;は同社の商標です。"^"In addition, DNAS is a trademark of the company."^
s^"まだ編集されていません。..編集取消しはできません。"^"It has not been edited yet.\\nYou cannot undo the edit."^
s^"また、編集範囲内に設定したチャプターマークは"^"Also, chapter marks set within the editing range are "^
s^"マドリード"^"Madrid"^
s^"マナーマ"^"Manama"^
s^"まもなく、おまかせ録画の開始時刻です。"^"It's almost time for automatic recording to start."^
s^"まもなく番組情報の取得を開始します。"^"We will begin retrieving program information shortly."^
s^"まもなく番組情報の取得を開始します。..現在の操作を中止します。"^"We will begin retrieving program information soon.\\nThe current operation will be cancelled."^
s^"マルチ画面表示"^"Multi-screen display"^
s^"マルチ画面表示キャンセル"^"Cancel multi-screen display"^
s^"ミックス"^"Mix"^
s^"ミッドウェー諸島"^"Midway Islands"^
s^"ミュージック"^"Music"^
s^\^・ミュージック^・Music^
s^"ミュージックの設定"^"Music settings"^
s^"ミュージック、フォトの一部の機能が利用できなくなります。"^"Some functions of Music and Photos will no longer be available."^
s^"ミンスク"^"Minsk"^
s^"ムービー :"^"Movie:"^
s^"メキシコシティ"^"Mexico City"^
s^"メディア :"^"Media"^
s^"メディアのサイズが分かりません。"^"I don't know the size of the media."^
s^"メニュー"^"Menu"^
s^"メニューがありません。"^"There's no menu."^
s^".*メニューテンプレート.*がありません。"^"%$3: Menu template '%3$s' does not exist."^
s^"メニューテンプレートがありません。..ダビングを中止します。"^"There is no menu template.\\nDubbing will be cancelled."^
s^".*メニューテンプレート.*に項目 .* がありません。"^"%$3: Item %1$s is missing from menu template '%3$s'."^
s^"メニューを作成しません"^"Do not create menu"^
s^"メニュー項目がありません。..ダビングを中止します。"^"There are no menu items.\\nDubbing will be cancelled."^
s^"メモリーカードの初期化"^"Initialize memory card"^
s^"メモリーカードを初期化しました。"^"Memory card has been initialized."^
s^"メモリーカードを抜かないでください。"^"Please do not remove the memory card."^
s^"メモリースティック"^"Memory Stick®"^
s^"メモリースティックが正しく差し込まれているか確認してください。"^"Please make sure the memory stick is inserted correctly."^
s^"メモリ割当エラー。"^"Memory allocation error."^
s^"メンテナンスのため、利用できません。"^"Unavailable due to maintenance."^
s^"モード"^"Mode"^
s^"モードを選択してください。"^"Please select a mode."^
s^"モードを選択して下さい。"^"Please select a mode."^
s^"モード固定"^"Mode fixed"^
s^"もう一度アップデートしてください。"^"Please update again."^
s^"もう一度設定してください。"^"Please configure again."^
s^"モスクワ"^"Moscow"^
s^"モデム使用の場合はモデムをリセットしてください。"^"If you are using a modem, please reset the modem."^
s^"モナコ"^"Monaco"^
s^"ヤクーツク"^"Yakutsk"^
s^"ユーザーID :"^"User ID:"^
s^"ユーザーＩＤ"^"User ID"^
s^"ユーザーキーワードを登録してください。"^"Please register user keywords."^
s^"ユーザー作成"^"User creation"^
s^"ユーザー操作は禁止されています。"^"User interaction is prohibited."^
s^"ユーロビート"^"Euro beat"^
s^"ユニークIDエラーです。"^"Unique ID Error"^
s^"ヨハネスブルグ"^"Johannesburg"^
s^"よろしいですか ?"^"Are you sure?"^
s^"よろしいですか？"^"Are you sure?"^
s^"よろしいですか？　　　　　　"^"Are you Sure?"^
s^"よろしいですか?"^"Is it OK?"^
s^\^よろしいですか？^Is it OK?^
s^"よろしいですか？変更後 DVD-RW ( VRモード ) 以外へのダビングは"^"Are you sure? After the change, dubbing to anything other than DVD-RW (VR mode) is prohibited."^
s^"よろしいですか？設定後、サーチは"^"Are you sure? After setting, the search will start."^
s^"よろしいですか？設定後、フラッシュは"^"Are you sure? After setting, the flash will be "^
s^"よろしければ"^"If you are sure,"^
s^"ライブラリをロードできません。"^"Unable to load library."^
s^"ラテン"^"Latin"^
s^"リガ"^"Riga"^
s^"リジューム操作は禁止されています。"^"Resume operation is prohibited."^
s^"リスボン"^"Lisbon"^
s^"リターン"^"Return"^
s^"リピート"^"repeat"^
s^"リピート入"^"Repeat on"^
s^"リピート切"^"Repeat off"^
s^"リピート開始時間"^"Key Repeat Delay"^
s^"リモコンの設定"^"Remote control settings"^
s^"リモコンモード"^"Remote Control Mode"^
s^"リヤド"^"Riyadh"^
s^"リュブリャナ"^"Lubljana"^
s^"ルータ使用の場合はPPPoEを「使用しない」に"^"If using a router, set PPPoE to 'disable'"^
s^"ルクセンブルグ"^"Luxemburg"^
s^"レイキャビク"^"Reykjavik"^
s^"レゲエ/スカ"^"Reggae/Ska"^
s^"レゲエ／スカ"^"Reggae/Ska"^
s^"レユニオン島"^"Reunion Island"^
s^"ロードハウ島"^"Lord Howe Island"^
s^"ローマ"^"Rome"^
s^"ローマ字入力"^"Romaji input"^
s^"ロシア語"^"Russian"^
s^"ロック"^"Rock"^
s^"ロンドン"^"London"^
s^"ワード"^"Word"^
s^"ワールド"^"World"^
s^"ワルシャワ"^"Warsaw"^
s^"を作成、または、すでに作成したPict Storyを"^"Create a Pict Story, or a Pict Story you have already created."^
s^"を押してかんたん設定を"^"Press for easy setup"^
s^"を押してください"^"Please press"^
s^"を押してください。"^"Please press."^
s^"を押して再起動してください。"^"Press to restart."^
s^"を押して次の画面に移ってください。"^"Press to move to the next screen."^
s^"を押して次の画面へ"^"Press to go to the next screen"^
s^"を押すとダビングを開始します。"^"Press to start dubbing."^
s^"を押すとファイナライズを開始します。"^"Press to start finalizing."^
s^"を押すと、値を入力または変更できます。"^"Press to enter or change the value."^
s^"を押すと、値を変更できます。"^"Press to change the value."^
s^"を押すと接続テストを中止します。"^"Press to cancel the connection test."^
s^"を押すと、時刻を変更できます。"^"Press to change the time."^
s^"を押すと、編集画面に戻ります。"^"Press to return to the editing screen."^
s^"を押すと、選択できます。"^"Press to select."^
s^"一時停止"^"Pause"^
s^"一時停止モード"^"Pause mode"^
s^"一時削除状態のため再生できません(メッセージ無し)"^"Unable to play because it is temporarily deleted (no message)"^
s^"上に移動"^"Move Up"^
s^"上書きします。..よろしいですか？"^"Overwrite.\\nAre you sure?"^
s^"上書き保存する"^"Overwrite"^
s^"上書き確認"^"Confirm overwrite"^
s^"下に移動"^"Move Down"^
s^"不明"^"not clear"^
s^"不明なエラーが発生しました。"^"An unknown error has occurred."^
s^"不正なセッションです。..ダビングを中止します。"^"Invalid session.\\nDubbing will be cancelled."^
s^"不要なPict Storyを削除してください。　"^"Please delete unnecessary Pict Stories."^
s^"不要なタイトル、トラック、フォトを削除"^"Delete unnecessary titles, tracks, and photos"^
s^"不要なタイトル、トラック、フォトを削除してください。"^"Delete unnecessary titles, tracks, and photos."^
s^"不要なタイトルやトラック、フォトを削除してください。"^"Please delete unnecessary titles, tracks, and photos."^
s^"不要なタイトルを削除してください。　　　"^"Please delete unnecessary titles."^
s^"不要なデータを削除してください。"^"Please delete unnecessary data."^
s^"不要なフォルダを削除し、再度作成してください。"^"Please delete unnecessary folders and create them again."^
s^"並び替え順変更"^"Change the sort order"^
s^"並び順変更"^"Change Sort"^
s^"中国語"^"Chinese"^
s^"中断"^"interrupted"^
s^"中止"^"cancel"^
s^"中止しない"^"Do not cancel"^
s^"中止しました。"^"Cancelled."^
s^"中止する"^"Abort"^
s^"中部標準時 (カナダ)"^"Central Standard Time (Canada)"^
s^"中部標準時 (米国)"^"Central Standard Time (US)"^
s^"主/副"^"Main/Secondary"^
s^"主+副で録画されている音声をDVD-RW(Videoモード)、DVD-R、..DVD+RWディスクにどのようにダビングするか設定します。"^"Set how to dub the audio recorded in main + sub to DVD-RW (Video mode), DVD-R, \\nDVD+RW disc."^
s^"主+副音声"^"Both"^
s^"主音声"^"Main Audio"^
s^"予約はありません。"^"No reservations."^
s^"予約はありません。"^"There are no reservations."^
s^"予約修正"^"Reservation modification"^
s^"予約候補リスト"^"Reservation candidate list"^
s^"予約削除"^"Delete reservation"^
s^"予約削除できませんでした。"^"Failed to delete reservation."^
s^"予約削除できませんでした。"^"The reservation could not be deleted."^
s^"予約状況が変更されました。..操作を中止します。"^"Reservation status has been changed.\\nThe operation will be cancelled."^
s^"予約確定"^"Confirm"^
s^"予約録画が開始されました。..ダビングをつづけますか?....はい を選ぶと録画を停止してダビングを続けます。..いいえ を選ぶとダビングを終了します。"^"Scheduled recording has started.\\nDo you want to continue dubbing?\n\\nSelect Yes to stop recording and continue dubbing.\\nSelect No to end dubbing."^
s^"予約録画が開始されました。..最適化を終了します。"^"Scheduled recording has started.\\nOptimization will end."^
s^"予約録画を優先しました。"^"I prioritized scheduled recording."^
s^"二カ国語音声を主音声にします。..そのためにダビングに時間がかかります。"^"The bilingual audio will be the main audio.\\nThis will take time to dub."^
s^"二カ国語音声を副音声にします。..そのためにダビングに時間がかかります。"^"The bilingual audio will be made into a secondary audio.\\nThis will take time to dub."^
s^"二重音声"^"double voice"^
s^"二重音声"^"Dual Audio"^
s^"以下の予約と重なっています。"^"This reservation overlaps with the following reservation."^
s^"以下の内容で設定しました。"^"The following settings were made."^
s^"以下の文字は使用できません。"^"The following characters are not allowed."^
s^"以下の文字は利用できません。"^"The following characters are not allowed."^
s^"以下の設定でPict Storyを作成します。"^"Create a Pict Story with the following settings."^
s^"以下の項目を確認してください。"^"Please check the following items."^
s^"以下を設定しました。"^"The following has been set."^
s^"件"^"subject"^
s^"位置"^"position"^
s^"作成したPict Storyビデオを"^"Created Pict Story video"^
s^"作成しない"^"Don't create"^
s^"作成する"^"Create"^
s^"作成・修正選択"^"Create/modify selection"^
s^"作成日付 :"^"date of creation :"^
s^"作成日時 :"^"Updated:"^ # was "Creation date and time" - seen on the game information screen
s^"作成機器名 :"^"Creation device name:"^
s^"作成済"^"Created"^
s^"作業を進めてください。"^"Proceed with your work."^
s^"使用する語句を選択してください。"^"Please select the phrase you would like to use."^
s^"使用済みの領域"^"Used Space"^
s^"使用許諾に同意されませんでした。"^"You did not agree to the license agreement."^
s^"使用許諾に同意しますか？"^"Do you agree to the license agreement?"^
s^"使用許諾の内容を確認し、"^"Please check the license agreement and"^
s^"使用許諾契約"^"License Agreement"^
s^"俄文"^"濄文"^
s^"保存"^"Save"^
s^"保存して終了"^"Save and exit"^
s^"「保存して終了」を選択すると、次回から選択した"^"If you select ``Save and Exit,'' the selected item will be saved next time.''"^
s^"保存しないで終了"^"Exit without saving"^
s^"保存しない場合、変更前の設定に戻ります。"^"If you do not save, the settings will revert to the previous settings."^
s^"保存しました。"^"Saved."^
s^"保存      中止"^"Save Cancel"^
s^"保存資料(PlayStation®2)"^"Archived materials (PlayStation®2)"^
s^"保存資料(PlayStation®)"^"Archived materials (PlayStation®)"^
s^"修復作業中です。..しばらくお待ちください。"^"Repair is in progress.\\nPlease wait for a while."^
s^"修正"^"fix"^
s^"修正　　"^"Modify"^
s^"修正します。　　　　　　　　　　　　　　　"^"to correct.               "^
s^"停止"^"Stop"^
s^"停止は禁止されています。"^"Stopping is prohibited."^
s^"停止状態"^"State of standstill"^
s^"元のタイトル"^"Original title"^
s^"先頭プログラム検索"^"Top program search"^
s^"光デジタル出力"^"Optical digital output"^
s^"光デジタル出力："^"Optical digital output:"^
s^"光デジタル出力を使用しますか？"^"Would you like to use optical digital output?"^
s^"光デジタル出力を設定し、→ボタンで次の画面に進んでください。"^"Please set the optical digital output and proceed to the next screen with the → button."^
s^"光デジタル出力設定"^"Optical digital output setting"^
s^"入"^"On"^
s^"入力した値は無効です。"^"The value you entered is invalid."^
s^"入力した受信チャンネルは無効です。"^"The receive channel you entered is invalid."^
s^"入力した名前はすでに使用されているか..使用できない文字が含まれます。"^"The name you entered is already in use or contains invalid characters."^
s^"入力した名前は、すでに使用されているか、使用できない文字が含まれます。お気に入りの名前を変更してください。"^"The name you entered is already in use or contains invalid characters. Please change your favorite name."^
s^"入力した名前はすでに使用しています。"^"The name you entered is already in use."^
s^"入力した名前は無効か、すでに使用しています。"^"The name you entered is either invalid or already in use."^
s^"入力した名前は長すぎます。"^"The name you entered is too long."^
s^"入力する項目を選択してください。"^"Please select the item to enter."^
s^"入力または変更後、"^"After entering or changing,"^
s^"入力値を確認してください。"^"Please check your input values."^
s^"全ての&#34;DNAS&#34;サービスが終了しました。"^"All DNAS services have been terminated."^
s^"全てのキーワードを削除します。..よろしいですか？"^"Delete all keywords.\\nAre you sure?"^
s^"全体"^"Selection progress"^
s^"共有バッファ使用中です。..ダビングを中止します。"^"Shared buffer in use.\\nDubbing will be cancelled."^
s^"内容の表示"^"Display Content"^
s^"再ダビング可能なディスクを挿入し"^"Insert a re-dubbable disc"^
s^"再生"^"Play"^
s^"再生が制限されているため再生できません。"^"Cannot be played because playback is restricted."^
s^"再生したくない場面の始点で、"^"At the beginning of the scene you don't want to play,"^
s^"再生したくない場面の終点で、"^"At the end of the scene you don't want to play,"^
s^"再生できるトラックはありません。"^"There are no tracks available to play."^
s^"再生モードをクリアしました。"^"Playback mode cleared."^
s^"再生モードを実行できません。"^"Unable to run playback mode."^
s^"再生中です。"^"Playing now."^
s^"再生回数"^"Frequency"^
s^"再生時間 :"^"Playing time:"^
s^"再生時間"^"Playing time"^
s^"再生時間："^"Playing time:"^
s^"再起動"^"Reboot"^
s^"再起動後、ハードディスク領域設定を開始します。"^"After reboot, hard disk space configuration will begin."^
s^"再起動後、引き続きアップデートを行います。"^"Updates will continue after restart."^
s^"処理の中止が要求されました。..ダビングを中止します。"^"A request has been made to abort the process.\\nDubbing will be aborted."^
s^"出荷時の設定に戻しています。"^"Reverting to factory settings."^
s^"出荷時設定に戻しました。"^"Restored to factory settings."^
s^"出荷時設定に戻す"^"Restore to factory settings"^
s^"出荷時設定に戻すを中止します。..よろしいですか？"^"Cancel resetting to factory settings.\\nAre you sure?"^
s^"分"^"minute"^
s^"分"^"minutes"^
s^"分かかります。"^"It will take a minute."^
s^"分後"^"Minute after"^
s^"←分類→"^"←Classification→"^
s^"分類"^"Classification"^
s^"切"^"Off"^
s^"切らないでください。"^"Do not cut it, please."^
s^"初期化"^"Format"^
s^"初期化が完了しました。..初期化を終了します。"^"Initialization has completed.\\nInitialization will end."^
s^"初期化が終了しました。"^"Initialization completed."^
s^"初期化されていません。"^"Not initialized."^
s^"初期化しています。しばらくお待ちください。"^"Initializing. Please wait a moment."^
s^"初期化しますか？"^"Do you want to initialize?"^
s^"初期化すると、メモリースティックにあるすべてのデータが消去されます。"^"Initializing will erase all data on the memory stick."^
s^"初期化に失敗しました。"^"Initialization failed."^
s^"初期化を開始します。..%sに録画予約があります。..初期化中、録画予約は実行できません。..よろしいですか?"^"Starting initialization.\n%s has a recording reservation.\\nDuring initialization, recording reservations cannot be executed.\\nAre you sure?"^
s^"初期化中"^"Initializing"^
s^"初期設定"^"Initial setting"^
s^"删除失败。"^"Forfeited."^
s^"删除完毕。"^"Complete completion."^
s^"別名で保存する"^"Save as"^
s^"「別名で保存」を選択してください。　　"^"Select Save As."^
s^"利用したいキーワードをいくつか選択してください。"^"Please select some keywords you would like to use."^
s^"刪除中。"^"Cutting out."^
s^"削除"^"Delete"^
s^"削除しています。"^"Deleting."^
s^"削除しています。しばらくお待ちください。"^"Deleting. Please wait."^
s^"削除してください。"^"Please delete it."^
s^"削除しました。"^"It has been deleted."^
s^"削除します。"^"Delete."^
s^"削除します。よろしいですか？"^"Delete. Are you sure?"^
s^"削除すると、アルバムにあるすべてのフォトと"^"If you delete all the photos in the album"^
s^"削除できませんでした。"^"Unable to delete."^
s^"削除に失敗しました。"^"Delete failed."^
s^"削除を中止しました。"^"Deletion aborted."^
s^"削除を中止します。"^"Delete will be cancelled."^
s^"削除失敗しました。"^"Delete failed."^
s^"前"^"Before"^
s^"前のトラックはありません。"^"No truck in front."^
s^"前のフォトはありません。"^"There are no before photos."^
s^"前のプログラムが一致しません。"^"Previous program does not match."^
s^"前のプログラムが無効です。"^"Previous program is invalid."^
s^"前の写真"^"Previous photo"^
s^"前プログラム検索"^"Previous program search"^
s^"前へ"^"Forward"^
s^"前面"^"Front"^
s^"前面 + 天面"^"Front + Top"^
s^"副音声"^"Secondary audio"^
s^"副音声"^"Side Audio"^
s^"効果1"^"Effect 1"^
s^"効果2"^"Effect 2"^
s^"北京"^"Beijing"^
s^"十分な空き容量がありません。"^"There is not enough free space."^
s^"及びRSA(R) BSAFE TM SSL-C を搭載しております。"^"And is equipped with RSA(R) BSAFE TM SSL-C."^
s^"取り込みが終了しました。"^"Import finished."^
s^"取り込みできるすべてのフォトを取り込みます。"^"Import all photos that can be imported."^
s^"取り込みできるすべてのミュージックを取り込みます。"^"I'll import all the music I can."^
s^"取り込みの終了後、DJ登録します。"^"After the import is complete, you will be registered as a DJ."^
s^"取り込みました。"^"I've imported it."^
s^"取り込みを中止します。"^"Importing will be cancelled."^
s^"取り込みを中止します。..よろしいですか？"^"Cancel importing.\\nAre you sure?"^
s^"取り込みを中止します。..よろしいですか？"^"The import will be cancelled. \\nAre you sure?"^
s^"取り込みを続けますか？"^"Do you want to continue importing?"^
s^"取り込み中です。本体の電源を切らないでください。"^"Data is being downloaded. Please do not turn off the device."^
s^"取り込み先のアルバムを確認してください。"^"Please check the album you are importing to."^
s^"取り込み先のアルバムを選択してください。"^"Please select the album you want to import."^
s^"取り込むトラックに以下の情報を設定します。"^"Set the following information for the track to be imported."^
s^"取り込めなかったトラックがあります。"^"There are some tracks that could not be imported."^
s^"取り込めませんでした。"^"Unable to import."^
s^"取り込んでいます。"^"I'm taking it in."^
s^"取得不可"^"Unavailable"^
s^"取消し"^"Cancel"^
s^"取込み"^"Import"^
s^"取込みに失敗しました。"^"Failed to import."^
s^"取込みました。"^"Imported."^
s^"取込みました。アルバムにあるトラック数が999を超えたのでいくつかのアイテムは取込めませんでした。"^"I imported it. Some items could not be imported because the number of tracks in the album exceeded 999."^
s^"取込みを中止しました。"^"Import has been cancelled."^
s^"取込み先のアルバムを選択してください。"^"Please select the album to import to."^
s^"取込み可能なアイテムはありません。"^"There are no items available for import."^
s^"取込み後のサイズ :"^"Size after import:"^
s^"取込み日時"^"Import Date"^
s^"取込み日時 :"^"Import date and time:"^
s^"取込む容量がありません(LPCM)。"^"There is no capacity to import (LPCM)."^
s^"取込んでいます。"^"I'm taking it in."^
s^"受信"^"Receive"^
s^"受光部の設定"^"Enabled Receivers"^
s^"受损资料"^"Entitlement fee"^
s^"可"^"OK"^
s^"可用容量不足。"^"Insufficient available capacity."^
s^"可 (移動) "^"Possible (Move)"^
s^"台北"^"Taipei"^
s^"右ボタンを押して始めてください。"^"Press the right button to begin."^
s^"同じ語句が存在します。"^"Same words exist."^
s^"同意しない"^"Disagree"^
s^"同意します"^"Agree"^
s^"同意しません"^"i disagree"^
s^"同意する"^"agree"^
s^"名前 :"^"name :"^
s^"名前の変更を中止しました。"^"Name change aborted."^
s^"名前を変更しています。しばらくお待ちください。"^"Changing name. Please wait."^
s^"名前を変更してください。"^"Please change the name."^
s^"名前を変更してください。　　　　　　　　　　"^"Please change the name."^
s^"名前を変更しました。"^"Name changed."^
s^"名前を変更しますか？"^"Do you want to change the name?"^
s^"名前変更"^"Rename"^
s^"名前（昇順）"^"Name (ascending)"^
s^"名前（降順）"^"Name (descending)"^
s^"名称変更"^"Change of Name"^
s^"否"^"no"^
s^"品番 :"^"ID:"^
s^"回転(右)"^"Rotate (right)"^
s^"回転（右 )"^"Rotate (right)"^
s^"回転（左 )"^"Rotate (left)"^
s^"回転(左)"^"Rotation (left)"^
s^"(土)"^"(Sat)"^
s^"土"^"Sat"^
s^"地域番号 :"^"Area code:"^
s^"地域番号設定"^"Area code setting"^
s^"型名 :"^"Model name:"^
s^"基本操作"^"basic operation"^
s^"場所 :"^"Location:"^
s^"場所"^"place"^
s^"変換中"^"Converting"^
s^"変更しないときは"^"When not changing"^
s^"変更または削除されているため、再生できません。　"^"Cannot be played because it has been modified or deleted."^
s^"変更後、"^"After the change,"^
s^"変更後、DVD-RW（VRモード）以外へのダビングは..DVD二カ国語記録音声の音声設定で行われます。..よろしいですか？"^"After the change, dubbing to anything other than DVD-RW (VR mode) will be done using the DVD bilingual recording audio settings.\\nAre you sure?"^
s^"変更後は"^"After the change"^
s^"复制失败。"^"The system has failed."^
s^"复制完毕"^"Complete completion of the system"^
s^"复制完毕。"^"Complete completion of the system."^
s^"复制目标位置的可用空间不足。"^"Insufficient space available for target location."^
s^"复制目标位置的可用空间不足。..无复制目标媒体。"^"Insufficient space available for unrestricted target location.\\nUnrestricted target medium."^
s^"外部入力1 ..ガイドチャンネル"^"Video Input 1\\nGuide channel"^
s^"外部入力2 ..ガイドチャンネル"^"Video Input 2\\nGuide channel"^
s^"外部入力音声設定"^"Video Input\\nAudio Input Mode"^
s^"外部映像入力1"^"Video Input 1\\nType"^
s^"外部映像入力2"^"Video Input 2\\nType"^
s^"多め ( 12時間 )"^"Large (12 hours)"^
s^"夜 : 5:00PM～0:00AM"^"Night: 5:00PM - 0:00AM"^
s^"夜のニュース"^"Nightly News"^
s^"夜の連続ドラマ"^"Night drama series"^
s^"大小"^"Big and small"^
s^"大西洋標準時 (カナダ)"^"Atlantic Standard Time (Canada)"^
s^"天面"^"Top"^
s^"太平洋標準時 (カナダ)"^"Pacific Standard Time (Canada)"^
s^"太平洋標準時 (米国)"^"Pacific Standard Time (United States)"^
s^"失敗"^"Failure"^
s^"始めから再生"^"Play from Start"^
s^"始点"^"starting point"^
s^"始点終点の設定を取消します。"^"Cancels the start and end point settings."^
s^"字幕"^"subtitles"^
s^"字幕切り換え"^"Subtitle switching"^
s^"字幕言語"^"Subtitle language"^
s^"実 行"^"Select"^
s^"実行"^"Select"^
s^"実行する"^"Execute"^
s^"実行すると、ハードディスクのすべての内容を消去します。"^"Running will erase all contents of your hard disk."^
s^"実行中は電源を切らないでください。"^"Do not turn off the power while it is running."^
s^"容量"^"capacity"^
s^"容量オーバー"^"Over capacity"^
s^"少なめ ( 3時間 )"^"Less (3 hours)"^
s^"尚未格式化。"^"Shangmi formalization."^
s^"山地標準時 (カナダ)"^"Mountain Standard Time (Canada)"^
s^"山地標準時 (米国)"^"Mountain Time (United States)"^
s^"已保存的资料(PlayStation®2)"^"Saved Fees (PlayStation®2)"^
s^"已保存的资料(PlayStation®)"^"Saved Fees (PlayStation®)"^
s^"巻き戻し中です。..しばらくお待ちください。"^"Rewinding.\\nPlease wait for a moment."^
s^"年"^"Year"^
s^"年/月/日"^"Date"^
s^"延長"^"Ext."^
s^"延長時間を正しく設定してください。"^"Please set the extension time correctly."^
s^"引渡し、展示し、輸出し、輸入し、または送信することは、法律により禁止されています。"^"Delivery, display, export, import, or transmission is prohibited by law."^
s^"復旧処理を行います。"^"Perform recovery processing."^
s^"德文"^"Dokubun"^
s^"您要在格式化後拷貝嗎？"^"Would you like to be tortured after the formalization?"^
s^"您要格式化嗎？"^"Would you like to change the format?"^
s^"您要覆寫以取代現有的資料嗎？"^"Would you like to cover the current material?"^
s^"情報"^"Information"^
s^"情報の保存に失敗しました。"^"Failed to save information."^
s^"情報を読込んでいます。"^"Loading information."^
s^"情報更新"^"Restore Info"^
s^"情報編集"^"Edit information"^
s^"意大利文"^"Idaili sentence"^
s^"成功"^"success"^
s^"戻る"^"Back"^
s^"戻る"^"Cancel"^
s^"手動チャンネル設定"^"Manual channel setting"^
s^"手動予約"^"Manual reservation"^
s^"手動設定"^"manual setting"^
s^"手動録画"^"Manual recording"^
s^"拒否"^"rejection"^
s^"拡大表示"^"View larger image"^
s^"拷貝中。"^"Torukaichu."^
s^"拷貝中。請勿拔取記憶卡。"^"During the torture. Asking for a memory card."^
s^"拷貝失敗。"^"Torukai failed."^
s^"拷貝完畢"^"Complete Torture Shell"^
s^"拷貝完畢。"^"The torture shell is complete."^
s^"指定されたタイトルは無効です。"^"The specified title is invalid."^
s^"指定されたチャプターは無効です。"^"The specified chapter is invalid."^
s^"指定された値は無効です。"^"The specified value is invalid."^
s^"指定された時間は無効です。"^"The time specified is invalid."^
s^"指定なし"^"unspecified"^
s^"接続テストを行っています。"^"Connection test in progress."^
s^"接続後、"^"After connecting,"^
s^"接続機器："^"Connected device:"^
s^"接続状態を認識できませんでした。"^"Connection status could not be recognized."^
s^"損壞資料"^"Loss and loss materials"^
s^"撮影"^"photograph"^
s^"撮影機器名 :"^"Photography equipment name:"^
s^"操作できるトラックはありません。"^"There are no available trucks."^
s^"操作できるフォトはありません。"^"There are no photos that can be manipulated."^
s^"操作に失敗しました。..メモリーカードが正しく差し込まれているか..確認してください。"^"The operation failed.\\nPlease check if the memory card is inserted correctly."^
s^"操作パネル"^"Control Panel"^
s^"操作パネルから行います。"^"Do it from the control panel."^
s^"操作制限 :"^"Limits:"^
s^"放送局"^"Broadcaster"^
s^"放送局 :"^"Source:"^
s^"文件保护"^"Text protection"^
s^"文字を入力していないか、"^"Are you not typing any characters?"^
s^"文字を入力していないか"^"Are you typing any characters?"^
s^"文字を入力してください。"^"Please enter characters."^
s^"文字入力の設定"^"Character input settings"^
s^"文字入力の設定 / 登録語句の編集"^"Character input settings/editing registered words"^
s^"新しいPict Storyとして保存する場合には"^"When saving as a new Pict Story"^
s^"新しいフォルダ"^"New folder"^
s^"新しく設定すると、現在の設定を消去して、"^"Setting a new one will erase the current settings and"^
s^\^新しく設定すると、現在の設定を消去して、^Setting a new one will erase the current settings and^
s^"新橋23時"^"Shinbashi 23:00"^
s^"新規"^"New"^
s^"新規作成"^"Create New"^
s^"无"^"No"^
s^"无复制目标媒体。"^"Unrestricted target medium."^
s^"无复制目标媒体。..此资料无法删除。..要在复制前格式化吗？"^"Unrestricted target medium. \\nThis fee is free of charge.\\nIs it necessary to make it pre-qualified?"^
s^"无复制目标媒体..请指定复制目标媒体。"^"Unlimited target medium\\nSpecified target medium."^
s^"无法复制到所指定的媒体。"^"No legal restrictions on designated media."^
s^"(日)"^"(Sun)"^
s^"日"^"Sun"^
s^"日付："^"date:"^
s^"日付"^"Date"^
s^"日付と時刻の表示形式を選択し、→ボタンで次の画面に進んでください。"^"Please select the date and time display format and proceed to the next screen using the → button."^
s^"日付と時刻の設定"^"Date and Time Settings"^
s^"日付と時刻を設定し、→ボタンで次の画面に進んでください。"^"Set the date and time, then press the → button to proceed to the next screen."^
s^"日付を変更するときは"^"When changing the date"^
s^"日付を正しく設定してください。"^"Please set the date correctly."^
s^"日付（古い順）"^"Date (oldest first)"^
s^"日付（新しい順）"^"Date (newest first)"^
s^"日付順"^"By date"^
s^"日文"^"Japanese"^
s^"日時"^"Date"^
s^"日/月/年"^"Day/Month/Year"^
s^"日本語"^"Japanese"^
s^"日本語キーボード"^"Japanese keyboard"^
s^"日本語入力"^"Japanese input"^
s^"早戻し"^"Fast rewind"^
s^"早見再生"^"Quick playback"^
s^"早送り"^"Fast forward"^
s^"映像"^"Composite"^
s^"映画"^"movie"^
s^"是"^"Yes"^
s^"是否格式化？"^"Formalization?"^
s^"昼 : 11:00AM～6:00PM"^"Lunch: 11:00AM-6:00PM"^
s^"時"^"Time"^
s^"時刻 :"^"time :"^
s^"時刻："^"time:"^
s^"時刻の設定"^"Setting the time"^
s^"時刻設定"^"Date and Time"^
s^"時間"^"time"^
s^"時間を選択してください。"^"Please select a time."^
s^"時間帯"^"Time period"^
s^"時間指定ジャンプ"^"Timed jump"^
s^"時間指定予約"^"Timed reservation"^
s^"時間表示は禁止されています。"^"Time display is prohibited."^
s^"普通 ( 6時間 )"^"Normal (6 hours)"^
s^"暗証番号が違います。"^"The PIN number is incorrect."^
s^"曲名 :"^"Song title:"^
s^"曲数 :"^"Song count:"^
s^"更新日時 :"^"Updated:"^
s^"更新日期和时间"^"Updated date and time"^
s^"書き出し"^"Export"^
s^"書出し"^"Export"^
s^"書込み中…"^"Writing..."^
s^"最後更新"^"Last updated"^
s^"最新ソフトウェアを確認しています。"^"Checking for latest software."^
s^"最新のソフトウェアがあります。"^"We have the latest software."^
s^"最適化"^"Optimize"^
s^"最適化が終了しました。"^"Optimization finished."^
s^"最適化に失敗しました。..ダビングを中止します。"^"Optimization failed.\\nDubbing will be cancelled."^
s^"最適化に失敗しました。..ダビングを終了します。"^"Optimization failed.\\nDubbing will end."^
s^"最適化の終了後、Pict Storyから"^"From Pict Story after optimization"^
s^"最適化の開始に失敗しました。"^"Failed to start optimization."^
s^"最適化を中止します。..よろしいですか？"^"Cancel optimization.\\nAre you sure?"^
s^"最適化を実行します。..終了まで"^"Run optimization.\\nUntil finished"^
s^"最適化を開始します。..最適化中、録画予約は実行できません。"^"Optimization will begin.\\nDuring optimization, recording reservations cannot be performed."^
s^"最適化中    %d％"^"Optimizing %d%"^
s^"最適化中です。..ダビングできません。"^"Optimization in progress. \\nDubbing is not possible."^
s^"最適化中です。..ダビングできません。"^"Optimizing.\\nUnable to dub."^
s^"最適化中です。..初期化できません。"^"Optimization in progress. \\nUnable to initialize."^
s^"最適化中です。..初期化できません。"^"Optimizing.\\nUnable to initialize."^
s^"最適化中のタイトルです。..その操作は実行できません。"^"The title is being optimized.\\nThe operation cannot be performed."^
s^"最適化中のタイトルです。..チャプター表示できません。"^"The title is being optimized.\\nChapter cannot be displayed."^
s^"最適化中のタイトルです。..プロテクトできません。"^"The title is being optimized.\\nCannot be protected."^
s^"最適化中のタイトルです。..プロテクト解除できません。"^"The title is being optimized.\\nUnprotected."^
s^"最適化中のタイトルです。..削除できません。"^"The title is being optimized.\\nIt cannot be deleted."^
s^"最適化中のタイトルです。..名前変更できません。"^"Title is being optimized.\\nCannot be renamed."^
s^"最適化中のタイトルです。..編集できません。"^"Title is being optimized.\\nCannot be edited."^
s^"最適化中のタイトルです。..編集取消しできません。"^"The title is being optimized.\\nEditing cannot be undone."^
s^"最適化中は取り込めません。"^"Cannot be imported during optimization."^
s^"最適化中は変更できません。"^"Cannot be changed during optimization."^
s^"最適化中止"^"Stop optimization"^
s^"最適化前と同じ設定です。"^"Same settings as before optimization."^
s^"最適化前のタイトル"^"Title before optimization"^
s^"最適化前のタイトルを残すか残さないかを設定します。"^"Set whether to keep the pre-optimized title or not."^
s^"最適化後タイトル"^"Title after optimization"^
s^"最適化後のタイトルから編集された部分を残すか残さないかを設定します。..「保存」を選んだ場合でも、最適化後のタイトルに、編集やチャプターの設定は..残りません。"^"Set whether to keep the edited parts of the title after optimization.\\nEven if you select Save, the edits and chapter settings will not remain in the title after optimization.\n yeah. "^
s^"最適化後の録画モードを設定します。..元の録画モードよりも高画質にすることはできません。"^"Sets the recording mode after optimization.\\nThe image quality cannot be higher than the original recording mode."^
s^"最適化後、容量を確保する場合は、「編集範囲」の削除..「最適化前のタイトル」の削除を選んでください。..容量を確保する必要がない場合は設定を変更しないでください。"^"If you want to secure space after optimization, please delete the 'edit range'\nPlease select to delete the 'title before optimization'. \\nPlease do not change the settings unless you need to secure the capacity. "^
s^"最適化開始"^"Start optimization"^
s^"(月)"^"(Mon)"^
s^"月"^"Mon"^
s^"月～土"^"Mon-Sat"^
s^"月/日/年"^"Month/Day/Year"^
s^"月～金"^"Mon-Fri"^
s^"朝 : 5:00AM～0:00PM"^"Morning: 5:00AM-0:00PM"^
s^"木"^"Thu"^
s^"(木)"^"(Tue)"^
s^"未フォーマット"^"unformatted"^
s^"未フォーマット"^"Unformatted"^
s^"未分類"^"Uncategorized"^
s^"未初期化"^"Uninitialized"^
s^"未初期化状態"^"Uninitialized state"^
s^"未取得"^"Unacquired"^
s^"未格式化。"^"Unformalized."^
s^"未視聴"^"Unwatched"^
s^"未読メッセージ : %i "^"Unread message: %i"^
s^"本体の設定"^"System settings"^
s^"本体の電源を切ったり、CDを取り出さないでください。"^"Do not turn off the unit or remove the CD."^
s^"本体の電源を切ったり、メディアを抜かないでください。"^"Please do not turn off the power or remove the media."^
s^"本体の電源を切ったり、メモリースティックを抜かないでください。"^"Do not turn off the power or remove the memory stick."^
s^"本体の電源を切らないでください。"^"Please do not turn off the device."^
s^"本体情報"^"System Information"^
s^"・本機とハブ/ルータ/モデム/ONU間を"^"・Between this unit and hub/router/modem/ONU"^
s^"・本機とルータ/ハブ/モデム/ONU間を"^"・Between this unit and router/hub/modem/ONU"^
s^"本機に搭載している「x-DJ」機能は、"^"The 「x-DJ」 function installed in this unit is "^
s^"本機の修復が完了しました。"^"Repair of this machine has been completed."^
s^"本機の終了後、電源を入れ直してください。"^"After shutting down the machine, please turn it back on."^
s^"本機の電源やネットワークの接続を"^"Connect the machine's power and network"^
s^"本機の電源やネットワークの接続を切らずに"^"Without turning off the machine's power or network connection"^
s^"本機の電源を入れなおした後、"^"After turning the machine off and on again,"^
s^"本機の電源を入れ直した後"^"After turning the machine off and on again"^
s^"本機の電源を入れ直した後、しばらくたってから"^"After a while after turning the machine off and on again"^
s^"本機の電源を切らないでください。"^"Please do not turn off the power of this unit."^
s^"・本機またはルータのDNS設定は正しい値を"^"・Ensure that the DNS settings of this unit or router are correct."^
s^"本機を使うための基本的な設定を行います。"^"Perform basic settings for using this device."^
s^"本機を再起動します。"^"Restart the device."^
s^"本機を終了します。"^"Terminate the device."^
s^"本製品の通信機能にはNetBSD Foundation, Inc.およびその協力者によって"^"The communication functionality of this product was provided by the NetBSD Foundation, Inc. and its contributors."^
s^"本製品は&quot;DNAS&quot; (Dynamic Network Authentication System)という著作権および"^"This product is copyrighted by DNAS (Dynamic Network Authentication System)."^
s^"本製品は、RSA Security Inc.のRSA(R) BSAFE TM Crypt-C"^"This product is RSA Security Inc.'s RSA(R) BSAFE TM Crypt-C"^
s^"本製品は、ドルビーラボラトリーズからの実施権に基づき製造されています。"^"This product is manufactured under license from Dolby Laboratories."^
s^"本製品は、著作権保護技術を採用しており、マクロビジョン社およびその他の著作権利者が保有する"^"This product employs copyright protection technology and is owned by Macrovision and other copyright holders."^
s^"東京"^"Tokyo"^
s^"東部標準時 (カナダ)"^"Eastern Standard Time (Canada)"^
s^"東部標準時 (米国)"^"Eastern Standard Time (United States)"^
s^"枚数 :"^"Quantity:"^
s^"株式会社ソニー・コンピュータエンタテインメントの商標です。"^"It is a trademark of Sony Computer Entertainment Inc."^
s^"株式会社ソニー・コンピュータエンタテインメントの登録商標です。"^"It is a registered trademark of Sony Computer Entertainment Inc."^
s^"格式化失敗。"^"Formalization failed."^
s^"格式化完毕。"^"Formalization completed."^
s^"格式化完畢。"^"Formalization complete."^
s^"格式化進行中。"^"Formalization in progress."^
s^"格式化進行中。請勿拔取記憶卡。"^"Formalization in progress. Request a memory card."^
s^"標準"^"Standard"^
s^"機器 :"^"device :"^
s^"機器情報エラーです。"^"Device information error."^
s^"檔案保護"^"Design protection"^
s^"檔案大小"^"Size of plan"^
s^"檔案種類"^"Type of plan"^
s^"次"^"Next"^
s^"次に再生するときは今のつづきから再生します。"^"The next time you play, it will play from where it left off."^
s^"次のトラックはありません。"^"There is no next track."^
s^"次のフォトはありません。"^"There is no next photo."^
s^"次のプログラムが一致しません。"^"The following programs do not match."^
s^"次のプログラムが無効です。"^"The following program is invalid."^
s^"次の写真"^"Next photo"^
s^"次プログラム検索"^"Next program search"^
s^"次へ"^"to the next"^
s^"歌謡曲"^"Popular songs"^
s^"・正しい種類のケーブルを接続している"^"・The correct type of cable is connected."^
s^"・正しい種類のケーブルを接続している。"^"・The correct type of cable is connected."^
s^"正しく接続されていることを確認してください。"^"Please make sure you are connected correctly."^
s^"正しく接続している"^"Connected correctly"^
s^"正しく接続している。"^"Connected correctly."^
s^"正在删除。"^"Correctly present."^
s^"正在复制。请勿取出记忆卡装置。"^"Correct existence record system. Reading and recording memory device."^
s^"正在格式化。"^"Present formalization."^
s^"正在格式化。请勿取出记忆卡装置。..格式化失败。"^"Formalization of current status. Recording device for recording.\\nFailure of formalization."^
s^"正在格式化。请勿取出记忆卡装置。..要覆盖现存的资料吗？"^"Formalization of current status. It is not necessary to take out the memory card device.\\nIs it necessary to cover the existing information?"^
s^"此资料无法删除。"^"This fee is free of charge."^
s^"此资料无法删除。..正在复制。请勿取出记忆卡装置。"^"This fee is free of charge. \\nIt is correct to use it. It is not necessary to take out the memory card device."^
s^"此资料无法复制。"^"This fee is free of charge."^
s^"毎日"^"every day"^
s^"毎週"^"Every week"^
s^"毎週"^"weekly"^
s^"毎週(土)"^"Every week (Sat)"^
s^"毎週(日)"^"Every week (Sunday)"^
s^"毎週(月)"^"Every week (Monday)"^
s^"毎週(木)"^"Every week (Thursday)"^
s^"毎週(水)"^"Every week (Wednesday)"^
s^"毎週(火)"^"Every week (Tuesday)"^
s^"毎週(金)"^"Every week (Friday)"^
s^"(水)"^"(Wed)"^
s^"水"^"Wed"^
s^"決定"^"Enter"^
s^"法文"^"Law text"^
s^"消去されます。"^"Erase."^
s^"深夜 : 11:00PM～5:00AM"^"Late night: 11:00PM - 5:00AM"^
s^"済"^"done"^
s^"演歌"^"Enka"^
s^"(火)"^"(Thu)"^
s^"火"^"Tue"^
s^"無"^"Nothing"^
s^"無法拷貝到您所選擇之媒體上。"^"The lawless torture vessel is the medium of selection."^
s^"玩游戏"^"Toy game"^
s^"現在&#34;DNAS&#34;サーバーが処理を受け付けることが"^"Currently, the DNAS server is unable to accept processing."^
s^"現在のゲーム領域："^"Current game area:"^
s^"現在のソフトウェアは最新バージョンです。"^"Your current software is the latest version."^
s^"現在のソフトウェアをバックアップしています。"^"Backing up current software."^
s^"現在の操作を中止します。"^"Aborts current operation."^
s^"現在の設定をクリアして、出荷時の状態に戻します。..よろしいですか？....実行すると、番組表のデータも削除されますが、..ハードディスクに保存しているビデオやミュージック、..フォトは削除されません。"^"This will clear the current settings and return them to the factory settings.\\nAre you sure?\n\\nWhen you execute this, the data in the program guide will also be deleted, but\\nthe videos saved on the hard disk and Music and\\nphotos will not be deleted."^
s^"現在の設定をクリアしました。"^"Current settings have been cleared."^
s^"現在の録画モード"^"Current recording mode"^
s^"現在、他のタイトルを最適化中です。"^"We are currently optimizing other titles."^
s^"現在時刻を確認してください。"^"Please check the current time."^
s^"環境で正しく働くように用意します。"^"Prepare it to work correctly in your environment."^
s^"画像名 :"^"Image name:"^
s^"      画質1 ( 高画質 )"^"      Q 1 (Clearest)"^
s^"      画質2"^"      Q 2"^
s^"      画質3"^"      Q 3"^
s^"      画質4"^"      Q 4"^
s^"      画質5"^"      Q 5"^
s^"      画質6 ( 長時間 )"^"      Q 6 (Longest)"^
s^"画面サイズを正しく設定してください。"^"Please set the screen size correctly."^
s^"画面表示"^"Screen display"^
s^"番組の終了まで録画します。"^"The program will be recorded until the end of the program."^
s^"番組情報取得中は変更できません。"^"Cannot be changed while program information is being acquired."^
s^"番組表"^"TV Schedule"^
s^"番組表に合わせる"^"Adjust to program schedule"^
s^"・番組表のデータ"^"・Program schedule data"^
s^\^・番組表のデータ^・Program Schedule Data^
s^"番組表の情報は取得できていません。"^"Program schedule information could not be obtained."^
s^"番組表は利用できません。"^"Program listing not available."^
s^"番組表予約"^"Program schedule reservation"^
s^"番組表取得チャンネル"^"Program Listing\\nAcquisition Channel"^
s^"番組表取得時刻"^"Program Listing\\nAcquisition Time"^
s^"番組追跡 :"^"Program tracking:"^
s^"番組追跡"^"Program tracking"^
s^"異常が発生したので再生を停止しました。"^"Playback has been stopped because an error occurred."^
s^"発売元 :"^"Publisher:"^
s^"発売年 :"^"Release year:"^
s^"発売年"^"Release year"^
s^"登録キーワード"^"Registered keyword"^
s^"登録されている語句はありません。"^"There are no registered words."^
s^"登録します。"^"I will register."^
s^"登録する場所を選択してください。"^"Please select a location to register."^
s^"登録する語句を編集してください。"^"Please edit the word you want to register."^
s^"登録中"^"Registering"^
s^"登録済みのサムネイルに上書きします。"^"Overwrites the registered thumbnail."^
s^"登録語句の編集"^"Edit saved words"^
s^"短い"^"short"^
s^"破損データ"^"Corrupted Data"^
s^"確定"^"Enter"^
s^"確認"^"confirmation"^
s^"確認再生"^"Confirm play"^
s^"確認再生の終了は、終了ボタンか、"^"To end the confirmation playback, press the exit button."^
s^"確認再生を終了します。"^"End confirmation playback."^
s^"確認再生を開始します。よろしいですか?"^"Start confirmation playback. Are you sure?"^
s^"確認、変更後は"^"Confirm, after changes"^
s^"禁止删除"^"Forbidden exclusion"^
s^"禁止刪除"^"Forbidden exclusion"^
s^"禁止复制"^"Prohibition system"^
s^"禁止拷貝"^"Forbidden torture shell"^
s^"秒"^"second"^
s^"移ってください。"^"Please move."^
s^"移動"^"Move"^
s^"移動しました。"^"moved."^
s^"移動に失敗しました。"^"Move failed."^
s^"移動に失敗しました。メモリーカードがありません。"^"Move failed. No memory card."^
s^"移動を中止しました。"^"Movement aborted."^
s^"移動先がありません。"^"There is nowhere to go."^
s^"移動先のデータを上書きしますか？"^"Do you want to overwrite the data at the destination?"^
s^"移動先の空き容量が不足しています。"^"There is not enough free space at the destination."^
s^"移動先を選択してください。"^"Please select a destination."^
s^"種類"^"Type:"^
s^"種類 :"^"Type:"^
s^"空き容量"^"Free space"^
s^"簡体字中国語"^"Simplified Chinese"^
s^"簡単設定を中止しますか？"^"Do you want to cancel easy setup?"^
s^"簡易設定"^"Simple settings"^
s^"簡略设定"^"Simple configuration"^
s^"米国特許およびその他の知的財産権によって保護されています。この著作権保護技術の使用は、"^"Protected by U.S. patents and other intellectual property rights. Use of this copyright protection technology is protected by U.S. patents and other intellectual property rights."^
s^"类型"^"type"^
s^"系統驅動程式(PlayStation®2)"^"Systematic Equation (PlayStation®2)"^
s^"系统驱动程序(PlayStation®2)"^"Sequence of Progress (PlayStation®2)"^
s^"約17時間(SP録画時)短くなります。"^"About 17 hours (when recording with SP) will be shorter."^
s^"約17時間(SP録画時)短くなります。"^"It will be about 17 hours shorter (when recording with SP)."^
s^"終了"^"end"^
s^"終了すると、次回から選択した部分は再生しません。"^"If you exit, the selected section will not be played next time."^
s^"終了までおよそ"^"Approximately until the end"^
s^"終了処理を行っています。"^"Finding process in progress."^
s^"終了時刻"^"Ends"^
s^"終了時刻を正しく設定してください。"^"Please set the end time correctly."^
s^"終点"^"the last stop"^
s^"続き"^"continuation"^
s^"続けてPict Storyのビデオ作成をします。　　"^"Next, let's create a Pict Story video."^
s^"続けるには何かキーを押してください。"^"Press any key to continue."^
s^"総再生時間 :"^"Runtime:"^
s^"総再生時間"^"Total playback time"^
s^"総再生時間"^"Total play time"^
s^"総容量"^"Total capacity"^
s^"編集 :"^"Edit:"^
s^"編集"^"Edit"^
s^"編集した映像を取消して終了します。"^"Cancels the edited video and exits."^
s^"編集した映像を確定して終了します。"^"Confirm the edited video and exit."^
s^"編集した映像を確認します。"^"Check the edited footage."^
s^"編集を取消しました。"^"Edit cancelled."^
s^"編集を取消します。..よろしいですか？"^"Cancel the edit.\\nAre you sure?"^
s^"編集を終了します。"^"Finish editing."^
s^"編集を続ける場合は、編集範囲やチャプターマークを"^"If you want to continue editing, please mark the editing range and chapter mark."^
s^"編集内容を保存しないで終了します。"^"Exit without saving your edits."^
s^"編集内容を保存できませんでした。"^"Your edits could not be saved."^
s^"編集取消し"^"Undo Edit"^
s^"( 編集後 :                       )"^"(Edited: )"^
s^"編集範囲"^"Edit range"^
s^"編集範囲の取消し"^"Cancel edit range"^
s^"繁体字中国語"^"Traditional Chinese"^
s^"置き換えますか?"^"Replace?"^
s^"義大利文"^"Gidai Toshifumi"^
s^"自動"^"Auto"^
s^"自動ステレオ受信"^"Automatic Stereo\\nReception"^
s^"自動チャンネル設定"^"Automatic channel setting"^
s^"自動チャンネル設定を実行します。..よろしいですか？"^"Perform automatic channel configuration.\\nAre you sure?"^
s^"自動チャンネル設定を行いますか？"^"Do you want to configure automatic channel settings?"^
s^"自動チャンネル設定中です。"^"Automatic channel setting in progress."^
s^"自動モード"^"auto mode"^
s^"自動時刻設定を実行しますか？"^"Do you want to perform automatic time setting?"^
s^"自動時刻設定を実行します。..よろしいですか？"^"Automatic time setting will be performed.\\nAre you sure?"^
s^"自動時刻設定中です。"^"Auto time setting in progress."^
s^"自動消去 :"^"Auto-delete:"^
s^"自動消去対象タイトル"^"Titles Subject to\\nAutomatic Deletion"^
s^"自動設定"^"Automatic configuration"^
s^"自動設定しない"^"Do not set automatically"^
s^"自動設定する"^"Automatically set"^
s^"英文"^"English"^
s^"英語"^"English"^
s^"英語キーボード"^"English keyboard"^
s^"荷兰文"^"Wen Wen"^
s^"荷蘭文"^"Lanwen"^
s^"葡萄牙文"^"Grape fang pattern"^
s^"表示チャンネル"^"Display channel"^
s^"表示言語"^"Display language"^
s^"表示間隔：0.5秒"^"Display interval: 0.5 seconds"^
s^"表示間隔：15秒"^"Display interval: 15 seconds"^
s^"表示間隔：6分"^"Display interval: 6 minutes"^
s^"表示間隔変更"^"Change display interval"^
s^"補間処理"^"Smooth"^
s^"視聴年齢制限レベル"^"Viewing age restriction level"^
s^"視聴年齢制限を 一時レベル"^"Temporary level of viewing age restriction"^
s^"視聴年齢制限使用地域"^"Viewing age restriction usage area"^
s^"視聴年齢制限暗証番号"^"Viewing Age Restriction PIN"^
s^"解像度 :"^"Resolution:"^
s^"言語："^"language:"^
s^"言語を選択し、→ボタンで次の画面に進んでください。"^"Select your language and press the → button to proceed to the next screen."^
s^"言語一覧"^"Language list"^
s^"設定1"^"Setting 1"^
s^"設定2"^"Settings 2"^
s^"設定"^"Settings"^
s^"設定がいっぱいです。"^"There are too many settings."^
s^"設定した内容をハードディスクに保存し"^"Save the settings to your hard disk."^
s^"設定している"^"Setting"^
s^"設定してください。"^"Please set."^
s^"設定チャンネル"^"Automatic time source"^
s^"設定ファイル(PlayStation®2)"^"Settings file (PlayStation®2)"^
s^"設定を保存します。..よろしいですか？"^"Save your settings.\\nAre you sure?"^
s^"設定を保存できませんでした。"^"Failed to save settings."^
s^"設定を変更すると、番組表やおまかせ・まる録が..利用できなくなる場合があります。..よろしいですか？"^"If you change these settings, you may not be able to use program guides\\nor Omakase/Maruroku. Are you sure?"^
s^"設定を変更すると、番組表やおまかせ・まる録が..利用できなく場合があります。..よろしいですか？"^"If you change these settings, you may not be able to use program guides\\nor Omakase/Maruroku. Are you sure?"^
s^"設定を読み込めませんでした。"^"Failed to load configuration."^
s^"設定中にエラーが発生しました。"^"An error occurred during configuration."^
s^"設定中は本機の電源を切らないでください。"^"Do not turn off the power of this unit while setting is in progress."^
s^"設定中は電源を切らないでください。"^"Do not turn off the power while setting."^
s^"設定値一覧"^"Setting value list"^
s^"設定内容を確認し、よろしければ○ボタンを押して"^"Check the settings and press the ○ button if you agree."^
s^"設定内容一覧"^"List of settings"^
s^"設定後、"^"After setting,"^
s^"設定檔案(PlayStation®2)"^"Setting plan (PlayStation®2)"^
s^"設定確認"^"Check settings"^
s^"設定終了"^"Setup complete"^
s^"設定開始"^"Start setting"^
s^"設定開始"^"Start setup"^
s^"許可"^"permission"^
s^"該当するデータがありません。"^"No matching data found."^
s^"詳細１の表示領域です。"^"This is the display area for details 1."^
s^"詳細２の表示領域です。"^"This is the display area for details 2."^
s^"詳細については「PSXについて」の後の「BSDライセンスについて」をご覧ください。"^"For more information, please see ``About the BSD License`` after ``About PSX.`` "^
s^"詳細表示"^"Detail View"^
s^"認証システムを使用しています。このシステムの無効化装置若しくはプログラムを譲渡し、"^"We are using an authentication system. We will transfer the disabling device or program for this system."^
s^"認証中は処理を中止できません。"^"Processing cannot be cancelled during authentication."^
s^"認識できないディスクです。..ダビング可能なディスクを..入れてください。"^"Unrecognized disc.\\nPlease insert a disc that can be dubbed."^
s^"語句を削除しました。"^"Phrase removed."^
s^"語句を削除します。..よろしいですか？"^"Delete the phrase.\\nAre you sure?"^
s^"語句を登録しました。"^"Word has been registered."^
s^"請選擇要接受拷貝的媒體。"^"A request for selection is required."^
s^"請選擇要接受拷貝的媒體。..您要在格式化後拷貝嗎？"^"You need to choose a medium for receiving treatment.\\nIs it necessary to use torture after formalization?"^
s^"請選擇要接受拷貝的媒體...本資料無法刪除。..您要在格式化後拷貝嗎？"^"I need to select a medium for receiving treatment.\\nThis material is illegally removed.\\nIs it necessary to use torture after making it into a formalized form?"^
s^"請選擇要接受拷貝的媒體。..本資料無法拷貝。"^"A request for selection is required.\\nThis material is illegally tortured."^
s^"设定文件(PlayStation®2)"^"Settings (PlayStation®2)"^
s^"请勿取出记忆卡装置。"^"A memory card device that can be read and retrieved."^
s^"请指定复制目标媒体。"^"Specified target medium."^
s^"请指定复制目标媒体。..要在复制前格式化吗？"^"Specified additional system target medium.\\nIs it necessary to formalize the existing system?"^
s^"資料作成"^"Material creation"^
s^"資料刪除失敗。"^"Data removal failed."^
s^"資料刪除完畢。"^"Materials removed complete."^
s^"転送"^"Check In"^
s^"輪郭強調"^"Image Sharpening"^
s^"追加"^"addition"^
s^"追加しています。.*本体の電源を切ったり、メディアを取り出さないでください。"^"Adding.\\nPlease do not turn off the device or remove the media."^
s^"追加しています。..本体の電源を切ったり、メディアを取り出さないでください。"^"Adding.\\nPlease do not turn off the device or remove the media."^
s^"追加しました。"^"Added."^
s^"追加しました。お気に入りにあるアイテム数が999を超えたのでいくつかのアイテムは取込めませんでした。"^"Added. Some items could not be imported because the number of items in Favorites exceeded 999."^
s^"追加に失敗しました。いくつかのコンテンツは追加できませんでした。"^"Add failed. Some content could not be added."^
s^"追加を中止します。よろしいですか？"^"Cancel addition. Are you sure?"^
s^"追加先のお気に入りを選択してください。"^"Please select a favorite to add to."^
s^"通信エラーです。"^"Communication error."^
s^"通信がタイムアウトしました。"^"Communication timed out."^
s^"通常再生ではない(メッセージ無し)"^"Not normal playback (no message)"^
s^"速い"^"fast"^
s^"進行状況"^"Progress"^
s^"遅い"^"slow"^
s^"遊戲"^"Play"^
s^"選択"^"choice"^
s^"選択されたタイトルの全ての録画モードを..自動で調整します。よろしいですか？..※録画モードが保護されたタイトルは、..　録画モードの変更は行われません。"^"All recording modes of the selected title will be automatically adjusted.\\nAre you sure?\n*For titles whose recording modes are protected,\\nthe recording mode will not be changed."^
s^"選択されたタイトルの全ての録画モードを自動で調整します。..よろしいですか？..※録画モードが保護されたタイトルは、..録画モードの変更は行われません。"^"All recording modes of the selected title will be automatically adjusted.\\nAre you sure?\n*If the recording mode is protected,\\nthe recording mode will not be changed."^
s^"選択した範囲を取消します。"^"Cancel the selected range."^
s^"選択後、"^"After selection,"^
s^"部分を再生しません。"^"Does not play part."^
s^"重複件数："^"Number of duplicates:"^
s^"野球"^"baseball"^
s^"(金)"^"(Fri)"^
s^"金"^"Fri"^
s^"録画NR"^"Recording NR"^
s^"録画しない"^"Do not record"^
s^"録画の終了後、Pict Storyから　　　　"^"After recording ends, from Pict Story"^
s^"録画モード :"^"Quality:"^
s^"録画モード"^"Recording mode"^
s^"録画モード"^"Recording Mode"^
s^"録画モードをSLP(DVD+RW用)にします。..そのためダビングに時間がかかります。"^"Set the recording mode to SLP (for DVD+RW).\\nTherefore, dubbing will take time."^
s^"録画モードをSLP(+VR用)にします。..ダビングには、タイトルの再生時間と..同じくらいの時間がかかります。"^"Set the recording mode to SLP (+VR).\\nDubbing takes about the same time as the title playback time."^
s^"録画モードを固定します。..このタイトルは、録画モードの..自動調整が行われません。..よろしいですか？"^"The recording mode will be fixed.\\nThe recording mode will not be automatically adjusted for this title.\\nAre you sure?"^
s^"録画モードを固定します。よろしいですか？..録画モードが固定されたタイトルは、..録画モードの自動調整の対象になりません。"^"Fix the recording mode. Are you sure?\\nTitles with a fixed recording mode will not be subject to automatic recording mode adjustment."^
s^"録画モードを正しく設定してください。"^"Please set the recording mode correctly."^
s^"録画を中止できませんでした。"^"Unable to stop recording."^
s^"録画中、あるいは最適化中です。..ダビングできません。"^"Recording or optimizing.\\nUnable to dub."^
s^"録画中、および最適化中は..初期化できません。"^"Cannot be initialized while recording or optimizing."^
s^"録画中です。エンコードできません。"^"Recording. Unable to encode."^
s^"録画中です。..ダビングできません。"^"Recording in progress. \\nDubbing is not possible."^
s^"録画中です。..ダビングできません。"^"Recording in progress.\\nUnable to dub."^
s^"録画中です。..チャンネルを切換えることはできません。"^"Recording in progress.\\nYou cannot change channels."^
s^"録画中です。..初期化できません。"^"Recording in progress. \\nUnable to initialize."^
s^"録画中です。..初期化できません。"^"Recording in progress.\\nUnable to initialize."^
s^"録画中です。..最適化できません。"^"Recording in progress.\\nUnable to optimize."^
s^"録画中です。..録画モードを変更できません。"^"Recording in progress.\\nUnable to change recording mode."^
s^"録画中のタイトルです。..その操作は実行できません。"^"This is the title being recorded.\\nThe operation cannot be performed."^
s^"録画中のタイトルです。..チャプター表示できません。"^"Title being recorded.\\nChapter cannot be displayed."^
s^"録画中のタイトルです。..編集できません。"^"Title being recorded.\\nCannot be edited."^
s^"録画中のタイトルです。..編集取消しできません。"^"Title being recorded.\\nEditing cannot be undone."^
s^"録画中のため、ビデオ作成できません。"^"Unable to create video because recording is in progress."^
s^"録画予約がいっぱいです。"^"We are fully booked for recording."^
s^"録画予約が正しく実行できない場合があります。"^"Recording reservations may not be executed correctly."^
s^"録画予約の準備中です。..ダビングを実行するには、録画予約を削除してください。"^"Preparing for recording reservation.\\nTo perform dubbing, please delete the recording reservation."^
s^"録画予約の準備中です。初期化を実行するには、録画予約を削除してください。"^"Preparing for recording reservations. To initialize, please delete the recording reservations."^
s^"録画予約の開始まで10分を切りました。..現在の操作を中止します。"^"There are less than 10 minutes left until the recording reservation starts.\\nThe current operation will be cancelled."^
s^"録画予約の開始まで1時間を切りました。"^"There is less than an hour left until the recording reservation starts."^
s^"録画予約は実行中です。..予約の修正を中止します。"^"Recording reservation is in progress.\\nReservation modification will be cancelled."^
s^"録画予約は終了しています。..予約の修正を中止します。"^"Recording reservation has ended.\\nReservation modification will be cancelled."^
s^"録画予約を修正しました。"^"Recording reservation has been modified."^
s^"録画予約を削除した後、Pict Storyから　　"^"After deleting the recording reservation, from Pict Story"^
s^"録画予約を削除しました。"^"Recording reservation has been deleted."^
s^"録画予約を削除しました。"^"The recording reservation was deleted."^
s^"録画予約を削除します"^"Delete recording reservation"^
s^"録画予約を削除します。..よろしいですか？"^"Delete recording reservation. \\nAre you sure?"^
s^"録画予約を削除します。..よろしいですか？"^"Delete the recording reservation.\\nAre you sure?"^
s^"録画予約を削除できませんでした。"^"The recording reservation could not be deleted."^
s^"録画予約を実行中です。..削除すると、録画を停止します。..よろしいですか？"^"Recording reservation is in progress. \\nIf you delete it, recording will stop. \\nAre you sure?"^
s^"録画予約を実行中です。..削除すると、録画を停止します。..よろしいですか？"^"Recording reservation is in progress.\\nIf you delete it, recording will stop.\\nAre you sure?"^
s^"録画予約を確定しました。"^"The recording reservation has been confirmed."^
s^"録画予約を確定できませんでした。..録画予約がいっぱいです。"^"The recording reservation could not be confirmed.\\nThe recording reservation is full."^
s^"録画予約を確定できませんでした。..録画予約がいっぱいです。"^"The recording reservation could not be confirmed. \\nWe are full of recording reservations."^
s^"録画予約を設定しました。"^"Recording reservation has been set."^
s^"録画予約時刻開始まで10分を切りました。"^"Less than 10 minutes left until the scheduled recording time starts."^
s^"録画予約準備、もしくは録画予約が開始されたため、..最適化を開始することができません。"^"\\nOptimization cannot be started because recording reservation preparation or recording reservation has started."^
s^"録画予約確認"^"Confirm recording reservation"^
s^"録画予約開始時刻になるとキャンセルして終了します。"^"When the scheduled recording start time arrives, the recording will be cancelled and ended."^
s^"録画予約開始時刻まで10分を切りました。"^"There are less than 10 minutes until the recording reservation start time."^
s^"録画停止ができませんでした。..ダビングを終了します。"^"Unable to stop recording.\\nDubbing will end."^
s^"録画準備中および録画中は変更できません。"^"Cannot be changed while preparing to record or recording"^
s^"録画準備中および録画中は変更できません。"^"Cannot be changed while preparing to record or recording."^
s^"録画準備中です。..ダビングを実行するには、録画予約を削除してください。"^"Preparing for recording. \\nTo perform dubbing, please delete the recording reservation."^
s^"録画準備中です。..ダビングを実行するには、録画予約を削除してください。"^"Preparing to record.\\nTo perform dubbing, please delete the recording schedule."^
s^"録画準備中です。..チャンネルを切換えることはできません。"^"Preparing to record.\\nCannot switch channels."^
s^"録画準備中です。..初期化を実行するには、録画予約を削除してください。"^"Preparing for recording. \\nTo initialize, please delete the recording schedule."^
s^"録画準備中です。..初期化を実行するには、録画予約を削除してください。"^"Preparing to record.\\nTo initialize, please delete the recording schedule."^
s^"録画準備中です。..別の録画を実行することはできません。"^"Preparing to record.\\nYou cannot perform another recording."^
s^"録画準備中です。..最適化するには、録画予約を削除してください。"^"Preparing to record.\\nFor optimization, please delete the recording schedule."^
s^"録画準備中です。..録画モードを変更できません。"^"Preparing to record.\\nUnable to change recording mode."^
s^"録画準備中のため、ビデオ作成できません。"^"Unable to create video because it is preparing to record."^
s^"録画禁止の設定に失敗しました。..ダビングを開始できません。"^"Failed to set recording prohibition.\\nUnable to start dubbing."^
s^"録画終了日時 :"^"Started:"^
s^"録画開始日時 :"^"Ended:"^
s^"録画開始時刻になると中止して終了します。"^"When the recording start time arrives, it will stop and end."^
s^"録音品質を正しく設定してください。"^"Please set the recording quality correctly."^
s^"録音方式を正しく設定してください。"^"Please set the recording method correctly."^
s^"長い"^"long"^
s^"閉じる"^"close"^
s^"開始"^"Start"^
s^"開始してください。"^"Please begin."^
s^"開始時刻"^"Start time"^
s^"開始時刻を正しく設定してください。"^"Please set the start time correctly."^
s^"開発されたソフトウェアが使用されています。"^"Developed software is used."^
s^"間"^"while"^
s^"関連するPict Storyを削除します。"^"Delete the associated Pict Story."^
s^"除外ワード"^"Exclude word"^
s^"電源は切らないでください。"^"Please do not turn off the power."^
s^"電源を入れなおしてください。"^"Please turn the power back on."^
s^"電源を切っても、ダビングは継続されます。"^"Dubbing will continue even if the power is turned off."^
s^"電源を切らずにお待ちください。"^"Please wait without turning off the power."^
s^"韓国語"^"Korean"^
s^"韓文"^"Korean text"^
s^"韩文"^"韩文"^
s^"音声"^"audio"^
s^"音声がありません。"^"No audio."^
s^"音声トラック自動選定"^"Automatic audio \\ntrack selection"^
s^"音声は１つだけです。"^"There's only one voice."^
s^"音声モード :"^"Voice mode:"^
s^"音声切り換え"^"Audio switching"^
s^"音声言語"^"Spoken language"^
s^"音声連動"^"Voice interlock"^
s^"音楽CD"^"Audio CD"^
s^"音楽"^"music"^
s^"音質 :"^"Sound quality:"^
s^"飛ばし"^"skip"^
s^"香港"^"Hong Kong"^
s^"高速"^"Fast"^
s^"03/5/27 13:33更新"^"Updated 03/5/27 13:33"^
s^"4：3パンスキャン"^"4:3 pan scan"^
s^"4:3 パンスキャン"^"4:3 Pan Scan"^
s^"4：3レターボックス"^"4:3 letterbox"^
s^"4:3 レターボックス"^"4:3 Letterbox"^
s^"CD取り込み設定"^"CD import settings"^
s^"DNAS認証エラーです"^"DNAS authentication error"^
s^"DNS（ドメインネームシステム）の設定方法を変更します。"^"Change how DNS (Domain Name System) is configured."^
s^"DTS出力"^"DTS output"^
s^"DVDに書き込む順番を確認してください。"^"Please check the order in which you want to write the DVD."^
s^"DVDメニュー表示言語"^"DVD menu display language"^
s^"feega-IDとパスワードは、feegaに対応したゲームなどのコンテンツ／サービスを楽しむ際に必要となります。..共通問合せ番号は、feegaカスタマーセンターへ問い合わせるときに必要です。..忘れないようにすべて控えておいてください。"^"feega-ID and password are required to enjoy content/services such as games compatible with feega.\nThe common inquiry number is required when contacting the feega customer center.\nDon't forget to write it down. Please leave it there."^
s^"feega-IDとパスワードを保存する"^"Save feega-ID and password"^
s^"feegaガイド"^"feega guide"^
s^"feegaから退会する"^"Withdraw from feega"^
s^"feegaでできること"^"What you can do with feega"^
s^"feegaとは"^"What is feega?"^
s^"feegaへ入会する"^"Join feega"^
s^"HDD読込エラーです。"^"HDD reading error."^
s^"HDD領域設定で、ゲーム領域の変更を"^"Change the game area in HDD area settings"^
s^"「ＨＤＤ領域設定」でのゲーム領域は"^"The game area in HDD area settings is"^
s^"HDD領域設定を行うと、ハードディスクの内容は"^"If you set the HDD area, the contents of the hard disk will be changed."^
s^"HTTPエラー"^"HTTP error"^
s^"IPアドレス"^"IP address"^
s^"IPアドレスの設定方法を変更します。"^"Change the IP address setting method."^
s^"Ｌ２・Ｒ２ボタンにサーチの機能を設定します。"^"Set the search function to the L2 and R2 buttons."^
s^"Ｌ２・Ｒ２ボタンにフラッシュの機能を設定します。"^"Set the flash function to the L2 and R2 buttons."^
s^"Ｌ２・Ｒ２ボタン設定"^"L2/R2 button settings"^
s^<LINE>14020です。ネットワークに接続できないときは、14020</LINE>^<LINE>This is 14020. If you cannot connect to the network, 14020</LINE>^
s^<LINE>(2003年6月現在)。</LINE>^<LINE> (as of June 2003). </LINE>^
s^<LINE>  2003年6月現在、VISA、Master、JCB、Diners</LINE>^<LINE> As of June 2003, VISA, Master, JCB, Diners</LINE>^
s^<LINE>  feega-IDおよびパスワードが第三者に漏洩してしまった</LINE>^<LINE> feega-ID and password were leaked to a third party</LINE>^
s^<LINE>・feega-IDおよびパスワードは、"PlayStation BB Unit"</LINE>^<LINE>・feega-ID and password are "PlayStation BB Unit"</LINE>^
s^<LINE>・feega-IDおよびパスワードは、これを保有しているお客</LINE>^<LINE>・Feega-ID and password are available to customers who have them</LINE>^
s^<LINE>・feega-IDおよびパスワードは、メモを取るなどして大切</LINE>^<LINE>・Feega-ID and password are important, such as taking notes</LINE>^
s^<LINE>feega-IDおよびパスワードを、</LINE>^<LINE>feega-ID and password,</LINE>^
s^<LINE>feega-IDおよびパスワードを保存することができます。</LINE>^<LINE>feega-ID and password can be saved. </LINE>^
s^<LINE>feega-IDおよびパスワードを入力する必要がなくなりま</LINE>^<LINE>You no longer need to enter your feega-ID and password</LINE>^
s^<LINE>    ・feega-IDおよびパスワードを忘れた場合は、メール</LINE>^<LINE> ・If you have forgotten your feega-ID and password, please send an email</LINE>^
s^<LINE>・feega-IDおよび共通問合せ番号の変更・再発行はできま</LINE>^<LINE>・feega-ID and common inquiry number cannot be changed or reissued</LINE>^
s^<LINE>feega-IDとパスワードを保存する</LINE>^<LINE>Save feega-ID and password</LINE>^
s^<LINE>feega-IDは使用できません。</LINE>^<LINE>feega-ID cannot be used. </LINE>^
s^<LINE>feega-IDや共通問合せ番号が通知されます。</LINE>^<LINE>feega-ID and common inquiry number will be notified. </LINE>^
s^<LINE>feega-IDを使用してコンテンツ/サービスを購入するごと</LINE>^<LINE>Every time you purchase content/services using feega-ID</LINE>^
s^<LINE>    ※feega-ID、共通問合せ番号、受付番号およびエラー</LINE>^<LINE> *feega-ID, common inquiry number, reception number and error</LINE>^
s^<LINE>「feega」およびfeegaロゴは、株式会社ソニー</LINE>^<LINE>"feega" and the feega logo are Sony Corporation</LINE>^
s^<LINE>    feegaカスタマーセンター</LINE>^<LINE> feega customer center</LINE>^
s^<LINE>    feegaカスタマーセンターへお問い合わせください。</LINE>^<LINE> Please contact feega customer center. </LINE>^
s^<LINE>  →feegaカスタマーセンターへお問い合わせください。</LINE>^<LINE> →Please contact the feega customer center. </LINE>^
s^<LINE>  feegaカスタマーセンターへのお問い合わせに必要で</LINE>^<LINE> Necessary for inquiries to feega customer center</LINE>^
s^<LINE>feegaカスタマーセンターへのお問い合わせに必要ですの</LINE>^<LINE>Necessary for inquiries to feega customer center</LINE>^
s^<LINE>feegaから退会する</LINE>^<LINE>Unsubscribe from feega</LINE>^
s^<LINE>feegaご利用前の準備として、次の点をご確認ください。</LINE>^Please check the following points in preparation before using <LINE>feega. </LINE>^
s^<LINE>feegaでできること</LINE>^<LINE>What you can do with feega</LINE>^
s^<LINE>・feegaでは、お客様のプライバシー保護のため、SSL暗</LINE>^<LINE>・feega uses SSL encryption to protect your privacy</LINE>^
s^<LINE>  feegaで使うfeega-IDやパスワードを忘れた</LINE>^<LINE> I forgot my feega-ID and password for feega</LINE>^
s^<LINE>feegaで利用できる決済方法のひとつです。</LINE>^This is one of the payment methods available on <LINE>feega. </LINE>^
s^<LINE>feegaとは</LINE>^<LINE>What is feega</LINE>^
s^<LINE>feegaに入会すると、feegaに対応したゲームなどのコン</LINE>^<LINE>When you join feega, you can access games and other computers that are compatible with feega</LINE>^
s^<LINE>feegaに対応したコンテンツ/サービスの購入を行います。</LINE>^Purchase content/services compatible with <LINE>feega. </LINE>^
s^<LINE>・feegaに対応していないコンテンツ/サービスを利用する</LINE>^<LINE>・Using content/services that are not compatible with feega</LINE>^
s^<LINE>  feegaに関するお問い合わせ、サービス購入手続きに</LINE>^<LINE> For inquiries regarding feega and service purchase procedures</LINE>^
s^<LINE>feegaのご利用にあたっては、次の点をご確認ください。</LINE>^When using <LINE>feega, please check the following points. </LINE>^
s^<LINE>  feegaの登録完了や退会完了のお知らせメールが届か</LINE>^<LINE> Did you receive an email notification of feega registration completion or withdrawal completion?</LINE>^
s^<LINE>feegaの退会手続きを行います。</LINE>^<LINE> Perform the withdrawal procedure from feega. </LINE>^
s^<LINE>「feega(フィーガ)」とは、株式会社ソニーファイナンス</LINE>^<LINE>What is "feega"?Sony Finance Co., Ltd.</LINE>^
s^<LINE>  →feegaへの入会・退会、登録内容の確認・変更、</LINE>^<LINE> → Joining/withdrawing from feega, checking/changing registration details, </LINE>^
s^<LINE>feegaへ入会する</LINE>^<LINE>Join feega</LINE>^
s^<LINE>    「feega」ホームページ</LINE>^<LINE> "feega" homepage</LINE>^
s^<LINE>  「feega」ホームページについて</LINE>^<LINE> About the "feega" homepage</LINE>^
s^<LINE>    「feega」ホームページは、携帯電話からはアクセス</LINE>^<LINE> The "feega" homepage can be accessed from a mobile phone</LINE>^
s^<LINE>feegaメニューの「feega-IDとパスワードの保存」から</LINE>^<LINE>From "Save feega-ID and password" in the feega menu</LINE>^
s^<LINE>feegaメニューの「feega入会」から操作します。</LINE>^<LINE> Operate from "feega membership" in the feega menu. </LINE>^
s^<LINE>feegaメニューの「feega登録内容の確認・変更」から</LINE>^<LINE>Click "Confirm/Change feega registration details" in the feega menu</LINE>^
s^<LINE>feegaメニューの「feega退会」から操作します。</LINE>^<LINE> Operate from "feega withdrawal" in the feega menu. </LINE>^
s^<LINE>feegaやfeegaに対応したあらゆるコンテンツ/サービス</LINE>^<LINE>feega and all content/services compatible with feega</LINE>^
s^<LINE>feegaを利用して購入できる有料コンテンツ/サービスの</LINE>^<LINE>Paid content/services that can be purchased using feega</LINE>^
s^<LINE>feegaを利用するときは、あらかじめ"PlayStation 2"</LINE>^<LINE>When using feega, please install "PlayStation 2" in advance</LINE>^
s^<LINE>  feegaを退会したことにはなりません。また、利用中の</LINE>^<LINE> This does not mean that you have withdrawn from feega. Also, the </LINE>^
s^<LINE>feegaを退会すると、feegaに対応したコンテンツ/</LINE>^<LINE>If you unsubscribe from feega, content compatible with feega/</LINE>^
s^<LINE>feega入会時に発行されるfeega-IDごとに固有の番号で</LINE>^<LINE>A unique number for each feega-ID issued when joining feega</LINE>^
s^<LINE>feega入会時に発行されるfeegaの会員番号です。</LINE>^<LINE> This is the feega membership number issued when you join feega. </LINE>^
s^<LINE>  feega利用規約と個人情報収集利用提供規約の内容は、</LINE>^<LINE> The contents of feega terms of use and personal information collection and provision terms are as follows</LINE>^
s^<LINE>「feega登録内容の確認・変更」で、いつでも確認/変更</LINE>^<LINE>Confirm/change at any time with "feega registration details"</LINE>^
s^<LINE>  「feega規約確認」でいつでもご確認いただけます。</LINE>^<LINE> You can check it at any time by clicking "Confirm feega terms". </LINE>^
s^<LINE>@@FONT"@@◆クレジットカードまたはウェブマネーを用意する◆</LINE>^<LINE>@@FONT"@@◆Prepare a credit card or web money◆</LINE>^
s^<LINE>@@FONT"@@◆コースを選択する◆</LINE>^<LINE>@@FONT"@@◆Select a course◆</LINE>^
s^<LINE>@@FONT"@@【ご注意】</LINE>^<LINE>@@FONT"@@[Note]</LINE>^
s^<LINE>@@FONT@@    【ご注意】</LINE>^<LINE>@@FONT@@ [Note]</LINE>^
s^<LINE>@@FONT"@@◆サービスの利用履歴を見る◆</LINE>^<LINE>@@FONT"@@◆View service usage history◆</LINE>^
s^<LINE>@@FONT"@@【ヒント】</LINE>^<LINE>@@FONT"@@[Hint]</LINE>^
s^<LINE>@@FONT"@@◆メールアドレスを用意する◆</LINE>^<LINE>@@FONT"@@◆Prepare an email address◆</LINE>^
s^<LINE>@@FONT"@@◆ルータ設定について◆</LINE>^<LINE>@@FONT"@@◆About router settings◆</LINE>^
s^<LINE>@@FONT"@@◆利用可能サービスを見る◆</LINE>^<LINE>@@FONT"@@◆See available services◆</LINE>^
s^<LINE>@@FONT"@@◆購入するコンテンツ/サービスを選択する◆</LINE>^<LINE>@@FONT"@@◆Select the content/service to purchase◆</LINE>^
s^<LINE>  "PlayStation 2"専用メモリーカード(8MB)のどちら</LINE>^<LINE> Which memory card (8MB) is for "PlayStation 2"</LINE>^
s^<LINE>"PlayStation BB Unit"および"PlayStation 2"</LINE>^<LINE>"PlayStation BB Unit" and "PlayStation 2"</LINE>^
s^<LINE>・"PlayStation BB Unit"および"PlayStation 2"</LINE>^<LINE>・"PlayStation BB Unit" and "PlayStation 2"</LINE>^
s^<LINE>    TEL 045-650-2386 10:00～18:00(年中無休)</LINE>^<LINE> TEL 045-650-2386 10:00-18:00 (Open all year round)</LINE>^
s^<LINE>  アを利用する場合は、各ソフトウェアの解説書をご覧く</LINE>^If you use <LINE>A, please refer to the manual for each software</LINE>^
s^<LINE>インターナショナルが提供する簡単・便利な会員制認証</LINE>^<LINE>Easy and convenient membership authentication provided by International</LINE>^
s^<LINE>    い。共通問合せ番号、受付番号などをお手元にご用意</LINE>^<LINE> Yes. Have your common inquiry number, reception number, etc. handy</LINE>^
s^<LINE>      い合わせには対応できません。</LINE>^<LINE> We are unable to accommodate requests. </LINE>^
s^<LINE>・ウェブマネー</LINE>^<LINE>・Web money</LINE>^
s^<LINE>ウェブマネーカードの銀部分をコインなどを使って削り取</LINE>^<LINE>Remove the silver part of the WebMoney card using a coin etc.</LINE>^
s^<LINE>  ウェブマネーについての不明点は、ウェブマネーユー</LINE>^<LINE> If you have any questions about Web Money, please use Web Money</LINE>^
s^<LINE>  ウェブマネーについて詳しくは、ウェブマネーホーム</LINE>^<LINE> For more information about Web Money, please visit Web Money Home</LINE>^
s^<LINE>  ウェブマネーは、1Point=1円です。</LINE>^<LINE> Web money is 1 Point = 1 yen. </LINE>^
s^<LINE>  ウェブマネーは、簡単にインターネット上の支払いに利</LINE>^<LINE> WebMoney makes it easy to use for online payments</LINE>^
s^<LINE>      ウェブマネーホームページ</LINE>^<LINE> Web money homepage</LINE>^
s^<LINE>      ウェブマネーユーザーサポート</LINE>^<LINE> Web Money User Support</LINE>^
s^<LINE>      ウェブマネー発行元</LINE>^<LINE> Webmoney issuer</LINE>^
s^<LINE>う名称で保存されます。</LINE>^It will be saved with the name <LINE>. </LINE>^
s^<LINE>    およびサービスの解約は「feega」ホームページで</LINE>^<LINE> and service cancellation on the "feega" homepage</LINE>^
s^<LINE>  およびパスワードは1組ずつです。コンテンツ/サービス</LINE>^There is one set of <LINE> and password. Content/Service</LINE>^
s^<LINE>    お問い合わせのときには、ご本人確認をさせていただ</LINE>^<LINE> When making an inquiry, we will ask you to confirm your identity</LINE>^
s^<LINE>  お客様の個人情報を外部に漏れることなく、安全に送信</LINE>^<LINE> Send your personal information securely without leaking it to outside parties</LINE>^
s^<LINE>お客様情報の登録が完了すると、画面およびメールで、</LINE>^<LINE>Once you have completed registering your customer information, you will receive a message on the screen and via email.</LINE>^
s^<LINE>お客様情報の確認・変更を行います。</LINE>^<LINE> Check and change customer information. </LINE>^
s^<LINE>お客様情報を画面の指示に従って設定します。</LINE>^<LINE>Set your customer information according to the on-screen instructions. </LINE>^
s^<LINE>お客様情報を登録して、feegaへの入会手続きを行いま</LINE>^<LINE>Register your customer information and complete the registration procedure for feega</LINE>^
s^<LINE>お客様情報を確認・変更する</LINE>^<LINE>Confirm/change customer information</LINE>^
s^<LINE>お支払いに使用するクレジットカードの情報です。</LINE>^<LINE>Credit card information used for payment. </LINE>^
s^<LINE>  お近くのコンビニエンスストア、パソコンショップ、</LINE>^<LINE> Nearby convenience store, computer shop, </LINE>^
s^<LINE>  かしか利用できない場合があります。</LINE>^<LINE> may be the only option available. </LINE>^
s^<LINE>か？」で「いいえ」を選択しても、ここから新規で保存す</LINE>^<LINE>? Even if you select "No", you can save as a new file from here</LINE>^
s^<LINE>がネットワークに接続されている必要があります。</LINE>^<LINE> must be connected to the network. </LINE>^
s^<LINE>  が必要です。</LINE>^<LINE> is required. </LINE>^
s^<LINE>    ください。</LINE>^<LINE> Please. </LINE>^
s^<LINE>  ください。</LINE>^<LINE> Please. </LINE>^
s^<LINE>  くなりますが、一定期間中に同じコンテンツ/サービス</LINE>^<LINE> However, the same content/service within a certain period of time</LINE>^
s^<LINE>・クレジットカード</LINE>^<LINE>・Credit card</LINE>^
s^<LINE>  クレジットカードとウェブマネーを利用できます。</LINE>^<LINE> Credit cards and web money can be used. </LINE>^
s^<LINE>・クレジットカードによっては、クレジットカード情報登</LINE>^<LINE>・Credit card information registration may be required depending on the credit card</LINE>^
s^<LINE>  クレジットカードのみで、feegaへクレジットカード</LINE>^<LINE> Credit card only to feega</LINE>^
s^<LINE>クレジットカード情報を登録しておくと、コンテンツ/</LINE>^<LINE>If you register your credit card information, you can receive content/</LINE>^
s^<LINE>    く場合があります。</LINE>^<LINE> may appear. </LINE>^
s^<LINE>  ゲームショップなどでお買い求めいただけます。</LINE>^<LINE> You can purchase it at game shops, etc. </LINE>^
s^<LINE>    ・ゲームの内容や"PlayStation 2"についてのお問</LINE>^<LINE> - Questions about game content or "PlayStation 2"</LINE>^
s^<LINE>コースの種類には、継続コース、一定期間コースがありま</LINE>^<LINE>Course types include continuous courses and fixed period courses</LINE>^
s^<LINE>  コンテンツ/サービスの利用状況にかかわらず、解約す</LINE>^<LINE> Cancel your subscription regardless of content/service usage status</LINE>^
s^<LINE>コンテンツ/サービスの料金の回収は、株式会社ソニー</LINE>^<LINE>Collection of fees for content/services is handled by Sony Corporation</LINE>^
s^<LINE>コンテンツ/サービスの有効期限や料金は、コースの選択</LINE>^<LINE>Choose a course for content/service expiration dates and fees</LINE>^
s^<LINE>  コンテンツ/サービスも解約されません。解約するとき</LINE>^<LINE> Content/services will not be canceled either. When canceling the contract</LINE>^
s^<LINE>コンテンツ/サービスを解約する</LINE>^<LINE>Cancel content/service</LINE>^
s^<LINE>コンテンツ/サービスを解約するときは、画面の指示に</LINE>^<LINE>When canceling content/services, follow the on-screen instructions</LINE>^
s^<LINE>コンテンツ/サービスを購入するときは、画面の指示に</LINE>^<LINE>When purchasing content/services, follow the on-screen instructions</LINE>^
s^<LINE>コンテンツ/サービスを購入すると、画面およびメール</LINE>^<LINE>When you purchase content/services, the screen and email</LINE>^
s^<LINE>コンテンツ/サービスを選択する画面に、以下のいずれか</LINE>^<LINE>On the content/service selection screen, select one of the following</LINE>^
s^<LINE>  コンテンツ/サービス内容について</LINE>^<LINE> About content/service details</LINE>^
s^<LINE>コンテンツ/サービス提供元へのお問い合わせには、</LINE>^<LINE>For inquiries to content/service providers, please contact us</LINE>^
s^<LINE>ご利用前の準備</LINE>^<LINE>Preparation before use</LINE>^
s^<LINE>  ザーサポートへお問い合わせください。</LINE>^<LINE> Please contact customer support. </LINE>^
s^<LINE>サービスおよびネットワーク課金サービスです。</LINE>^<LINE> service and network billing service. </LINE>^
s^<LINE>サービスの利用履歴を確認することができます。</LINE>^You can check your usage history for <LINE> services. </LINE>^
s^<LINE>「サービスの利用明細確認」で、購入したコンテンツ/</LINE>^<LINE>Contents purchased through "Confirm service usage details"/</LINE>^
s^<LINE>サービスを利用できなくなります。</LINE>^You will no longer be able to use the <LINE> service. </LINE>^
s^<LINE>サービスを購入するときに、その都度クレジットカード情</LINE>^<LINE>Credit card information each time you purchase a service</LINE>^
s^<LINE>/サービスを購入するときに使用します。</LINE>^<LINE>/Used when purchasing services. </LINE>^
s^<LINE>  され、"PlayStation 2"専用メモリーカード(8MB)が</LINE>^<LINE> ``PlayStation 2'' dedicated memory card (8MB)</LINE>^
s^<LINE>  します。同じ月に同じ継続コースの購入・解約を複数回</LINE>^<LINE>. Purchased/cancelled the same continuous course multiple times in the same month</LINE>^
s^<LINE>ショナルがコンテンツ/サービス提供元から受託して行い</LINE>^<LINE>Contracted by content/service provider</LINE>^
s^<LINE>す。feega-IDおよびパスワードは、feega-ID情報とい</LINE>^<LINE> Feega-ID and password are feega-ID information</LINE>^
s^<LINE>す。feegaカスタマーセンターにお問い合わせをするとき</LINE>^<LINE> When contacting feega customer center</LINE>^
s^<LINE>す。feegaへの入会、基本サービスの利用、および年会費</LINE>^<LINE> Joining feega, using basic services, and annual membership fee</LINE>^
s^<LINE>  す。</LINE>^<LINE> </LINE>^
s^<LINE>スが必要です。携帯電話のメールアドレスでも登録可能で</LINE>^<LINE> is required. You can also register using your mobile phone email address</LINE>^
s^<LINE>すが、文字数制限や受信制限等により正しく通知されない</LINE>^<LINE>However, notifications are not being sent correctly due to character limit, reception limit, etc.</LINE>^
s^<LINE>  す。セキュリティコードは、クレジットカード裏面の署</LINE>^<LINE> The security code is the signature on the back of your credit card.</LINE>^
s^<LINE>  するためのものです。</LINE>^<LINE> is for. </LINE>^
s^<LINE>するときに必要です。</LINE>^Required when <LINE>. </LINE>^
s^<LINE>するまで、継続コースでのコンテンツ/サービスの利用が</LINE>^<LINE>Continued course content/services may not be used until </LINE>^
s^<LINE>  す。共通問合せ番号は、「feega登録内容の確認・変</LINE>^<LINE> The common inquiry number is ``Confirmation/Change of feega registration details</LINE>^
s^<LINE>  せん。</LINE>^<LINE> No. </LINE>^
s^<LINE>センターやコンテンツ/サービス提供元へお問い合わせを</LINE>^<LINE>Contact the center or content/service provider</LINE>^
s^<LINE>  ターナショナルは責任を負いかねます。</LINE>^<LINE> International is not responsible. </LINE>^
s^<LINE>  ださい。</LINE>^<LINE> Please. </LINE>^
s^<LINE>  タテインメントおよび株式会社ソニーファイナンスイン</LINE>^<LINE> Tatainment and Sony Finance Incorporated</LINE>^
s^<LINE>  た行為については、株式会社ソニー・コンピュータエン</LINE>^<LINE> Please contact Sony Computer Engineering Co., Ltd.</LINE>^
s^<LINE>  ついて(Q.01～Q.02に該当しない場合)</LINE>^Regarding <LINE> (if not applicable to Q.01~Q.02)</LINE>^
s^<LINE>ツ/サービスは自動的に解約されます。</LINE>^The <LINE> service will be automatically canceled. </LINE>^
s^<LINE>  できます。</LINE>^<LINE> Yes. </LINE>^
s^<LINE>    できません。</LINE>^<LINE> Not possible. </LINE>^
s^<LINE>      てください。</LINE>^<LINE> Please. </LINE>^
s^<LINE>    です。</LINE>^<LINE>. </LINE>^
s^<LINE>      ではなく、必ず電話でお問い合わせください。</LINE>^Please be sure to contact us by phone, not <LINE>. </LINE>^
s^<LINE>で、メモを取るなどして大切に保管してください。</LINE>^Please take notes on <LINE> and keep it in a safe place. </LINE>^
s^<LINE>テンツ/サービスに対して、簡単に料金の支払いを行うこ</LINE>^<LINE>Easily pay for content/services</LINE>^
s^<LINE>テンツ/サービスを確認することができます。</LINE>^You can check <LINE> contents/services. </LINE>^
s^<LINE>で、受付番号や共通問合せ番号が通知されます。</LINE>^You will be notified of the reception number and common inquiry number on <LINE>. </LINE>^
s^<LINE>トアやパソコンショップなどの取扱店で購入できます。</LINE>^It can be purchased at retailers such as <LINE>Toa and computer shops. </LINE>^
s^<LINE>トカードの有効/無効をクレジットカード会社に確認しま</LINE>^<LINE>Confirm with your credit card company whether your credit card is valid or invalid</LINE>^
s^<LINE>とができます。なお、feegaを利用して購入できる有料</LINE>^<LINE> is available. In addition, there is a paid fee that can be purchased using feega</LINE>^
s^<LINE>と確認された場合は、他の有効なクレジットカードを登録</LINE>^If confirmed as <LINE>, register another valid credit card</LINE>^
s^<LINE>  ない</LINE>^<LINE> No</LINE>^
s^<LINE>  ないでください。</LINE>^<LINE> Please don't. </LINE>^
s^<LINE>  なお、feega-IDと同時に通知される共通問合せ番号は、</LINE>^<LINE> In addition, the common inquiry number notified at the same time as feega-ID is </LINE>^
s^<LINE>なお、クレジットカード情報は、feegaメニューの</LINE>^<LINE>In addition, credit card information can be found in the feega menu</LINE>^
s^<LINE>  なお、一定期間コースは、一度購入すると解約できま</LINE>^<LINE> Please note that once you purchase a certain period course, you cannot cancel it</LINE>^
s^<LINE>    なお、退会、登録内容の確認・変更、およびサービス</LINE>^<LINE> Please note that you may wish to cancel your membership, confirm/change your registration details, and provide services</LINE>^
s^<LINE>なります。また、当月ご利用分が当月末日付けで表示され</LINE>^It will be <LINE>. Also, the usage amount for the current month will be displayed as of the last day of the month</LINE>^
s^<LINE>ナンスインターナショナルは、登録したクレジットカード</LINE>^<LINE>Nance International uses registered credit cards</LINE>^
s^<LINE>について、登録時および登録後一定期間ごとに、クレジッ</LINE>^For <LINE>, you will receive a credit card at the time of registration and at regular intervals after registration.</LINE>^
s^<LINE>になる場合があります。詳しくは、お使いの通信機器に付</LINE>^It may be <LINE>. For more information, please refer to your communication device</LINE>^
s^<LINE>  によっては、"PlayStation BB Unit"および</LINE>^Depending on <LINE>, "PlayStation BB Unit" and </LINE>^
s^<LINE>により決まります。</LINE>^Determined by <LINE>. </LINE>^
s^<LINE>  に保管してください。</LINE>^Please save it to <LINE>. </LINE>^
s^<LINE>に必要です。</LINE>^Required for <LINE>. </LINE>^
s^<LINE>に発行される番号です。</LINE>^This is the number issued to <LINE>. </LINE>^
s^<LINE>のアイコンが表示されます。</LINE>^The <LINE> icon will be displayed. </LINE>^
s^<LINE>のポートを通過させるようにルータ設定をしてください。</LINE>^Please configure your router to allow the <LINE> port to pass through. </LINE>^
s^<LINE>  の各ロゴマークが入ったクレジットカードを使用できま</LINE>^You can use credit cards with <LINE> logo marks</LINE>^
s^<LINE>    の解約を行うには、feega-IDとパスワードが必要</LINE>^<LINE> To cancel your subscription, you need feega-ID and password</LINE>^
s^<LINE>はじめにお読みください</LINE>^<LINE>Please read first</LINE>^
s^<LINE>・パスワードは他人に教えたりしないよう適切に管理して</LINE>^<LINE>・Manage your password appropriately so as not to share it with others</LINE>^
s^<LINE>・パスワードは定期的に変更することをおすすめします。</LINE>^<LINE>・We recommend changing your password regularly. </LINE>^
s^<LINE>パスワードを入力するときは、以下の点に注意してくださ</LINE>^<LINE>Please note the following when entering your password</LINE>^
s^<LINE>  は、入力していただいた情報を暗号化することによって</LINE>^<LINE> encrypts the information you enter</LINE>^
s^<LINE>      は携帯電話番号)、およびメールアドレスを記載し</LINE>^<LINE> is your mobile phone number) and email address</LINE>^
s^<LINE>  は、本feegaガイドの「コンテンツ/サービスを解約す</LINE>^<LINE> is the "Cancel content/services</LINE>" section of this feega guide.^
s^<LINE>は無料です。購入するコンテンツ/サービスごとに料金が</LINE>^<LINE> is free. There is a fee for each content/service you purchase</LINE>^
s^<LINE>ファイナンスインターナショナルがコンテンツ/サービス</LINE>^<LINE>Financial International content/services</LINE>^
s^<LINE>ファイナンスインターナショナルの登録申請中の商標です</LINE>^<LINE>This is a trademark pending registration of Finance International</LINE>^
s^<LINE>      フリーダイヤル  0120-364-033</LINE>^<LINE> Toll-free number 0120-364-033</LINE>^
s^<LINE>  ページをご覧ください。</LINE>^Please see the <LINE> page. </LINE>^
s^<LINE>  また、feegaに対応していないゲームなどのソフトウェ</LINE>^<LINE> Also, software such as games that are not compatible with feega</LINE>^
s^<LINE>また、保存したfeega-IDおよびパスワードを、確認・変</LINE>^<LINE>Also, check/change your saved feega-ID and password</LINE>^
s^<LINE>マネーで、ウェブマネーでの支払いに対応したコンテンツ</LINE>^<LINE>Contents that support payment with WebMoney</LINE>^
s^<LINE>    メールアドレス feega@sfi.sony.co.jp</LINE>^<LINE> Email address feega@sfi.sony.co.jp</LINE>^
s^<LINE>      メールアドレス  support@webmoney.ne.jp</LINE>^<LINE> Email address support@webmoney.ne.jp</LINE>^
s^<LINE>    ◆メールでお問い合わせの場合</LINE>^<LINE> ◆For inquiries by email</LINE>^
s^<LINE>  メールによるお問い合わせにはすぐに返答できないこと</LINE>^<LINE> Not being able to respond immediately to inquiries by email</LINE>^
s^<LINE>  もあります。</LINE>^There is also <LINE>. </LINE>^
s^<LINE>  や"PlayStation 2"専用メモリーカード(8MB)に保存</LINE>^<LINE> or "PlayStation 2" dedicated memory card (8MB)</LINE>^
s^<LINE>  ります。</LINE>^<LINE> </LINE>^
s^<LINE>ルータ設定をするときに、コンピュータからの設定が必要</LINE>^<LINE>When configuring the router, settings from the computer are required</LINE>^
s^<LINE>ることができます。</LINE>^You can <LINE>. </LINE>^
s^<LINE>ると、 英数字または記号で構成されたスクラッチ番号が</LINE>^<LINE>, a scratch number consisting of alphanumeric characters or symbols will appear.</LINE>^
s^<LINE>  るまで毎月一定額が引き落とされます。支払方法は</LINE>^A fixed amount will be deducted every month until <LINE> is reached. Payment method is</LINE>^
s^<LINE>  る」をご覧ください。</LINE>^<LINE> </LINE>^
s^<LINE>  わせください。</LINE>^<LINE> Please let me know. </LINE>^
s^<LINE>  を保存する」をご覧ください。</LINE>^Please refer to ``Save <LINE>''. </LINE>^
s^<LINE>を利用するときに必要です。</LINE>^Required when using <LINE>. </LINE>^
s^<LINE>  を必ずお読みになり、規約に同意していただく必要があ</LINE>^<LINE> Please be sure to read and agree to the terms</LINE>^
s^<LINE>  を追加で購入することで、期間の延長が行えます。</LINE>^You can extend the period by purchasing additional <LINE>. </LINE>^
s^<LINE>・一定期間コース</LINE>^<LINE>・Course for a fixed period of time</LINE>^
s^<LINE>  万一、許可なく自分のfeega-IDが利用された場合または</LINE>^<LINE> In the unlikely event that your feega-ID is used without your permission, or</LINE>^
s^<LINE>上限に支払いを行うことができます。</LINE>^<LINE> You can make payments up to the limit. </LINE>^
s^<LINE>使用しないでください。</LINE>^Please do not use <LINE>. </LINE>^
s^<LINE>  (例：姓・名・メールアドレスの全部、または一部など)</LINE>^<LINE> (Example: Last name, first name, email address, all or part, etc.)</LINE>^
s^<LINE>・保存したfeega-IDおよびパスワードを削除しても、</LINE>^<LINE>・Even if you delete the saved feega-ID and password,</LINE>^
s^<LINE>保存したfeega-IDおよびパスワードを、削除することが</LINE>^<LINE>You can delete your saved feega-ID and password</LINE>^
s^<LINE>保存したfeega-IDおよびパスワードを、確認・変更する</LINE>^<LINE>Check and change your saved feega-ID and password</LINE>^
s^<LINE>保存すると、コンテンツ/サービスを利用するときに</LINE>^<LINE>Save it when you use the content/service</LINE>^
s^<LINE>・保存などを行う前に、"PlayStation BB Unit"が接続</LINE>^<LINE>・Before saving etc., "PlayStation BB Unit" is connected.</LINE>^
s^<LINE>・個人情報保護のため、feega-IDおよびパスワードの確</LINE>^<LINE>・Ensure feega-ID and password to protect personal information</LINE>^
s^<LINE>停止されます。</LINE>^<LINE> will be stopped. </LINE>^
s^<LINE>入会するときに「feega-IDとパスワードを保存します</LINE>^<LINE>Save feega-ID and password when joining</LINE>^
s^<LINE>・入会の前にfeega利用規約と個人情報収集利用提供規約</LINE>^<LINE>・Before joining feega terms of use and personal information collection and provision terms</LINE>^
s^<LINE>入会時に登録するパスワードで、feega-IDに対応してい</LINE>^<LINE>The password you register when joining is compatible with feega-ID</LINE>^
s^<LINE>利用上のご注意</LINE>^<LINE>Precautions for use</LINE>^
s^<LINE>利用中のコンテンツ/サービスの解約手続きを行います。</LINE>^<LINE> Perform the cancellation procedure for the content/service you are currently using. </LINE>^
s^<LINE>「利用可能サービス一覧」で有効期限内の利用可能なコン</LINE>^<LINE>Available services within the expiration date in the "List of available services"</LINE>^
s^<LINE>◆削除</LINE>^<LINE>◆Delete</LINE>^
s^<LINE>      受付時間  平日10:00～18:00(土日祝日を除く)</LINE>^<LINE> Reception hours: Weekdays 10:00-18:00 (excluding Saturdays, Sundays, and holidays)</LINE>^
s^<LINE>受付番号</LINE>^<LINE>Reception number</LINE>^
s^<LINE>  号化通信を採用しています。SSL(Secure Socket Layer)</LINE>^<LINE> We use coded communication. SSL(Secure Socket Layer)</LINE>^
s^<LINE>  →各コンテンツ/サービス提供元へお問い合わせくださ</LINE>^<LINE> →Please contact each content/service provider</LINE>^
s^<LINE>同社が発行するウェブマネーカードは、コンビニエンスス</LINE>^<LINE>The web money card issued by the company is a convenience store</LINE>^
s^<LINE>  名欄にある7桁または19桁の数字の右3桁の数字です。</LINE>^<LINE> This is the 3-digit number to the right of the 7-digit or 19-digit number in the name field. </LINE>^
s^<LINE>困ったときは</LINE>^<LINE>When you are in trouble</LINE>^
s^<LINE>報を入力する必要がなくなります。登録しなくても入会手</LINE>^You no longer need to enter <LINE> information. You can join without registering</LINE>^
s^<LINE>場合があります。できるだけ携帯電話のメールアドレスは</LINE>^<LINE> may occur. If possible, please use your mobile phone email address</LINE>^
s^<LINE>  場合は、コンテンツ/サービス提供元の各社にお問い合</LINE>^<LINE> If so, please contact each content/service provider</LINE>^
s^<LINE>  場合は、ただちにfeegaカスタマーセンターにお問い合</LINE>^<LINE> If so, please contact feega customer center immediately</LINE>^
s^<LINE>  専用メモリーカード(8MB)には、25KB以上の空き容量</LINE>^<LINE> Dedicated memory card (8MB) has at least 25KB free space</LINE>^
s^<LINE>専用メモリーカード(8MB)に保存することができます。</LINE>^You can save it to the <LINE> dedicated memory card (8MB). </LINE>^
s^<LINE>  専用メモリーカード(8MB)に保存できるfeega-ID</LINE>^<LINE> feega-ID that can be saved on a dedicated memory card (8MB)</LINE>^
s^<LINE>属の取扱説明書をご覧ください。</LINE>^Please see the instruction manual for the <LINE> genus. </LINE>^
s^<LINE>履歴が確認できるのは、課金が開始された月の翌月からと</LINE>^<LINE>You can check your history from the month following the month in which billing started</LINE>^
s^<LINE>  差込口1または2に差し込まれていることを確認して</LINE>^<LINE> Make sure it is inserted into socket 1 or 2</LINE>^
s^<LINE>従って操作してください。</LINE>^<LINE>Please follow the instructions. </LINE>^
s^<LINE>必ず共通問合せ番号を使用してください。</LINE>^<LINE>Please be sure to use the common inquiry number. </LINE>^
s^<LINE>    ※必ず、共通問合せ番号、氏名、ご連絡先(自宅また</LINE>^<LINE> *Be sure to include your common inquiry number, name, and contact information (home or </LINE>^
s^<LINE>必要に応じて"PlayStation BB Navigator"からルータ</LINE>^<LINE>If necessary, connect the router from "PlayStation BB Navigator"</LINE>^
s^<LINE>  情報の登録が必要です。</LINE>^<LINE> Information registration is required. </LINE>^
s^<LINE>情報を登録する必要があります。株式会社ソニーファイ</LINE>^<LINE> information must be registered. Sony Phi Co., Ltd.</LINE>^
s^<LINE>  →手続きしてから2日以上たっても届かない場合は、手</LINE>^<LINE> →If you do not receive it within 2 days after completing the procedure, please contact us</LINE>^
s^<LINE>提供元から受託して行います。</LINE>^<LINE> This is commissioned by the provider. </LINE>^
s^<LINE>操作します。</LINE>^<LINE>operate. </LINE>^
s^<LINE>料金の回収は、株式会社ソニーファイナンスインターナ</LINE>^<LINE>Fee collection is handled by Sony Finance Inter Co., Ltd.</LINE>^
s^<LINE>◆新規保存</LINE>^<LINE>◆Save new</LINE>^
s^<LINE>  更」で確認できます。</LINE>^You can check with ``<LINE> Change''. </LINE>^
s^<LINE>更、削除することができます。</LINE>^<LINE> can be edited or deleted. </LINE>^
s^<LINE>  有効期限を過ぎるとコンテンツ/サービスは利用できな</LINE>^<LINE> Content/services cannot be used after the expiration date</LINE>^
s^<LINE>      本社 東京都品川区上大崎2丁目24番9号</LINE>^<LINE> Head office 2-24-9 Kamiosaki, Shinagawa-ku, Tokyo</LINE>^
s^<LINE>      株式会社ウェブマネー</LINE>^<LINE> Web Money Co., Ltd.</LINE>^
s^<LINE>株式会社ウェブマネーが発行するプリペイドカード型電子</LINE>^<LINE>Prepaid card type electronic issued by Web Money Co., Ltd.</LINE>^
s^<LINE>  様の責任で管理してください。当該IDを利用して行われ</LINE>^<LINE> is responsible for managing it. This is done using the ID</LINE>^
s^<LINE>・氏名、生年月日、および性別は変更できません。</LINE>^<LINE>・Name, date of birth, and gender cannot be changed. </LINE>^
s^<LINE>  決められた期間に応じて料金が引き落とされます。</LINE>^<LINE> Fees will be deducted according to the specified period. </LINE>^
s^<LINE>決済時にスクラッチ番号を入力すると、カードの購入額を</LINE>^<LINE>If you enter the scratch number at the time of payment, the purchase amount of your card will be calculated.</LINE>^
s^<LINE>現れます。</LINE>^<LINE> will appear. </LINE>^
s^<LINE>  用できるプリペイド型電子マネーです。</LINE>^This is a prepaid electronic money that can be used on <LINE>. </LINE>^
s^<LINE>用語解説</LINE>^<LINE>Term explanation</LINE>^
s^<LINE>登録内容やその他の情報を通知するための、メールアドレ</LINE>^<LINE>Email address for notification of registration details and other information</LINE>^
s^<LINE>◆確認・変更</LINE>^<LINE>◆Confirm/Change</LINE>^
s^<LINE>・第三者が容易に推測できるような番号をパスワードにし</LINE>^<LINE>・Use a number that can be easily guessed by a third party as your password</LINE>^
s^<LINE>  約した月は、解約日にかかわらず1か月分の料金が発生</LINE>^<LINE> For the month of contract, you will be charged for one month regardless of the cancellation date</LINE>^
s^<LINE>・継続コース</LINE>^<LINE>・Continuation course</LINE>^
s^<LINE>継続コースの場合、ご利用開始月は無料となるため、利用</LINE>^<LINE>For continuous courses, the first month of use is free, so use it</LINE>^
s^<LINE>継続コースを利用中に、登録したクレジットカードが無効</LINE>^<LINE>The registered credit card is invalid while using the continuation course</LINE>^
s^<LINE>継続コースを購入する場合は、feegaへクレジットカード</LINE>^<LINE>If you wish to purchase the continuation course, please send a credit card to feega</LINE>^
s^<LINE>    続きが正しく行われていない可能性があります。</LINE>^<LINE> The continuation may not have been completed correctly. </LINE>^
s^<LINE>続きはできます。</LINE>^<LINE>You can continue. </LINE>^
s^<LINE>    行うこともできます。</LINE>^You can also do <LINE>. </LINE>^
s^<LINE>  行った場合は、1か月分の料金が発生します。</LINE>^If you go to <LINE>, you will be charged for one month. </LINE>^
s^<LINE>      表示の内容などをお手元にご用意ください。</LINE>^<LINE> Please have the information displayed on hand. </LINE>^
s^<LINE>解約完了を通知するメールは送られません。</LINE>^<LINE>An email will not be sent to notify you that the cancellation is complete. </LINE>^
s^<LINE>設定をしてください。feegaで使用するポート番号は、</LINE>^Please set <LINE>. The port number used by feega is </LINE>^
s^<LINE>  詳しくは、本feegaガイドの「feega-IDとパスワード</LINE>^<LINE> For more information, see "feega-ID and password" in this feega guide</LINE>^
s^<LINE>  認・変更は、保存した"PlayStation 2"以外ではできま</LINE>^<LINE> Confirmation/changes cannot be made on any device other than the saved "PlayStation 2"</LINE>^
s^<LINE>購入したコンテンツ/サービスについて、feegaカスタマー</LINE>^<LINE>feega customer regarding purchased content/services</LINE>^
s^<LINE>  購入した月は、購入日から月末まで無料となります。解</LINE>^<LINE> The month of purchase is free from the date of purchase until the end of the month. Solution</LINE>^
s^<LINE>購入可能なコンテンツ/サービスが一覧表示されます。</LINE>^<LINE>A list of purchasable contents/services will be displayed. </LINE>^
s^<LINE>退会すると、購入しているコンテンツ/サービスが有効期</LINE>^<LINE>If you cancel your membership, the content/services you have purchased will expire</LINE>^
s^<LINE>  録時にセキュリティコードの入力が必要な場合がありま</LINE>^<LINE> You may be required to enter a security code when recording</LINE>^
s^<LINE>限内でも無効になるほか、継続コースで利用中のコンテン</LINE>^<LINE>In addition to being invalidated within the limit, content currently being used in the continuing course</LINE>^
s^<LINE>    ◆電話でお問い合わせの場合</LINE>^<LINE> ◆For inquiries by phone</LINE>^
s^"MACアドレス"^"MAC address"^
s^>     <pad_help triangle="新規登録"^> <pad_help triangle="New registration"^
s^>     <pad_help triangle="詳細"^> <pad_help triangle="Details"^
s^"PlayStation®BB Navigatorが最新では..ありません。..ページを表示するには、アップデートを行って..ください。..(%ERRORNO)"^"PlayStation®BB Navigator is not\nupdated.\nPlease update\nto view the page.\n(%ERRORNO)"^
s^"PlayStation®BB ガイド"^"PlayStation®BB Guide"^
s^"PlayStation®またはPlaystation®2規格の"^"PlayStation® or Playstation®2 standard"^
s^"ＰＰＰｏＥを使用しますか？"^"Would you like to use PPPoE?"^
s^"PSXの電源、ネットワークの接続を切らないでください。"^"Do not turn off the PSX power or network connection."^
s^"アップデートする必要はありません。"^"No need to update."^
s^"アップデートに失敗しました。"^"Update failed."^
s^"アルバムの選択"^"Select album"^
s^"アルバム内のトラック数が99トラックを越えたので"^"The number of tracks in the album exceeds 99."^
s^"アルバム内のフォトがいっぱいです。"^"The album is full of photos."^
s^"アルバム内のフォトを全て取り込みます。"^"Import all photos in the album."^
s^"アルバム名"^"Album name"^
s^"イーサーネットケーブルを正しく接続してから、"^"Connect the Ethernet cable properly, then"^
s^"イギリス"^"England"^
s^"いくつかのトラックは取り込めませんでした。"^"Some tracks could not be imported."^
s^"イタリア"^"Italy"^
s^"インストール中は画面にはしばらく何も表示されません。"^"During installation, the screen will be blank for a while."^
s^"インドネシア"^"Indonesia"^
s^"ウェブマネー"^"web money"^
s^"エラーが発生しました。 一度電源を切った後、再度電源を入れ直してください。"^"An error has occurred. Please turn off the power and then turn it on again."^
s^"エラーです"^"Error"^
s^"オーストリア"^"Austria"^
s^"オーディオトラック"^"Audio track"^
s^"オーディオプレーヤー"^"Audio player"^
s^"オランダ"^"Netherlands"^
s^"お客様情報を確認・変更する"^"Confirm/change customer information"^
s^"お買い上げの状態の戻します。"^"Restores to the state of purchase."^
s^"カナダ"^"Canada"^
s^"かんたん設定 / "^"Easy setup / "^
s^"かんたん設定で設定した値の一覧です。"^"This is a list of values set in Easy Settings."^
s^"かんたん設定を中止しますか？"^"Do you want to cancel easy setup?"^
s^"かんたん設定 / 自動チャンネル設定"^"Easy setup/Automatic channel setup"^
s^"クレジットカード情報"^"Credit card information"^
s^"ゲームが起動するまでしばらくお待ちください。"^"Please wait while the game starts."^
s^"ゲームが起動するまでしばらくお待ちください。" y="+8"^"Please wait while the game starts. y=+8"^
s^"ゲーム領域 :"^"Game area:"^
s^"ゲーム領域を"^"Game area"^
s^"ゲーム領域を作成しますか？"^"Do you want to create a game area?"^
s^"ゲーム領域を作成すると、"^"When you create a game area,"^
s^"ゲーム領域を削除しますか？"^"Do you want to delete the game area?"^
s^"ゲーム領域を削除すると、"^"If you delete the game area,"^
s^"ここではネットワークに接続するための"^"Here for connecting to the network"^
s^"このアルバム内の全てのフォトを"^"All photos in this album"^
s^"このアルバム内の全てのフォトを削除します。"^"Delete all photos in this album."^
s^"　このゲームを本機から削除します。"^"Delete this game from your device."^
s^"このサービスは、事前にクレジットカード情報の登録が必要です。feegaメニューの「feega登録内容の確認・変更」からクレジットカード情報の登録を行ってください。..(B10)"^"This service requires you to register your credit card information in advance. Please register your credit card information from ``Confirm/Change Feega Registration Contents'' in the Feega Menu. \n(B10)"^
s^"このタイトルは保護されているため、 削除は実行できません。"^"This title is protected and cannot be deleted."^
s^"このタイトルは保護されているため、 名前変更は実行できません。"^"This title is protected and cannot be renamed."^
s^"このタイトルは保護されているため、 編集は実行できません。"^"This title is protected and cannot be edited."^
s^"このタイトルは保護されているため、 編集取消しは実行できません。"^"This title is protected and cannot be undone."^
s^"このタイトルをあとでDVDにダビングする時の目安です。"^"This is a guide when dubbing this title to a DVD later."^
s^"このタイトルを削除します。よろしいですか？"^"Delete this title. Are you sure?"^
s^"このディスクを取り込むことはできません。"^"This disc cannot be ripped."^
s^"このフォトをメモリースティックに書き出します。"^"Export this photo to a memory stick."^
s^"このフォトを削除します。よろしいですか？"^"Delete this photo. Are you sure?"^
s^"このフォトを取り込みます。"^"Import this photo."^
s^"このフォトを書き出すアルバムを選択してください。"^"Please select an album to export this photo to."^
s^"この画像をビデオのサムネイルに"^"Set this image as video thumbnail"^
s^"この番組を最後まで録画しますか？ (終了"^"Do you want to record this program to the end? (End)"^
s^"この番組を最後まで録画することはできません。 1つのタイトルは最長6時間までです。"^"This program cannot be recorded in its entirety. One title can be up to 6 hours long."^
s^"この設定でよければ"^"If this setting is fine"^
s^"この音楽を取り込みますか？"^"Do you want to import this music?"^
s^"コマ数"^"Number of frames"^
s^"これからHDD領域設定を開始します。"^"We will now start setting up the HDD area."^
s^"これからインストールを開始します。"^"The installation will now begin."^
s^"これからハードディスクの初期化を開始します。"^"The hard disk will now be initialized."^
s^"これでかんたん設定を終了します。"^"This completes the easy setup."^
s^"これ以上新しいアルバムを作成できません。"^"You can't create any more new albums."^
s^"コンテンツ/サービスを解約する"^"Cancel content/service"^
s^"ご利用前の準備"^"Preparation before use"^
s^"サーバーが正常に動作しておりません。"^"The server is not working properly."^
s^"サーバーへの接続に失敗しました。"^"Failed to connect to server."^
s^"サービスメニュー"^"Service Menu"^
s^"サイズ"^"Size:"^
s^"サポート"^"support"^
s^"サムネイルに登録しました。"^"Registered as thumbnail."^
s^"サムネイル登録"^"Thumbnail registration"^
s^"されています。"^"It has been."^
s^"システムエラー"^"System error"^
s^"しないでください。"^"please do not."^
s^"ジャンル順"^"By genre"^
s^"スイス"^"Switzerland"^
s^"スウェーデン"^"Sweden"^
s^"すでに登録されています。上書きしてもよろしいですか？"^"Already registered. Are you sure you want to overwrite it?"^
s^"スペイン"^"Spain"^
s^"すべての設定を出荷時に戻さずに"^"Without resetting all settings to factory defaults"^
s^"すべて削除されますが、"^"Everything will be deleted, but"^
s^"すべて削除されますので、ご注意ください。"^"Please note that everything will be deleted."^
s^"すべて消去されます。"^"Everything will be erased."^
s^"することが出来ます。"^"You can."^
s^"する、しないを選び、"^"Choose yes or no."^
s^"セカンダリDNS"^"Secondary DNS"^
s^"その操作は実行できません。"^"The operation cannot be performed."^
s^"ソフトウェア更新が完了しました。"^"Software update completed."^
s^"ソフトウェア更新を終了します。"^"Software update will end."^
s^"タイ"^"Thailand"^
s^"タイトル削除できませんでした。"^"The title could not be deleted."^
s^"タイトル名"^"Title name"^
s^"タイムアウトになりました。"^"Timeout has expired."^
s^"ダウンロードが終わるまでは、本機の電源や、"^"Until the download is complete, the device's power supply, "^
s^"ダウンロードに失敗しました。"^"Download failed."^
s^"ダウンロード状況"^"Download status"^
s^"データがありません。"^"there is no data."^
s^"でBSアンテナ電源供給の"^"With BS antenna power supply"^
s^"でDTSの入、切を選び、"^"Select DTS on or off with "^
s^"ディスクではありません。"^"It's not a disc."^
s^"ディスクを取り出さないでください。"^"Please do not remove the disc."^
s^"ディスク種別"^"Disk type"^
s^"テクスチャ補間"^"Texture interpolation"^
s^"でダビングしたいタイトルを選択し"^"Select the title you want to dub"^
s^"でドルビーデジタルの入、切を選び、"^"Select Dolby Digital on or off,"^
s^"テレビの設定 / BSアンテナレベル表示"^"TV settings/BS antenna level display"^
s^"テレビの設定 / BSチャンネル飛ばし"^"TV settings / Skip BS channels"^
s^"テレビの設定 / 手動チャンネル設定"^"TV Settings/Manual Channel Settings"^
s^"テレビの設定 / 自動チャンネル設定"^"TV Settings / Automatic Channel Settings"^
s^"デンマーク"^"Denmark"^
s^"で光デジタル出力の"^"With optical digital output"^
s^"　  で取消す編集範囲を選び、　　　を押してください。"^"Use to select the edit range you want to cancel, and press ."^
s^"で変更したい項目を選び"^"Select the item you want to change with"^
s^"で数値を変更してください。"^"Please change the value."^
s^"で本機につないでいるテレビ画面の"^"on the TV screen connected to this unit"^
s^"で自動チャンネル設定を"^"Automatic channel setting"^
s^"で選び"^"Select"^
s^"で選んで"^"Select with"^
s^"ドイツ"^"Germany"^
s^"ド）にのみダビング可能となります。よろしいですか？"^"You can only dub to C). Are you sure?"^
s^"トラック %i"^"Track %i"^
s^"トラック %i を取り込んでいます。"^"Importing track %i."^
s^"トラックがありません。"^"No truck."^
s^"トラック名"^"Track name"^
s^"ドルビーデジタル出力"^"Dolby Digital Output"^
s^"なります。"^"Become."^
s^"なりますがよろしいですか？"^"Are you sure?"^
s^"にする場合は、かんたん設定を終了後"^"If you want to, after completing the easy setup"^
s^"に設定されています。"^"Set to."^
s^"ネットワークアップデートを中止します。"^"Network update will be canceled."^
s^"ネットワークアップデートを中止しますか？"^"Do you want to cancel network update?"^
s^"ネットワークアップデートを終了します。"^"Network update will end."^
s^"ネットワークアップデートを開始します。"^"Starting network update."^
s^"ネットワークに接続しています。"^"Connecting to the network."^
s^"ネットワークに接続します。"^"Connect to the network."^
s^"ネットワークの接続を中止しました。"^"Network connection aborted."^
s^"ネットワークの設定が正しいか確認してください。"^"Please make sure your network settings are correct."^
s^"ネットワークを切断しないでください。"^"Do not disconnect from the network."^
s^"ネットワーク設定を中止しますか？"^"Are you sure you want to cancel network settings?"^
s^"ネットワーク設定を読み込めません。"^"Unable to load network settings."^
s^"ネットワーク設定を開始してください。"^"Please begin network configuration."^
s^"ノルウェー"^"Norway"^
s^"ハードディスクに保存した映像や写真、"^"Videos and photos saved on your hard disk,"^
s^"ハードディスクの内容をすべて消去して"^"Erase everything on your hard disk"^
s^"ハードディスクの初期化が終了しました。"^"Hard disk initialization has finished."^
s^"ハードディスクの初期化が終了するまで、しばらくお待ちください。"^"Please wait for a moment until the hard disk is initialized."^
s^"ハードディスクの初期化を中止しました。"^"Hard disk initialization has been canceled."^
s^"ハードディスクの初期化を中止しますか？"^"Do you want to cancel hard disk initialization?"^
s^"ハードディスクの残量が足りません。"^"There is not enough free space on the hard disk."^
s^"ハードディスクの残量を確認しています。　"^"Checking the remaining hard disk space."^
s^"ハードディスクの空き容量を確認しています"^"Checking free space on hard disk"^
s^"ハードディスクの領域を変更できます。"^"You can change the space on your hard disk."^
s^"ハードディスクの領域設定を中止しますか？"^"Are you sure you want to cancel hard disk space settings?"^
s^"ハードディスクの領域設定を行うと、"^"When you configure the hard disk space,"^
s^"ハードディスクを使用するゲームが"^"Games that use hard disks"^
s^"ハードディスク領域説明"^"Hard disk area description"^
s^"はじめにお読みください"^"Please read first"^
s^"ファイルが壊れているため編集できません。"^"The file is corrupt and cannot be edited."^
s^"ファイルをコピーできませんでした。"^"Failed to copy file."^
s^"ファイル名"^"file name"^
s^"ファイル数"^"number of files"^
s^"フィリピン"^"Philippines"^
s^"フィンランド"^"Finland"^
s^"フォトの名前が重複しています。"^"Duplicate photo name."^
s^"フォトの名前を変更してください。"^"Please rename the photo."^
s^"プライマリDNS"^"Primary DNS"^
s^"フランス"^"France"^
s^"ページの再読み込みに失敗しました。..(%ERRORNO)"^"Failed to reload page.\n(%ERRORNO)"^
s^"ページの表示中にエラーが発生しました。"^"An error occurred while rendering the page."^
s^"ページの記述に間違いがあります。..(%ERRORNO)"^"There is an error in the page description.\n(%ERRORNO)"^
s^"ページの読み込みが中断されました。"^"Page loading was interrupted."^
s^"ページの読み込みに失敗しました。..(%ERRORNO)"^"Failed to load page.\n(%ERRORNO)"^
s^"ページの読み込みに失敗しました。..PlayStation®BB Navigatorのアップデートを行ってください。"^"Failed to load the page.\nPlease update PlayStation®BB Navigator."^
s^"ページの読み込みに失敗しました。..イーサネットケーブルが正しく接続されていることを確認してください。..その後、ネットワーク設定メニューの「確認・変更」から設定内容を確認し、ネットワークに接続してください。"^"Failed to load the page.\nPlease make sure the Ethernet cable is connected correctly.\nThen, check the settings in the network settings menu."^
s^"ベルギー"^"Belgium"^
s^"ボタンを押すとインストールを開始します。"^"Press the button to start the installation."^
s^"マーク"^"mark"^
s^"マレーシア"^"Malaysia"^
s^"ミュート設定"^"Mute settings"^
s^"メモリーカードユーティリティ"^"Memory Card Utility"^
s^"メモリースティックが正しく挿入されていることを"^"Memory Stick is inserted correctly"^
s^"メモリースティックに書き出します。"^"Export to memory stick."^
s^"メモリースティックの残量が不足しています。"^"There is not enough space left on the memory stick."^
s^"メモリースティックを抜いたりしないでください。"^"Do not remove the memory stick."^
s^"メモリースティックを抜かないでください。"^"Please do not remove the memory stick."^
s^"メモリースティック内のアルバムが多すぎます。"^"There are too many albums on the memory stick."^
s^"メモリースティック内の音楽を全て取り込みますか？"^"Do you want to import all music on the memory stick?"^
s^"メンテナンス中のためご利用できません。"^"Unavailable due to maintenance."^
s^"ユーザーID"^"USER ID"^
s^"ユーザーIDとパスワードを入力してください。"^"Please enter your user ID and password."^
s^"よろしいですか？設定後、サーチはジョイスティックの"^"Are you sure? After setting, use the joystick to search."^
s^"よろしいですか？ 設定後、フラッシュは"^"Are you sure? After setting, the flash will start."^
s^"ロシア"^"Russia"^
s^"を削除します。よろしいですか？"^"Delete. Are you sure?"^
s^"を取り込み中です。"^"Currently loading."^
s^"を取り込んでいます。"^"is being imported."^
s^"を押して"^"Press"^
s^"を押して、ダウンロードを開始してください。"^"Press to start downloading."^
s^"を押して、接続テストを開始してください。"^"Press to start the connection test."^
s^"を押して次の画面に"^"Press to go to next screen"^
s^"を押して次の画面に進んでください。"^"Press to proceed to the next screen."^
s^"を押して次の画面へ移ってください。"^"Press to move to the next screen."^
s^"を押すと、ゲーム領域を作成します。"^"Press to create a game area."^
s^"を押すと、ゲーム領域を削除します。"^"Press to delete the game area."^
s^"不可"^"Not possible"^
s^"不要なタイトルやトラック、フォトを削除"^"Delete unnecessary titles, tracks, and photos"^
s^"中国"^"China"^
s^"中止する場合、今までの作業内容は保存されません"^"If you cancel, your work will not be saved."^
s^"主＋副音声"^"Main + secondary audio"^
s^"予約が一件もありません。"^"We don't have any reservations."^
s^"二カ国語放送を録画した場合、ＤＶＤ－ＲＷ（ＶＲモー"^"When recording bilingual broadcasts, DVD-RW (VR mode)"^
s^"二カ国語放送記録音声"^"Bilingual broadcast recording audio"^
s^"二カ国語放送記録音声の設定を変更します。この設定で"^"Change the settings for bilingual broadcast recording audio. With this setting"^
s^"以下の文字を使用することはできません。"^"The following characters are not allowed:"^
s^"作成日時"^"Last Updated:"^
s^"使用しない"^"do not use"^
s^"使用する"^"use"^
s^"使用する、使用しないを選び、"^"Choose whether to use or not."^
s^"使用許諾契約に同意しない場合、ソフトウェアを"^"If you do not agree to the license agreement, please use the software"^
s^"使用許諾契約を確認したら"^"After checking the license agreement"^
s^"使用許諾条件に同意しますか？"^"Do you agree to the terms of use?"^
s^"保存した内容は、feega-IDとパスワードの保存メニューの「確認・変更」から確認することができます。"^"You can check the saved contents from ``Confirm/Change'' in the feega-ID and password save menu. "^
s^"保存しない"^"Do not save"^
s^"保存する"^"save"^
s^"保護"^"Protect"^
s^"保護解除"^"Unprotect"^
s^"保護設定されているため、登録できません。"^"Unable to register because it is protected."^
s^"入、切を選んで"^"Choose on or off"^
s^"入力が終了したら、"^"When you have finished typing,"^
s^"入力された名前は重なっているか、不正です。"^"The names you entered are duplicates or invalid."^
s^"入力する項目を"^"Enter items"^
s^"全てのデータが削除されます。"^"All data will be deleted."^
s^"全ての設定を出荷時に戻しますか？"^"Do you want to restore all settings to factory defaults?"^
s^"共通問合せ番号"^"Common inquiry number"^
s^"内部エラー"^"Internal error"^
s^"再生したくない場面の始点で、　 　  を押してください。"^"Press at the start of the scene you don't want to play."^
s^"再生したくない場面の終点で、　 　  を押してください。"^"Press at the end of the scene you don't want to play."^
s^"再生できませんでした。 一度電源を切った後、再度電源を入れ直してください。"^"Playback failed. Please turn off the power and then turn it on again."^
s^"再起動後、ハードディスク領域設定を"^"After reboot, set hard disk space"^
s^"出荷時設定に戻しています。"^"Reverting to factory settings."^
s^"出荷時設定に戻すと、"^"If you restore the factory settings,"^
s^"出荷時設定に戻すを中止しますか？"^"Are you sure you want to cancel resetting to factory settings?"^
s^"切 (入に出来ませんでした)"^"Could not turn on."^
s^"初期化が完了しました。"^"Initialization completed."^
s^"初期化してよろしいですか？"^"Are you sure you want to initialize?"^
s^"初期化しない"^"Do not initialize"^
s^"初期化する"^"initialize"^
s^"初期化できませんでした。"^"Failed to initialize."^
s^"初期化を実行しますか？"^"Do you want to perform initialization?"^
s^"初期化を行うとメモリースティックに入っている"^"If you initialize it, it will be stored on the memory stick."^
s^"初期化を開始すると中断できません。"^"Once initialization begins, it cannot be interrupted."^
s^"初期化中は本機の電源を切らないでください。"^"Do not turn off the power of this unit during initialization."^
s^"利用上のご注意"^"Precautions for use"^
s^"　削除が完了しました。"^"　Deletion completed."^
s^"削除が完了しました。"^"Delete completed."^
s^"　削除できませんでした。"^"Failed to delete."^
s^"削除中です..."^"Deleting..."^
s^"削除中は本機の電源を切らないでください。"^"Do not turn off the power of this device while deleting."^
s^"前のトラックがありません。"^"The truck in front is missing."^
s^"取り込み"^"Import"^
s^"取り込みが終了しましたが、"^"The import has finished, but"^
s^"取り込みに失敗しました。"^"Failed to import."^
s^"取り込みを中止します。よろしいですか？"^"Cancel importing. Are you sure?"^
s^"取り込み後のサイズ"^"Size after import"^
s^"取り込み状況"^"Import status"^
s^"取り込むトラックがありません。"^"There are no tracks to import."^
s^"取り込んだトラックを入れるアルバムを"^"Create an album to put the imported tracks in."^
s^"取り込んだ音楽を入れるアルバムを選択してください。"^"Please select an album for the imported music."^
s^"取り込んでいます。本機の電源を切ったり、"^"Importing data. Please turn off the device or "^
s^"取消"^"cancel"^
s^"取消さない"^"Do not cancel"^
s^"取消す"^"Cancel"^
s^"受付番号"^"Receipt number"^
s^"受付番号：%s"^"Reception number: %s"^
s^"台湾"^"Taiwan"^
s^"名前順（▲）"^"Name order (▲)"^
s^"名前順（▼）"^"Name order (▼)"^
s^"品番"^"ID:"^
s^"回転（右）"^"Rotate (right)"^
s^"回転（左）"^"Rotation (left)"^
s^"困ったときは"^"When you are in trouble"^
s^"地域番号"^"Area code"^
s^"変更"^"change"^
s^"変更した場合、映像、音楽、写真、番組表のデータは"^"If you make changes, video, music, photos, and program guide data will be changed."^
s^"外部映像入力"^"External video input"^
s^"多すぎます。取り込みを中止します。"^"Too many. Import will be canceled."^
s^"完了"^"completion"^
s^"実行してもよろしいですか？"^"Are you sure you want to do this?"^
s^"実行しますか？"^"Do you want to do it?"^
s^"左右で行います。"^"Do it left and right."^
s^"必ずお読みください"^"Please be sure to read it"^
s^"情報表示"^"Information display"^
s^"戻さない"^"Do not return"^
s^"戻している間は本機の電源を切らないでください。"^"Do not turn off the power of this unit while it is being restored."^
s^"戻す"^"return"^
s^"抜いたりしないでください。"^"Please don't pull it out."^
s^"指定されたタイトルが無効です。"^"The specified title is invalid."^
s^"指定されたチャプターが無効です。"^"The specified chapter is invalid."^
s^"指定された値が無効です。"^"The specified value is invalid."^
s^"指定された時間が無効です。"^"The time specified is invalid."^
s^"接続に失敗しました。"^"Connection failed."^
s^"接続に成功しました。"^"Connection succeeded."^
s^"接続を中断しました。"^"Connection interrupted."^
s^"接続を解除しないでください。"^"Please do not disconnect."^
s^"携帯電話のメールアドレスには、文字数制限などによりメールが正しく届かない可能性があります。できるだけ携帯電話のメールアドレスは使用しないでください。"^"Emails may not be delivered correctly to mobile phone email addresses due to character limits, etc. Please avoid using mobile phone email addresses as much as possible."^
s^"撮影日時"^"Date and time of shooting"^
s^"操作ガイド"^"Operation Guide"^
s^"日付順（古い順）"^"Date order (oldest first)"^
s^"日付順（新しい順）"^"Date order (newest first)"^
s^"日本"^"Japan"^
s^"映像、音楽、写真、番組表のデータは"^"Video, music, photos, and program guide data"^
s^"時刻を変更するときは"^"When changing the time"^
s^"時刻を設定したら"^"Once you set the time"^
s^"時刻合わせ"^"Time adjustment"^
s^"時間を設定して下さい。"^"Please set a time."^
s^"曲の長さ"^"Song length"^
s^"更新することはできません。"^"Cannot be updated."^
s^"更新作業中です。"^"Update in progress."^
s^"書き出しが終了しました。"^"Export finished."^
s^"書き出しています。本機の電源を切ったり、"^"Writing is in progress. Turn off the machine,"^
s^"書き出しできませんでした。"^"Failed to export."^
s^"最新のソフトウェアがインストール"^"Latest software installed"^
s^"最新のソフトウェアに更新されています。"^"Updated to the latest software."^
s^"最新のソフトウェアをダウンロードしています。"^"Downloading the latest software."^
s^"最新情報"^"Latest information"^
s^"未視聴順"^"Unwatched"^
s^"本体"^"Body"^
s^"本体の設定 / HDD領域設定"^"Main unit settings/HDD area settings"^
s^"本体の設定 / ハードディスクの初期化"^"Main unit settings/Hard disk initialization"^
s^"本体の設定 / 出荷時設定に戻す"^"Main unit settings/Return to factory settings"^
s^"本体の設定 / 情報表示"^"Main unit settings/information display"^
s^"「本体設定」/「ＨＤＤ領域設定」で変更してください。"^"Please change it in Main unit settings/HDD area settings."^
s^"本機の設定値がすべて初期状態に"^"All settings of this unit will be reset to default"^
s^"本機の電源を切ったり、"^"Turn off the power of this unit,"^
s^"本機の電源を切ったりしないでください。"^"Do not turn off the power of this device."^
s^"本機の電源を切ったり、ネットワークの接続を切断したり"^"Turn off the power of this machine or disconnect from the network"^
s^"本機の電源を切ったり、メモリースティックを"^"Turn off the power of the device or remove the memory stick."^
s^"次のトラックがありません。"^"No next track available."^
s^"次の画面に移ってください。"^"Please move on to the next screen."^
s^"正しく接続されていることを確認をしてください。"^"Please make sure you are connected correctly."^
s^"現在このムービーは再生できません。"^"This movie cannot be played at this time."^
s^"現在のゲーム領域"^"Current game area"^
s^"現在の設定での接続テストを行うことができます。"^"You can test the connection with the current settings."^
s^"現在予約録画準備中です。ダビングを行う場合は 予約削除してから行ってください。"^"Currently preparing for scheduled recording. If you wish to dub, please delete the reservation first."^
s^"現在録画中です。ダビングを行う場合は 停止してから行ってください。"^"Currently recording. If you want to dub, please stop it first."^
s^"用語解説"^"Glossary"^
s^"画像名"^"Image name"^
s^"     画質1（高画質）"^"Image quality 1 (high image quality)"^
s^"     画質2"^"Image quality 2"^
s^"     画質3"^"Image quality 3"^
s^"     画質4"^"Image quality 4"^
s^"     画質5"^"Image quality 5"^
s^"     画質6（長時間）"^"Image quality 6 (long time)"^
s^"番組表の情報を取得できませんでした。"^"Failed to retrieve program listing information."^
s^"発売元"^"Publisher:"^
s^"登録したメールアドレスに購入完了のお知らせが届きます。届かない場合はfeegaカスタマーセンターまでお問い合わせください。..受付番号と共通問合せ番号は、feegaカスタマーセンターへ問い合わせるときに必要です。忘れないようにすべて控えておいてください。"^"A notification of purchase completion will be sent to the registered e-mail address. If you do not receive it, please contact the feega customer center.\nThe reception number and common inquiry number are required when contacting the feega customer center. Please make sure to record them all. Please leave it there."^
s^"登録します。よろしいですか？"^"I'm registering. Are you sure?"^
s^"確認してください。"^"please confirm."^
s^"確認再生を開始します。 確認再生を終了したいときは、確認終了ボタンか、 　 を押してください。"^"Confirmation playback will start. If you want to end confirmation playback, press the confirmation end button or ."^
s^"確認終了"^"Confirmation completed"^
s^"空のアルバムは取り込めません。"^"Empty albums cannot be imported."^
s^"終了する場合は"^"If you want to exit"^
s^"編集されていないため取消しはできません。"^"Cannot be undone as it has not been edited."^
s^"編集を取消します。よろしいですか？"^"Cancel the edit. Are you sure?"^
s^"編集を完了します。よろしいですか？ 完了すると、次回から選択した部分を 再生しなくなります。"^"Complete editing. Are you sure? Once completed, the selected section will not be played again."^
s^"編集内容を保存することができませんでした。 ハードディスクの残量が足りないか、 ハードディスクに問題がある可能性があります。"^"Your edits could not be saved. There may be insufficient free space on your hard disk, or there may be a problem with your hard disk."^
s^"編集内容を保存せずに終了します。よろしいですか？"^"Exit without saving your edits. Are you sure?"^
s^"(編集後"^"(After editing)"^
s^"編集画面に戻りたいときは、　 を押してください。"^"If you want to return to the editing screen, press ."^
s^"編集－確認再生"^"Edit - Check Play"^
s^"編集箇所の上限を超えています。 これ以上編集範囲を決定することはできません。 既存の編集範囲を取消す必要があります。"^"The maximum number of edits has been exceeded. No further edits can be made. The existing edit range must be canceled."^
s^"編集－編集範囲の取消"^"Edit - Cancel editing range"^
s^"縦横比を選び、"^"Choose an aspect ratio,"^
s^"自動チャンネル設定をしない"^"Do not set automatic channels"^
s^"自動チャンネル設定をする"^"Automatic channel settings"^
s^"自動チャンネル設定を実行しますか？"^"Do you want to perform automatic channel configuration?"^
s^"視聴年齢制限のレベル"^"Level of viewing age restriction"^
s^"解像度"^"resolution"^
s^"設定した内容をハードディスクに保存して、"^"Save the settings to your hard disk and"^
s^"設定した内容を本機に保存できませんでした。"^"The settings could not be saved to the device."^
s^"設定の確認と変更"^"Checking and changing settings"^
s^"設定を初期化しています。"^"Initializing settings."^
s^"該当するデータがありません"^"No matching data"^
s^"詳細"^"detail"^
s^"認証に失敗しました。..入力内容に誤りがあります。"^"Authentication failed.\nThere is an error in the entered information."^
s^"起動できないゲームです。"^"The game cannot be started."^
s^"起動できないディスクです。"^"Unbootable disk."^
s^"遊べなくなります。"^"You won't be able to play."^
s^"違うアルバムを選択してください。"^"Please select a different album."^
s^"選択したアルバムに入っているトラックの数が"^"The number of tracks in the selected album"^
s^"選択したアルバム内のフォトが多すぎます。"^"There are too many photos in the selected album."^
s^"選択した範囲を取消します。よろしいですか？"^"Cancel the selected range. Are you sure?"^
s^"選択してください。"^"please select."^
s^"録画中のタイトルです。 その操作は実行できません。"^"The title is currently being recorded. The operation cannot be performed."^
s^"録画中は、変更できません。"^"Cannot be changed while recording."^
s^"録画予約開始時刻になると中止して終了します。"^"When the scheduled recording start time is reached, the recording will be canceled and ended."^
s^"録画予約開始時刻になると自動的に終了します。"^"It will end automatically when the scheduled recording start time arrives."^
s^"録画予約開始時刻まで1時間を切りました。"^"There is less than an hour left until the recording reservation start time."^
s^"録画予約開始時刻を過ぎました。"^"The scheduled recording start time has passed."^
s^"録画日時"^"Recording date and time"^
s^"録画時間が約17時間(SP録画時)短く"^"Recording time is about 17 hours (shorter when recording with SP)"^
s^"録画開始時刻になると自動的に終了します。"^"Recording will end automatically when the recording start time is reached."^
s^"開始しない"^"Does not start"^
s^"開始します。"^"start."^
s^"開始する"^"Start"^
s^"音声モード"^"Audio mode"^
s^"音楽などのデータは削除されません。"^"Data such as music will not be deleted."^
s^"音質"^"Sound quality"^
s^"高速読み込み"^"Fast loading"^
